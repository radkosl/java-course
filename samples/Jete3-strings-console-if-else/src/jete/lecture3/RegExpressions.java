package jete.lecture3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpressions {

	public static void main(String[] args) {
		
		String test = "Radko is tired";
		
		test.substring(9);
		
		System.out.println(test);
		
		System.out.println(test.indexOf('t'));
		
		String text = "Hello, my number in Plovdiv is +359 894 885 119, "
				+ "but in London my number is +49 89 975-99222.";
		Pattern phonePattern = Pattern.compile("\\+\\d{1,3}([ -]*([0-9]+))+");
		Matcher matcher = phonePattern.matcher(text);
		while (matcher.find()) {
			System.out.println(matcher.group());
		}

	}

}
