package jete.lecture3.strings;

public class FirstExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s = "Stand up, stand up, Balkan superman.";
		System.out.printf("s = \"%s\"%n", s);
		System.out.printf("s.length() = %d%n", s.length());
		for (int i = 0; i < s.length(); i++) {
			System.out.printf("s[%d] = %c%n", i, s.charAt(i));
		}
	}

}
