package jete.lecture3.strings;

public class ExtractingSubstrings {
	
	public static String extractExtension(String fileName) {
		String extension = "";
		int dotIndex = fileName.lastIndexOf('.');
		if (dotIndex != -1) {
			extension = fileName.substring(dotIndex + 1);
		}
		return extension;
	}

	public static String extractFileName(String path) {

		String dirSlash = System.getProperty("file.separator");
		int slashIndex = path.lastIndexOf(dirSlash);
		String fileName = path.substring(slashIndex + 1);
		return fileName;
	}

	public static String extractPath(String fullFileName) {
		String dirSlash = System.getProperty("file.separator");
		int slashIndex = fullFileName.lastIndexOf(dirSlash);
		String path = "";
		if (slashIndex != -1) {
			path = fullFileName.substring(0, slashIndex);
		}
		return path;
	}

	public static void main(String[] args) {
		String fileName = "C:\\Pics\\Rila2005.jpg";
		System.out.println("Full file name: " + fileName);

		String pathOnly = extractPath(fileName);
		System.out.println("Path: " + pathOnly);

		String fileNameOnly = extractFileName(fileName);
		System.out.println("File name only: " + fileNameOnly);

		String extension = extractExtension(fileName);
		System.out.println("File extenson: " + extension);
	}

}
