package jete.lecture3.strings;

public class StringConcatenation {

	public static void main(String[] args) {
		String firstName = "Svetlin";
		String lastName = "Nakov";

		String fullName = firstName + " " + lastName;
		System.out.println(fullName);

		int age = 25;

		String nameAndAge = 
			"Name: " + fullName + 
			"\nAge: " + age;
		System.out.println(nameAndAge);
	}

}
