package jete.lecture3.strings;

public class FindingSmallestString {

	public static void main(String[] args) {
		String[] towns = { "Sofia", "Varna", "Plovdiv", "Pleven", "Bourgas",
				"Rousse", "Stara Zagora", "Veliko Tarnovo", "Yambol", "Sliven" };
		String firstTown = towns[0];
		for (int i = 1; i < towns.length; i++) {
			String currentTown = towns[i];
			if (currentTown.compareTo(firstTown) < 0) {
				firstTown = currentTown;
			}
		}
		System.out.println("First town: " + firstTown);
	}

}
