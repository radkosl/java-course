package jete.lecture3.strings;

public class SearchingInString {

	public static void main(String[] args) {
		String str = "Java Programming Course";

		int index = str.indexOf("Java"); // index = 0
		System.out.println(index);
		index = str.indexOf("Course"); // index = 17
		System.out.println(index);
		index = str.indexOf("COURSE"); // index = -1
		System.out.println(index);
		// indexOf is case sensetive. -1 means not found
		index = str.indexOf("ram"); // index = 9
		System.out.println(index);
		index = str.indexOf("r"); // index = 6
		System.out.println(index);
		index = str.indexOf("r", 7); // index = 9
		System.out.println(index);
		index = str.indexOf("r", 10); // index = 20
		System.out.println(index);
	}

}
