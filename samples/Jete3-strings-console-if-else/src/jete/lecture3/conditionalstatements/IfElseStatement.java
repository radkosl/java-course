package jete.lecture3.conditionalstatements;

import java.util.Scanner;

public class IfElseStatement {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Please enter a number: ");
		int number = input.nextInt();

		int myVar = 100;
		
		if (number % 2 == 0) {
			myVar = 200;
		} else {
			myVar = 50;
		}
		
		
		
		System.out.println(myVar);
		
		myVar = (number % 2 == 0) ? 200 : 50;
		
		System.out.println(myVar);
		
	}

}
