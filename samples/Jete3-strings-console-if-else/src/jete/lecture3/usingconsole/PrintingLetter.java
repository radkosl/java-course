package jete.lecture3.usingconsole;

import java.util.Scanner;

public class PrintingLetter {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.printf("Enter person name: ");
		String person = input.nextLine();

		System.out.printf("Enter company name: ");
		String company = input.nextLine();

		System.out.printf("  Dear %s,%n", person);
		System.out.printf("We are pleased to inform " + 
			"you that %2$s has chosen you to take part " +
			"in the \"Java Explore the Enterprise\" " + 
			"course. %2$s wishes you good luck!%n",
			person, company);

		System.out.println("  Yours,");
		System.out.printf("  %s", company);
		input.close();
	}

}
