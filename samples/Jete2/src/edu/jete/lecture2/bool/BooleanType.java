package edu.jete.lecture2.bool;

public class BooleanType {
	public static void main(String[] args) {
		boolean b = true;
		System.out.println(b); // true
		System.out.println(!b); // false

		boolean a = false;
		System.out.println(a | b); // true
		System.out.println(a ^ b); // true
		System.out.println(a && b); // false
		System.out.println(a || b); // true
		
		//boolean val = 1; // This will not compile
	}
}
