package edu.jete.lecture2.original;

public class ComparisonAndAssignment {

	public static void main(String[] args) {
		int a = 5;
		int b = 4;
		System.out.println(a>=b);   // true
		System.out.println(a!=b);   // true
		System.out.println(a!=++b); // false
		System.out.println(a>b);    // false

		int x = 6;
		int y = 4;
		System.out.println(y*=2); // 8
		int z=y=3; // y=3 and z=3  
		System.out.println(z); // 3
		System.out.println(x|=1); // 7
		System.out.println(x+=3); // 10
		System.out.println(x/=2); // 5

	}

}
