package edu.jete.lecture2.original;

public class AssigningValues {

	public static void main(String[] args) {
		int firstValue = 5;
		int secondValue;
		int thirdValue;

		// Using an already declared variable:
		secondValue = firstValue;

		// The following cascade calling assigns
		// 3 to firstValue and then firstValue
		// to thirdValue, so both variables have
		// the value 3 as a result:
		thirdValue = firstValue = 3;

		// The following would assign the default
		// value of the string to name:
		String name = new String(); // name = ""

		// This is how we use a literal expression:
		float heightInMeters = 1.74f;

		// Here we use an already initialized variable:
		String greeting = "Hello World!";
		String message = greeting;

		System.out.println("firstValue = " + firstValue);
		System.out.println("secondValue = " + secondValue);
		System.out.println("thirdValue = " + thirdValue);
		System.out.println("name = " + name);
		System.out.println("heightInMeters = " + heightInMeters);
		System.out.println("greeting = " + greeting);
		System.out.println("message = " + message);
	}
}
