package edu.jete.lecture2.original;

public class IntegerTypes {

	public static void main(String[] args) {
		byte centuries = 20; // Usually a small number
		short years = 2000;
		int days = 730480;
		long hours = 17531520; // May be a very big number

		System.out.println(centuries + " centuries is " + years + " years, or "
				+ days + " days, or " + hours + " hours.");
	}

}