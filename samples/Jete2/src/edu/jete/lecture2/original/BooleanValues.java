package edu.jete.lecture2.original;

public class BooleanValues {

	public static void main(String[] args) {
		int a = 3;
		int b = 2;

		boolean greaterAB = (a > b);

		System.out.println(greaterAB); // false

		boolean equalA1 = (a == 1);

		System.out.println(equalA1); // true
	}
}
