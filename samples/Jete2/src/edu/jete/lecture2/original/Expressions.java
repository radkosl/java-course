package edu.jete.lecture2.original;

public class Expressions {

	public static void main(String[] args) {
		// Expression of type int (evaluated at compile time)
		int a = 2 + 3; // a = 5
		System.out.println(a);

		// Expression of type int (evaluated at runtime)
		int b = (a+3) * (a-4) + (2*a + 7) / 4;  // b = 12
		System.out.println(b);

		// Expression of type bool (evaluated at runtime)
		boolean greater = (a > b) || ((a == 0) && (b == 0)); // greater = false
		System.out.println(greater);

		// Expression of type double (evaluated at runtime)
		double c = (double) (a + b) / b; // c = 1.41666666666667
		System.out.println(c);
	}

}
