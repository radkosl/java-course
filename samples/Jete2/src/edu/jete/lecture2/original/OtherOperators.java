package edu.jete.lecture2.original;

public class OtherOperators {

	public static void main(String[] args) {
		int a = 6;
		int b = 4;
		System.out.println(a > b ? "a>b" : "b>=a"); // a>b
		System.out.println((int) a); // 6

		int c=b=3; // b=3 and c=3
		System.out.println(c); // 3
		System.out.println((a+b)/2); // 4

		String s = "Beer";
		System.out.println(s instanceof String); // True

		int d = 0;
		System.out.println(d); // 0

		System.out.println( (a+b) / d); // ArithmeticException
	}

}
