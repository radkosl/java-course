package edu.jete.lecture2.original;

public class FloatingPointTypes {

	public static void main(String[] args) {
		float floatPI = 3.141592653589793238f;
		double doublePI = 3.141592653589793238;
		System.out.println("Float PI is: " + floatPI);
		System.out.println("Double PI is: " + doublePI);

		// Example of comparison abnormality
		Float sum = 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f
				+ 0.1f + 0.1f;
		
		Float a = 1.0f;
		System.out.println(sum.equals(a));
		boolean equal = (a == sum); // false!!!
		System.out.println("a=" + a + "  sum=" + sum + "  equal=" + equal);
	}
}
