package edu.jete.lecture2.original;

public class Objects {

	public static void main(String[] args) {
		Object container = 5;
		System.out.print("The value of container is: ");
		System.out.println(container);

		container = "Five";
		System.out.print("The value of container is: ");
		System.out.println(container);
		
		container = 100f;
		
		float newNumber = 100f + (float)container;
		
		System.out.print("The value of container is: ");
		System.out.println(newNumber);
	}

}
