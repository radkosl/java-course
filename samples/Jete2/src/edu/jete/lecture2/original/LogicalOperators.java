package edu.jete.lecture2.original;

public class LogicalOperators {

	public static void main(String[] args) {
		boolean a = true;
		boolean b = false;
		System.out.println(a); // true
		System.out.println(b); // false
		System.out.println(a && b); // false
		System.out.println(a || b); // true
		System.out.println(!a); // false
		System.out.println(!b); // true
		System.out.println(b || true); // true
		System.out.println(b && true); // false
		System.out.println(b ^ true); // true
		System.out.println((5>7) ^ (a==b)); // false
		String first = "Star";
		String second = "Craft";
		System.out.println(first + second); // StarCraft
		String output = "The number is: ";
		int number = 5;
		System.out.println(output + number); // The number is: 5
	}

}
