package edu.jete.lecture2.original;

public class BitwiseOperators {

	public static void main(String[] args) {
		short a = 3;                // 00000000 00000011 = 3
		short b = 5;                // 00000000 00000101 = 5

		System.out.println( a | b); // 00000000 00000111 = 7
		System.out.println( a & b); // 00000000 00000001 = 1
		System.out.println( a ^ b); // 00000000 00000110 = 6
		System.out.println(~a & b); // 00000000 00000100 = 4
		System.out.println( a<<1 ); // 00000000 00000110 = 6
		System.out.println( a>>1 ); // 00000000 00000001 = 1
		System.out.println( a>>2 ); // 00000000 00000000 = 0
		System.out.println( a<<2 ); // 00000000 00001100 = 12
		System.out.println(~a);     // 11111111 11111100 = -4
	}

}
