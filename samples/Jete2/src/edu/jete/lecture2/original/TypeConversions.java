package edu.jete.lecture2.original;

public class TypeConversions {

	public static void main(String[] args) {
		float heightInMeters = 1.74f; // Explicit conversion
		double maxHeight = heightInMeters; // Implicit conversion

		double minHeight = (double) heightInMeters; // Explicit conversion (not required)

		float actualHeight = (float) maxHeight; // Explicit conversion

		//float maxHeightFloat = maxHeight; // Compilation error!
	}

}
