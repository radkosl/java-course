package facebook;

public class CommonUtils {
	
	private static final int ZERO_AS_INT_CONSTANT = (int)'0';
	private static final int NINE_AS_INT_CONSTANT = (int)'9';
	
	/**
	 * Given a character, check if that character is actually digit
	 */
	public static boolean isDigit(char charAt) {
		int currentVal = (int)charAt;
		return currentVal >= ZERO_AS_INT_CONSTANT && currentVal <= NINE_AS_INT_CONSTANT;
	}

	/**
	 * Given a digit as character, retrieve integer representation
	 */
	public static int getDigit(char digitAsChar) {
		
		if(isDigit(digitAsChar)) {
			int currentVal = (int)digitAsChar;
			return currentVal - ZERO_AS_INT_CONSTANT;
		}
		
		return 0;
		
	}

}
