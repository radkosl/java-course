package facebook;

public class Tasks {

	public static void main(String[] args) {
		
		// input
		String source = "12aa12aa";
		
		// --- CALL TEST METHODS ---
		
		// sum up all digits in string
		long sum = sumDigitsInString(source);
		System.out.printf("Sum of all digits in the string '%s' is: %d %n", source, sum);
		
		// sum up all numbers in a string
		sum = sumNumbersInString(source);
		System.out.printf("Sum of all numbers in the string '%s' is: %d %n", source, sum);
	}
	
	/**
	 * Find all numbers in a string, and sum them up
	 */
	private static long sumNumbersInString(String source) {
		
		StringBuilder currentNumberAsSB = new StringBuilder();
		long result = 0L;
		for (int i = 0; i < source.length(); i++) {
			if(CommonUtils.isDigit(source.charAt(i))) {
				currentNumberAsSB.append(source.charAt(i));
				if(i == source.length() - 1 || !CommonUtils.isDigit(source.charAt(i+1))) {
					int number = Integer.parseInt(currentNumberAsSB.toString());
					result = result + number;
					currentNumberAsSB = new StringBuilder();	
				}
			}
		}
		
		return result;
	}

	/**
	 * Find all digits in the string and sum them up
	 */
	private static long sumDigitsInString(String source) {
		long result = 0L;
		for (int i = 0; i < source.length(); i++) {
			if(CommonUtils.isDigit(source.charAt(i))) {
				result = result + CommonUtils.getDigit(source.charAt(i));
			}
		}
		return result;
		
	}

}
