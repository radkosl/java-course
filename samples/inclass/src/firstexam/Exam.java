package firstexam;

public class Exam {

	public static void main(String[] args) {
		
//		String source = "This is my source";
//		printZigZag(source);
		
		String source = "1234522";
		System.out.println(calculateSum(source));
		
		
	}

	private static long calculateSum(String source) {
		
		int product = 1;
		int sum = 0;
		
		int length = source.length();
		
		for (int i = 0; i < length; i++) {
			
			int currentValue = Character.getNumericValue(source.charAt(i));// Integer.parseInt("" + source.charAt(i));
			if(i % 2 == 0) {
				product = product * currentValue;
			} else {
				sum = sum + currentValue;
			}
		}
		
		return product + sum;
		
	}

	private static int getDigit(char charAt) {
		int zero = (int)'0';
		int currentVal = (int)charAt;
		return currentVal - zero;
		
	}

	public static void printZigZag(String source) {
		
		int length = source.length();
		
		for(int i = 0 ; i < length; i ++) {
			
			char forPrint = source.charAt(i);
			if(source.charAt(i) == ' ') {
				forPrint = '-';
			}
			
			if(i % 2 == 0) {
				System.out.println(forPrint);
			} else {
				System.out.println(" " + forPrint);
			}
			
		}
		
	}

}
