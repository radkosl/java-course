package design_patterns;

public abstract class Bmw {
	
	private String model;
	private int doorsCount;
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public int getDoorsCount() {
		return doorsCount;
	}
	
	public void setDoorsCount(int doorsCount) {
		this.doorsCount = doorsCount;
	}
	
	

}
