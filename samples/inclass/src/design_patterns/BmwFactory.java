package design_patterns;

public class BmwFactory {

	private BmwFactory() {
		// just hidden
	}
	
	
	public static Bmw createFromType(String type) {
		switch(type) {
			case "3" : return new Bmw3();
			case "5" : return new Bmw5();
			case "7" : return new Bmw7();
			default: throw new RuntimeException("We dont support this type");
		}
	}
}
