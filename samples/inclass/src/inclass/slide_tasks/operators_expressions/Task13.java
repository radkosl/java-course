package inclass.slide_tasks.operators_expressions;

import java.util.Scanner;

/**
	Write an expression that extracts from a
	given integer i the value of a given bit
	number b. Example: if i=5 and b=2, value=1.
 * 
 * @author radko
 *
 */
public class Task13 {

	public static void main(String[] args) {
		Scanner input  = new Scanner(System.in);
		System.out.println("Enter positive integer");
		int source = input.nextInt();
		System.out.println("Enter the position, starting from 0");
		int position = input.nextInt();
		// Same logic as in Task 12... just output 0 or 1
		int value = (source >> position) % 2  == 0 ? 0 : 1;
		String template = "The value of the bit in position %d in number %d is -> %d";
		System.out.printf(template, position, source, value);
		input.close();
	}
}
