package inclass.slide_tasks.operators_expressions;

import java.util.Scanner;

/**
 * Write a boolean expression that returns true
 * if the bit at position p in a given integer v is
 * 1. Example: if v=5 and p=1, return false.
 * 
 * @author radko
 *
 */
public class Task12 {

	public static void main(String[] args) {
		
		Scanner input  = new Scanner(System.in);
		System.out.println("Enter positive integer");
		int source = input.nextInt();
		System.out.println("Enter the position, starting from 0");
		int position = input.nextInt();
		
		// How is that working ?
		// In order to find the bit at current position
		// what we can do is to shift that amount of bits
		// so the search bit will become at the most right
		// then we can check if the resulting number is odd
		// if it is, that means the bit is 1, otherwise is 0
		// example :
		// 15 == 000001111 in binary
		// we want to check if the 2nd bit is 1 or 0.
		// so we do 15 >> 2 and we are going to end up with 00000011 as result
		// then we do the % 2 function which looks for even / odd.
		boolean expression = (source >> position) % 2 != 0;
		String template = "Is the bit at position %d in number %d with value 1? -> %b";
		System.out.printf(template, position, source, expression);
		input.close();
		
	}
}
