package inclass.slide_tasks.classes.full_example.entities;

public class Employee extends Person {

	private double salary;
	private String occupation;

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	@Override
	public String toString() {
		return "Employee [salary=" + salary + ", occupation=" + occupation + ", toString()=" + super.toString() + "]";
	}
	
}
