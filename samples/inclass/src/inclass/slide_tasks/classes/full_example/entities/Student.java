package inclass.slide_tasks.classes.full_example.entities;

public class Student extends Person {

	private String discipline;
	private double averageGrade;

	public String getDiscipline() {
		return discipline;
	}

	public void setDiscipline(String discipline) {
		this.discipline = discipline;
	}

	public double getAverageGrade() {
		return averageGrade;
	}

	public void setAverageGrade(double averageGrade) {
		this.averageGrade = averageGrade;
	}

	@Override
	public String toString() {
		return "Student [discipline=" + discipline + ", averageGrade=" + averageGrade + ", toString()="
				+ super.toString() + "]";
	}
	
}
