package inclass.slide_tasks.classes.full_example;

public class ParseLineException extends Exception {

	public ParseLineException(String message) {
		super(message);
	}

}
