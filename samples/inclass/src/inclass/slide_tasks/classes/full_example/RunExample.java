package inclass.slide_tasks.classes.full_example;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import inclass.slide_tasks.classes.full_example.entities.Person;

public class RunExample {

	public static void main(String[] args) throws FileNotFoundException {
		
		PersonFileReader reader = new PersonFileReader(new FileInputStream(new File("people.csv")));
		
		Person[] allData = reader.read();
		
		for(Person person : allData) {
			System.out.println(person.toString());
		}
	}

}
