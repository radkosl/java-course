package inclass.slide_tasks.classes.full_example;

import java.io.InputStream;
import java.util.Scanner;

import inclass.slide_tasks.classes.full_example.entities.Employee;
import inclass.slide_tasks.classes.full_example.entities.Person;
import inclass.slide_tasks.classes.full_example.entities.Student;

public class PersonFileReader {
	
	private static final int expectedLineParts = 7;
	private Scanner input;
	
	/**
	 * Constructor
	 */
	public PersonFileReader(InputStream inputStream) {
		this.input = new Scanner(inputStream);
	}
	
	/**
	 * Read method, which reads the entire file and create array of @Person objects
	 */
	public Person[] read() {
		
		if(!validState()) {
			return null;
		}
		
		int peopleCount = Integer.parseInt(input.nextLine());
		Person[] people = new Person[peopleCount];
		int currentIndex = 0;
		while(input.hasNextLine()) {
			Person person;
			String line = input.nextLine();
			try {
				person = parseLine(line);
				if(person != null) {
					people[currentIndex++] = person;
				}
			} catch (ParseLineException e) {
				System.out.println(e.getMessage());
			} catch (Exception ex) {
				System.out.println(String.format("There is problem reading data for line [%s]. Exception: [%s]", line, ex.getMessage()));
			}
		}
		
		input.close();
		
		// if some lines are not read, the array position will have null value. 
		// To remove them we need to count how many actual values we have
		// and create new array only with them
		
		int notNullCount = 0;
		for (int i = 0; i < people.length; i++) {
			if(people[i] != null) {
				notNullCount++;
			}
		}
		
		int index = 0;
		Person[] result = new Person[notNullCount];
		for (int i = 0; i < people.length; i++) {
			if(people[i] != null) {
				result[index++] = people[i];
			}
		}
		
		return result;
		
	}
	
	private Person parseLine(String line) throws ParseLineException {
		// get all parts
		String[] parts = line.split("[,]");
		
		if(parts.length != expectedLineParts) {
			String message = String.format("Cannot read the following line: [%s]. Expects [%d] parts, [%d] existing", line, expectedLineParts, parts.length);
			throw new ParseLineException(message);
		}
		
		// expects 7 parts
		String personType = parts[0];
		
		if(PersonType.EMPLOYEE.name().equals(personType)) {
			return createEmployee(parts);
		} else if (PersonType.STUDENT.name().equals(personType)) {
			return createStudent(parts);
		}
		
		return null;
	}

	private Person createStudent(String[] parts) {
		Student student = new Student();
		populate(student, parts);
		student.setDiscipline(parts[3]);
		student.setAverageGrade(Double.parseDouble(parts[6]));
		return student;
	}
		
	private Employee createEmployee(String[] parts) {
		Employee employee = new Employee();
		populate(employee, parts);
		employee.setOccupation(parts[3]);
		employee.setSalary(Double.parseDouble(parts[6]));
		return employee;
	}

	private void populate(Person p, String[] parts) {
		p.setId(Integer.parseInt(parts[1]));
		p.setName(parts[2]);
		p.setEgn(parts[4]);
		p.setAge(Integer.parseInt(parts[5]));
	}
	
	private boolean validState() {
		return input != null && input.hasNextLine();
	}
	
}
