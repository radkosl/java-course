package inclass.slide_tasks.methods;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 4.Write a method that counts how many times
	given number appears in given array. Write a
	test program to check if the method is correct.
 * @author radko
 */
public class Task4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter array length (how many elements)");
		int arraySize = input.nextInt();
		int[] source = new int[arraySize];
		System.out.println("Now fill the array");
		fillUpArray(input, source);
		System.out.println("The array we have created is: " + Arrays.toString(source));
		System.out.println("Now enter the number you want to look for:");
		int lookUpNumber = input.nextInt();
		int occurances = findNumberCount(source, lookUpNumber);
		System.out.printf("Number %d occurs %d times in the array \n. Ending...", lookUpNumber, occurances);
		input.close();
	}

	private static void fillUpArray(Scanner input, int[] source) {
		int currentIndex = 0;
		while(currentIndex < source.length) {
			System.out.println("Enter element at index: " + currentIndex);
			int element = input.nextInt();
			source[currentIndex] = element;
			currentIndex++;
		}
	}

	private static int findNumberCount(int[] source, int lookUpNumber) {
		int occurances = 0;
		for (int i = 0; i < source.length; i++) {
			if(source[i] == lookUpNumber) {
				occurances++;
			}
		}
		return occurances;
	}

}
