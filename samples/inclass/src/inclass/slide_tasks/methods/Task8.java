package inclass.slide_tasks.methods;

/**
 *  Write a method that reverses the digits of
	given decimal number.
	Example: 256 -> 652
 *
 */
public class Task8 {

	public static void main(String[] args) {
		System.out.println(reverse(12));
		System.out.println(reverse(123));
		System.out.println(reverse(531));
		System.out.println(reverse(97321));
		System.out.println(reverse(973211));
		System.out.println(reverse(8812));
		System.out.println(reverse(1));
	}
	
	/**
	 * Given a source number, reverse it.
	 */
	private static int reverse(int source) {
		
		// get the length of the number
		int digitsCount = countDigits(source);
		// starting from left to right get initial base.
		int currentBase = (int)Math.pow(10, digitsCount - 1);
		// to build up the result, we need to add the base from the right
		int inverseBase = 1;
		// hold final result
		int result = 0;
		
		int currentPosition = digitsCount;
		
		// we need to process all positions
		while(currentPosition > 0) {
			
			// get the current digit by dividing the current base
			int currentDigit = source / currentBase;
			// add this digit multiplied by the inverse base to the result.
			result = result + currentDigit * inverseBase;
			
			// substract the position, moving forward left to right
			currentPosition--;
			// decrement the number by current base
			source = source % currentBase;
			// decrement the base
			currentBase = currentBase / 10;
			// increment the inverse base for next addition
			inverseBase = inverseBase * 10;
		}
		
		return result;
	}

	/**
	 * Given source number, calculate how many digit it has
	 * e.g. 812 => 3, 1246 => 4 , 23 => 2 
	 */
	private static int countDigits(int source) {
		int currentBase = 10;
		int digitCount = 1;
		
		while(source / currentBase > 0) {
			digitCount++;
			currentBase = currentBase * 10;
		}
		return digitCount;
	}

}
