package inclass.slide_tasks.methods;

import java.util.Scanner;

/***
 * Write a program for generating and printing
	all permutations of the numbers 1, 2, ..., n for
	given integer number n. Example:
	n=3 =>{1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1},
	{3, 1, 2}, {3, 2, 1}
 * @author radkos
 */
public class Task12 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter number [n]");
		int threshold = input.nextInt();
		
		System.out.println("Finding all permutations of array: ");
		
		// put all numbers in array
		int[] source = createArray(threshold);
		
		printArray(source);
		System.out.println("Permutations list: ");
		findPermutations(source, source.length);
		
	}

	/**
	 * Create array with values from 1 to threshold
	 */
	private static int[] createArray(int threshold) {
		int[] arr = new int[threshold];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i+1;
		}
		return arr;
	}
	
	/** Recursive function for permutations **/
	private static void findPermutations(int[] source, int n) {
		
		if(n == 1) {
			printArray(source);
			return;
		}
		
		for (int i = 0; i < n; i++) {
			swap(source, i , n -1);
			findPermutations(source, n - 1);
			swap(source, i , n -1);
		}
		

	}
	
    /**
     * Simple function that swap values between 2 positions in array
     * We need temp variable to do that in order not to lose value
     */
    private static void swap(int[] source, int swapIndex1, int swapIndex2) {
        int temp = source[swapIndex1];
        source[swapIndex1] = source[swapIndex2];
        source[swapIndex2] = temp;
    }
    
	/**
	 * Print simple array
	 */
	private static void printArray(int[] source) {
		System.out.print("[");
		for (int i = 0; i < source.length; i++) {
			System.out.print(source[i]);
			if(i < source.length-1) {
				System.out.print(", ");
			}
		}
		System.out.println("]");
		System.out.println();
	}
}
