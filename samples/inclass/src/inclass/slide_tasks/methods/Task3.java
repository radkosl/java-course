package inclass.slide_tasks.methods;

import java.util.Scanner;

public class Task3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int lookUp = input.nextInt();
		
		String result = "";
		
		if(lookUp > 9) {
			result  = stringRepresentation(lookUp % 10);
		} else {
			result = stringRepresentation(lookUp);
		}
		
		System.out.println(result);
		
		input.close();
	}
	

	public static String stringRepresentation(int digit) {
		
		if(digit > 9 || digit < 0) {
			return "";
		}
		
		String word = "";
		
		switch(digit) {
			case 0 : word = "Zero"; break;
			case 1 : word = "One"; break; 
			case 2 : word = "Two"; break; 
			case 3 : word = "Three"; break;
			case 4 : word = "Four"; break;
			case 5 : word = "Five"; break;
			case 6 : word = "Six"; break;
			case 7 : word = "Seven"; break;
			case 8 : word = "Eight"; break;
			case 9 : word = "Nine";
		}
		
		return word;
	}
	

}
