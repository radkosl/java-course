package inclass.slide_tasks.arrays.sorting;

import java.util.Arrays;

/**
 * https://en.wikipedia.org/wiki/Insertion_sort
 *
 */
public class InsertionSort {

	public static void main(String a[]) {
		int[] source = { 4, 5, 64, 5, 10, 12, 6, 4, 3, 8, 1 };
		sort(source);
		System.out.println(Arrays.toString(source));
	}

	public static int[] sort(int[] input) {

		int temp;
		for (int i = 1; i < input.length; i++) {
			for (int j = i; j > 0; j--) {
				if (input[j] < input[j - 1]) {
					temp = input[j];
					input[j] = input[j - 1];
					input[j - 1] = temp;
				}
			}
		}
		return input;
	}

}
