package inclass.slide_tasks.arrays.sorting;

import java.util.Arrays;

/**
 * Worst algorithm with biggest time consumption
 * 
 * @author radko
 */
public class BubbleSort {

	public static void main(String[] args) {
		int[] source = { 4, 5, 64, 5, 10, 12, 6, 4, 3, 8, 1 };
		sort(source);
		System.out.println(Arrays.toString(source));
	}

	public static void sort(int[] source) {
		boolean swap = false;
		do {
			swap = false;
			for (int i = 0; i < source.length - 1; i++) {
				if (source[i] > source[i + 1]) {
					int temp = source[i];
					source[i] = source[i + 1];
					source[i + 1] = temp;
					swap = true;
				}
			}
		} while (swap);

	}

}
