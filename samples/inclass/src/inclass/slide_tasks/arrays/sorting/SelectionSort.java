package inclass.slide_tasks.arrays.sorting;

import java.util.Arrays;

/**
 * https://en.wikipedia.org/wiki/Selection_sort
 * @author radko
 *
 */
public class SelectionSort {

	public static void main(String[] args) {
		int[] source = { 4, 5, 64, 5, 10, 12, 6, 4, 3, 8, 1 };
		sort(source);

		System.out.println(Arrays.toString(source));
	}

	private static void sort(int[] source) {
		
		
		for (int i = 0; i < source.length; i++) {
			
			int currentMin = source[i];
			int exchangeIndex = i;
			for (int j = i+ 1; j < source.length; j++) {
				if(source[j] < currentMin) {
					currentMin = source[j];
					exchangeIndex = j;
				}
			}
			
			if(currentMin != source[i] && exchangeIndex != i) {
				int temp = source[i];
				source[i] = source[exchangeIndex];
				source[exchangeIndex] = temp;
			}
		}
		
		
	}

}
