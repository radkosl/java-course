package inclass.slide_tasks.arrays;

import java.util.Arrays;
import java.util.Scanner;

/**
 *  Write a program that creates an array
	containing all letters from the alphabet. Read
	a word from the console and print the index
	of each letter in the array
 * @author radko
 */
public class Task14 {

	public static void main(String[] args) {
		
		
		char[] array = new char[26];
		
		char current = 'a';
		for (int i = 0; i < array.length; i++) {
			array[i] = current;
			current = (char)((int)current + 1);
		}
		
		System.out.println("Array is");
		System.out.println(Arrays.toString(array));
		
		Scanner input = new Scanner(System.in);
		System.out.println("Type a word:");
		String word = input.nextLine().toLowerCase();
		
		for (int i = 0; i < word.length(); i++) {
			
			char currentChar = word.charAt(i);
			int index = (int)currentChar - (int)'a';
			System.out.printf("Letter %s is at index [%d] %n", currentChar, index);
		}
		
		
		
		input.close();
	}

}
