package inclass.slide_tasks.generics;

public class SortComparableItems {

	public static <T extends Comparable<T>> void sort(T[] items) {
		
		boolean swapExists = false;
		do {
			swapExists = false;
			for (int i = 0; i < items.length - 1; i++) {
				if(items[i].compareTo(items[i+1]) < 0) {
					swap(items, i , i + 1);
					swapExists = true;
				}
			}
		} while (swapExists);
		
	}
	
	private static <T> void swap(T[] source, int i, int j) {
		T temp = source[i];
		source[i] = source[j];
		source[j] = temp;
	}
	
}
