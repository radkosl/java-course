package inclass.slide_tasks.generics;

import java.util.Arrays;

/**
 *  Write a method that sorts an array of generic
	type that implements Comparable<T>
	interface
 *
 */
public class Task2 {

	public static void main(String[] args) {
		
		// we can use any sorting algorithm
		// lets use the easiest, bubble sort.
		
		Shape[] shapes = {
				new Circle().setRadius(10), new Rectangle().setSide1(10).setSide2(30).setSide3(25),
				new Circle().setRadius(16), new Rectangle().setSide1(20).setSide2(40).setSide3(45)
		};
		
		SortComparableItems.sort(shapes);
		
		System.out.println(Arrays.toString(shapes));
		
		
	}

}
