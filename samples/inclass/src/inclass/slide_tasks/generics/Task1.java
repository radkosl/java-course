package inclass.slide_tasks.generics;
/**
	Write a method that finds the maximal
	element of a given array of generic type that
	implements Comparable<T> interface
 */

public class Task1 {

	public static void main(String[] args) {
		
		Integer[] ints = {1 ,43 , 22, 33, 777, 56555, 2, 564};
		System.out.println("Max int is " + findMaxElement(ints));
		
		String[] strings = {"radko", "todor", "ivan", "petyr", "maria"};
		System.out.println("Max string is " + findMaxElement(strings));
		
		Shape[] shapes = {
				new Circle().setRadius(10), new Rectangle().setSide1(10).setSide2(30).setSide3(25),
				new Circle().setRadius(16), new Rectangle().setSide1(20).setSide2(40).setSide3(45)
		};
		System.out.println("Max shape (based on the area) is " + findMaxElement(shapes));
	}
	
	public static <TYPE extends Comparable<TYPE>> TYPE findMaxElement(TYPE[] sourceArray) {
		
		// array should not be empty
		if(sourceArray == null || sourceArray.length == 0) {
			return null;
		}
		
		// assume that the max element is the first element
		TYPE currentMax = sourceArray[0];
		
		for(int i = 1; i< sourceArray.length; i++) {
			
			// my current element is smaller than the next
			if(currentMax.compareTo(sourceArray[i]) < 0) {
				currentMax = sourceArray[i];
			}
		}
		
		return currentMax;
		
	}
}
