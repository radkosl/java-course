package inclass.slide_tasks.generics.homework;

import java.util.ArrayList;
import java.util.List;

public final class Catalog<TYPE> {
	
	// name of the catalog, once created, it can't be changed
	private final String name;
	
	// all elements that are in the catalog
	private final List<TYPE> elements;
	
	// max size of the catalog
	private final int capacity;
	
	public Catalog(String name, int size) {
		this.name = name;
		this.capacity = size;
		this.elements = new ArrayList<TYPE>();
	}
	
	
	public void add(TYPE element) {
		
		// you can't add more elements if you reach the initial capacity
		if(elements.size() == capacity) {
			throw new RuntimeException("The catalog is full. Cannot add more items");
		}
		
		this.elements.add(element);
	}
	
	/**
	 * Get Catalog name
	 */
	public String getName() {
		return name;
	}
	
	public int getCurrentSize() {
		return elements.size();
	}
	
	public int getMaxSize() {
		return capacity;
	}
	
	/**
	 * Convert current Catalog into array
	 */
	@SuppressWarnings("unchecked")
	public TYPE[] asArray() {
		TYPE[] array = (TYPE[]) new Object[this.elements.size()];
		return this.elements.toArray(array);
	}



}
