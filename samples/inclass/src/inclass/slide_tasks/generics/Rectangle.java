package inclass.slide_tasks.generics;

public class Rectangle extends Shape {

	private double side1;
	private double side2;
	private double side3;

	@Override
	public double getArea() {
		return side1 + side2 + side3;
	}

	public double getSide1() {
		return side1;
	}

	public Rectangle setSide1(double side1) {
		this.side1 = side1;
		return this;
	}

	public double getSide2() {
		return side2;
	}

	public Rectangle setSide2(double side2) {
		this.side2 = side2;
		return this;
	}

	public double getSide3() {
		return side3;
	}

	public Rectangle setSide3(double side3) {
		this.side3 = side3;
		return this;
	}

	@Override
	public String toString() {
		return "Rectangle [side1=" + side1 + ", side2=" + side2 + ", side3=" + side3 + "]";
	}

}
