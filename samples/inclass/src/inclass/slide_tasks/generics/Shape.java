package inclass.slide_tasks.generics;

public abstract class Shape implements Comparable<Shape> {
	
	public abstract double getArea();
	
	@Override
	public int compareTo(Shape anotherShape) {
		return Double.valueOf(getArea()).compareTo(anotherShape.getArea());
	}
}
