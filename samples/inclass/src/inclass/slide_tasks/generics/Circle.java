package inclass.slide_tasks.generics;

public class Circle extends Shape {
	
	private int radius;
	
	@Override
	public double getArea() {
		return 2 * Math.PI * getRadius();
	}

	public int getRadius() {
		return radius;
	}

	public Circle setRadius(int radius) {
		this.radius = radius;
		return this;
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	
}
