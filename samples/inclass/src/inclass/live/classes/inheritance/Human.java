package inclass.live.classes.inheritance;

public class Human {

	private int age;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		sayHello();
		this.age = age;
	}
	
	public void sayHello() {
		System.out.println("hello");
	}
}
