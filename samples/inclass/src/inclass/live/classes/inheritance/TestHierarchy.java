package inclass.live.classes.inheritance;

public class TestHierarchy {

	public static void main(String[] args) {
		
		Human human = new Human();
		human.setAge(65);
		
		human.sayHello();
		
		Employee emp = new Employee();
		emp.setAge(44);
		
		System.out.println(emp.getAge());
	}

}
