package inclass.live.classes.inheritance;

public class TestAnimals {

	public static void main(String[] args) {
		
		Animal[] animals = getAnimals();
		
		for(Animal animal : animals) {
			animal.scream();
		}
		
	}

	private static Animal[] getAnimals() {
		return new Animal[]{new Dog(), new Cat(), new Animal()};
	}
}
