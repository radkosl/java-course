package inclass.live.classes.inheritance;

public class Cat extends Animal {

	@Override
	public void scream() {
		System.out.println("Cat yells");
	}

}
