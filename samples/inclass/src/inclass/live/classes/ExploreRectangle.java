package inclass.live.classes;

public class ExploreRectangle {

	public static void main(String[] args) {
		
		Rectangle r = new Rectangle();
		r.setHeight(100);
		r.setWidth(50);
		
		System.out.println(r.calculateSurface());

	}

}
