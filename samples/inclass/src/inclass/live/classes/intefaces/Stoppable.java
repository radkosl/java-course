package inclass.live.classes.intefaces;

public interface Stoppable {

	void stop();

}
