package inclass.live.classes.intefaces;

public abstract class Player implements Playable, Stoppable, Pausable  {
	
	private String brand;
	private String price;
	
	public String getBrand() {
		return brand;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public String getPrice() {
		return price;
	}
	
	public void setPrice(String price) {
		this.price = price;
	}
	
}
