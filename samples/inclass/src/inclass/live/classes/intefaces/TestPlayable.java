package inclass.live.classes.intefaces;

public class TestPlayable {

	public static void main(String[] args) {
		
		Player audio = new AudioPlayer();
		Player video = new VideoPlayer();
		
		Pausable stream = new Stream();
		Pausable player = new AudioPlayer();
		
		player.pause();
		
	}

}
