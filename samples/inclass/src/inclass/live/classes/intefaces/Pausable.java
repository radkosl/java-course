package inclass.live.classes.intefaces;

public interface Pausable {

	void pause();

}
