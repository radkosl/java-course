package inclass.live.classes.coffee;

import java.util.Scanner;

public class RunBar {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter bar's name");
		
		Bar bar = createBar(input.nextLine());
		
		System.out.println("Enter number of employees");
		int employeesCount = Integer.parseInt(input.nextLine());
		Employee[] employeeList = new Employee[employeesCount];
		bar.setEmployees(employeeList);
		
		int counter = 0;
		
		// fill up employees
		while(counter < employeesCount) {
			
			int display = counter +1;
			System.out.println("Enter employees " + display + " name:");
			String name = input.nextLine();
			
			System.out.println("Enter employees " + display + " salary:");
			double salary = Double.valueOf(input.nextLine());
			
			System.out.println("Enter employees " + display + " occupation:");
			String occupation = input.nextLine();
			

			Employee employee = new Employee();
			employee.setName(name);
			employee.setOccupation(occupation);
			employee.setSalary(salary);

			employeeList[counter++] = employee;
		}
		
		
		System.out.println(bar);
		
		input.close();
	}

	private static Bar createBar(String barName) {
		Bar bar = new Bar();
		bar.setName(barName);
		return bar;
	}

}
