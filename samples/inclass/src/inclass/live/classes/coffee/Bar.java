package inclass.live.classes.coffee;

import java.util.Arrays;

public class Bar {

	private String name;
	private Employee[] employees;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Employee[] getEmployees() {
		return employees;
	}
	public void setEmployees(Employee[] employees) {
		this.employees = employees;
	}
	
	@Override
	public String toString() {
		return "Bar [name=" + name + ", employees=" + Arrays.toString(employees) + "]";
	}
	
	
}
