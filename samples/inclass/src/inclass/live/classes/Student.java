package inclass.live.classes;

public class Student extends Human {

	
	private static String globalName = "Radko";
	private String name = null;
	private int age = 0;
	private double salary = 0.0d;
	
	public Student() {
		
	}
	
	public void setName(String newName) {
		this.name = newName;
	}
	
	public String getName() {
		return name;
	}
	
	public static void testStatic() {
		globalName = "Hhaha";
	}
	
	public void testInstance() {
		this.name = "Testing";
	}
	
	public static void printAny(Student s) {
		System.out.println(s.getName());
	}
	
}
