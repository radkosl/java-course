package inclass.live.classes;

public class TestStaticRun {

	public static void main(String[] args) {
	
		TestStatic object1 = new TestStatic();
		
		TestStatic object2 = new TestStatic();
		
		TestStatic.classVariable = "TEST1";
		
		System.out.println(object1.classVariable);
		System.out.println(object2.classVariable);
		
		TestStatic.classVariable = "TEST2";
		
		System.out.println(object1.classVariable);
		System.out.println(object2.classVariable);
		
	}

}
