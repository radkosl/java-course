package inclass.live;

public class Strings {

	String test;
	
	public static void main(String[] args) {
	
		
		String source = "This is my text";
		
		System.out.printf("String is : %s %n", source);
		System.out.printf("String.length() is : %d %n", source.length());
		System.out.printf("String.charAt(5) is : %s %n", source.charAt(5));
	
		String result = source;
		
//		for (int i = 0; i < source.length(); i++) {
//			result = result + source.charAt(i);
//		}
		
		System.out.println(new StringBuilder(source).reverse().toString());
		for(int i = source.length() - 1; i >=0 ; i --) {
			result = result + source.charAt(i);
		}
		System.out.println(result);
		
		String sample = getSample();
		System.out.println(sample);
		
		String one = "RaDko";
		
		String two = "radko";
		
		System.out.println(one.equalsIgnoreCase(two));
		
		System.out.println(one.toUpperCase());
		if(one.equals(two)) {
			System.out.println("yey");
		}
		
		Integer test = 255;
		Integer test1 = 255;
		
		String one1 = "one";
		String anotherOne = "one";
		
		System.out.println(one1 == anotherOne);
		
		int[] source1 = {10,1,2,3,4,5,-100,6};
		
		int min = source1[0];
		for (int i = 1; i < source1.length; i++) {
			if(source1[i] < min) {
				min = source1[i];
			}
		}
		
		System.out.println(min);
		
		String src = "   raddko   ";
		
		System.out.println(src.trim());
		
	}

	private static int length(int[] test1) {
		return test1.length;
		
	}
	
	private static int elementAt(int[] array, int index) {
		return array[index];
	}
	
	public static String getSample() {
		return "My Sample";
	}
	
	
	
}
