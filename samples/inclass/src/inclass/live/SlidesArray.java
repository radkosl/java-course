package inclass.live;

import java.util.Scanner;

public class SlidesArray {
	
	public static void main(String[] args) {

//		int[] numbers = new int[2];
//		int lenght = numbers.length;
//		
//		numbers = new int[5];
//		numbers[0] = 100;
//		numbers[0] = 101;
//		numbers[0] = 102;
//		System.out.println(numbers[0]);
//		System.out.println(numbers[2]);
//
//		String[] daysOfWeek = { "Monday", 
//				"Tuesday", 
//				"Wednesday", 
//				"Thursday", 
//				"Friday", 
//				"Saturday", 
//				"Sunday" };
//		
//		System.out.println(daysOfWeek[3]);
//		
//		daysOfWeek[3] = "Thursday Upgraded";
//		
//		System.out.println(daysOfWeek[3]);
//		
//		
//		int[] source = {1,2,3,4,5,6};
//		
//		for (int i = source.length - 1; i >= 0; i--) {
//			System.out.print(stringRepresentation(source[i]));
//		}
//		
//		System.out.println();
//		int[] testEmpty = new int[10];
//		
//		for (int i = 0; i < testEmpty.length; i++) {
//			testEmpty[i] = i * i + 10;
//		}
//		
//		printArray(testEmpty);
//			
//		Scanner input = getScanner();
//		System.out.println("Type the size of the array");
//		int size = input.nextInt();
//		int[] array = new int[size];
//		
//		int currentIndex = 0;
//		while(currentIndex < size) {
//			System.out.printf("Enter number at index [%d]", currentIndex);
//			int enteredNumber = input.nextInt();
//			array[currentIndex] = enteredNumber;
//			currentIndex = currentIndex + 1;
//		}
//		
//		printArray(array);
//		
//		System.out.println(isSymetrical(array));
//		
//		
//		int sum = 0;
//		for(int currentElement : array) {
//			currentElement = 10;
//		}
//		printArray(array);
//		System.out.println(sum);
		
		printMatrix();
		
	}
	
	public static void printMatrix() {
		
		int[][] matrix = {
				{1,2,3,4},
				{5,6,7,8},
				{9,6,5,1},
				{9,6,5,1},
		};
		
		for (int i = 0; i < matrix.length; i++) {
			for(int j = 0 ; j < matrix[i].length ; j++) {
				//System.out.printf("Element at [%d][%d] is [%d]", i, j, matrix[i][j]);
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		
	}
	
	public static boolean isSymetrical(int[] source) {
		
		for(int i = 0 ; i < (source.length + 1) / 2; i++) {
			if(source[i] != source[source.length - i - 1]) {
				return false;
			}
		}
		
		return true;
	}
	
	public static Scanner getScanner() {
		return new Scanner(System.in);
	}
	public static void printArray(int[] source) {
		for (int i = 0; i < source.length; i++) {
			System.out.println(source[i]);
		}
	}
	public static String stringRepresentation(int digit) {
		
		if(digit > 9 || digit < 0) {
			return "";
		}
		
		String word = "";
		
		switch(digit) {
			case 0 : word = "Zero"; break;
			case 1 : word = "One"; break; 
			case 2 : word = "Two"; break; 
			case 3 : word = "Three"; break;
			case 4 : word = "Four"; break;
			case 5 : word = "Five"; break;
			case 6 : word = "Six"; break;
			case 7 : word = "Seven"; break;
			case 8 : word = "Eight"; break;
			case 9 : word = "Nine";
		}
		
		return word;
	}
	
}
