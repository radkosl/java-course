package inclass.live.collections.list;

import java.lang.reflect.Array;
import java.util.Arrays;

import inclass.live.collections.Node;

public class CustomLinkedList<T> {
	
	private Node<T> begin;
	private int size;
	
	
	public CustomLinkedList() {
		begin = null;
	}
	
	public void add(T element) {
		if(begin == null) {
			begin = new Node<T>(element);
		} else {
			Node<T> newElement = new Node<T>(element);
			begin.setLink(newElement);
		}
		size++;
	}
	
	/**
	 * remove last element
	 */
	public T remove() {
		return remove(size() - 1);
	}
	
	/**
	 * remove element at specific index
	 */
	public T remove(int index) {
		check(index);
		size--;
		
		if(index == 0) {
			T value = begin.getValue();
			begin = null;
			return value;
		}
		
		int currentIndex = 1;
		Node<T> previous = begin;
		Node<T> current = begin.getLink();
		
		while(current != null) {
			
			if(index == currentIndex) {
				Node<T> next = current.getLink();
				T value = current.getValue();
				previous.setLink(next);
				current = null;
				return value;
			}
			
			currentIndex++;
			previous = current;
			current = current.getLink();
			
		}
		
		return null;
	}
	
	/**
	 * Check if index is valid
	 */
	private void check(int index) {
		
		if(isEmpty()) {
			throw new IndexOutOfBoundsException("List is empty");
		}
		
		if(index < 0) {
			throw new IndexOutOfBoundsException("Cannot remove element at negative index");
		}
		
		if(size() <= index) {
			throw new IndexOutOfBoundsException("This index is bigger than the list size");
		}
		
	}

	/**
	 * Get  the size of the list
	 */
	public int size() {
		return size;
	}
	
	
	/**
	 * Check if list is empty
	 */
	public boolean isEmpty() {
		return size() == 0;
	}
	
	@SuppressWarnings("unchecked")
	/**
	 * Convert the list into array
	 */
	public T[] asArray() {
		T[] elements = (T[])Array.newInstance(Object.class, size());
		Node<T> current = begin;
		int currentIndex = 0;
		while(current != null) {
			elements[currentIndex++] = current.getValue();
			current = current.getLink();
		}
		
		return elements;
	}
	
	@Override
	/**
	 * String rep
	 */
	public String toString() {
		return Arrays.toString(asArray());
	}
	
	
}
