package inclass.live.collections.list;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TestCustomLinkedList {

	public static void main(String[] args) {
		
		
		CustomLinkedList<Integer> ints = new CustomLinkedList<>();
		ints.add(100);
		ints.add(200);
		ints.add(300);
		
		System.out.println(ints.size());
		
		ints.remove();
		
		System.out.println(ints);
		
		Map<Integer, String> map = new HashMap<>();	
		

		for (int i = 0; i < 10000; i++) {
			map.put(i+1, "test-" + i);
		}
		
		for(Integer i : map.keySet()) {
			System.out.println(map.get(i));
		}
	}

}
