package inclass.live.collections.stack;

import java.util.ArrayList;
import java.util.List;

import inclass.live.collections.Node;

public class LinkedStack<T> {

	Node<T> top;

	private int size;

	public LinkedStack() {
		top = null;
	}

	/**
	 * Add element on top of the stack
	 */
	public void push(T element) {
		
		if(top == null) {
			top = new Node<T>(element);
		} else {
			Node<T> newTop = new Node<T>(element, top);
			top = newTop;
		}
		size++;
	}
	
	/**
	 * Check what is on top of the stack
	 */
	public T peek() {
		if(isEmpty()) {
			throw new IndexOutOfBoundsException("Cannot remove from empty stack");
		}
		return top.getValue();
	}
	
	public T pop() {
		if(isEmpty()) {
			throw new IndexOutOfBoundsException("Cannot remove element from empty stack");
		}
		Node<T> secondLast = top.getLink();
		T value = top.getValue();
		top = secondLast;
		size--;
		return value;
		
	}
	
	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}
	
	public List<T> asList() {
		
		List<T> result = new ArrayList<>(size());
		
		Node<T> current = this.top;
		while(current != null) {
			result.add(current.getValue());
			current = current.getLink();
		}
		
		return result;
	}

	@Override
	public String toString() {
		return asList().toString();
	}
	
	
}
