package inclass.live.collections.stack;

public class LinkedStackTest {

	public static void main(String[] args) {
		
		LinkedStack<Integer> myStack = new LinkedStack<>();
		
		System.out.println(myStack.size() == 0);
		
		System.out.println(myStack.isEmpty() == true);
		
		myStack.push(10);
		
		System.out.println(myStack);
		
		myStack.push(20);
		
		System.out.println(myStack);
		
		System.out.println(myStack.peek());
		
		System.out.println(myStack.pop());
		
		System.out.println(myStack);
	}

}
