package inclass.live.collections.set;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import inclass.live.generics.example_compare.Animal;
import inclass.live.generics.example_compare.AnimalColor;
import inclass.live.generics.example_compare.Butterfly;
import inclass.live.generics.example_compare.Fish;
import inclass.live.generics.example_compare.Lion;
import inclass.live.generics.example_compare.Tortoise;

public class HashSetvsLinkedHashSet {

	public static void main(String[] args) {
		
		Set<Animal> unorderedAnimals = new HashSet<>();
		unorderedAnimals.add(new Lion().age(2).color(AnimalColor.YELLOW).name("Lion King"));
		unorderedAnimals.add(new Fish().age(3).color(AnimalColor.RED).name("Freddy"));
		unorderedAnimals.add(new Tortoise().age(76).color(AnimalColor.GREEN).name("Old buddy"));
		unorderedAnimals.add(new Butterfly().age(1).color(AnimalColor.BLUE).name("Butter buddy"));
		
		for(Animal a : unorderedAnimals) {
			System.out.println(a);
		}
		
		System.out.println("---");
		
		Set<Animal> orderedAnimals = new LinkedHashSet<>();
		orderedAnimals.add(new Lion().age(2).color(AnimalColor.YELLOW).name("Lion King"));
		orderedAnimals.add(new Fish().age(3).color(AnimalColor.RED).name("Freddy"));
		orderedAnimals.add(new Tortoise().age(76).color(AnimalColor.GREEN).name("Old buddy"));
		orderedAnimals.add(new Butterfly().age(1).color(AnimalColor.BLUE).name("Butter buddy"));
		
		for(Animal a : orderedAnimals) {
			System.out.println(a);
		}
		
	}

}
