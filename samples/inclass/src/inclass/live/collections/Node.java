package inclass.live.collections;

public class Node<T> {

	private T value;
	private Node<T> link;

	public Node() {
		this(null, null);
	}

	public Node(T element) {
		this(element, null);
	}

	public Node(T element, Node<T> next) {
		this.value = element;
		this.link = next;
	}

	public Node<T> getLink() {
		return link;
	}

	public void setLink(Node<T> next) {
		this.link = next;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T element) {
		this.value = element;
	}

	@Override
	public String toString() {
		return "Node [value=" + value + "]";
	}
	
}
