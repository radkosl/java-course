package inclass.live.collections.iterators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class TestIterator {

	public static void main(String[] args) {
		
		List<Integer> ints = new ArrayList<>();
		
		ints.add(20);
		ints.add(30);
		
		System.out.println(ints);
		
		Collection<Integer> unmodifiable = Collections.unmodifiableCollection(ints);
		
		unmodifiable.add(500);
		
		System.out.println(unmodifiable);
	}

}
