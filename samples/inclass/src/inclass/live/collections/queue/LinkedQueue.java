package inclass.live.collections.queue;

import java.util.ArrayList;
import java.util.List;

import inclass.live.collections.Node;

public class LinkedQueue<T> {

	Node<T> head;
	Node<T> tail;
	
	private int size;

	public LinkedQueue() {
		head = null;
		tail = null;
	}

	/**
	 * Add element on top of the stack
	 */
	public void enqueue(T element) {
		
		if(head == null) {
			head = new Node<T>(element);
			tail = head;
		} else {
			Node<T> newTail = new Node<T>(element);
			tail.setLink(newTail);
			tail = newTail;
		}
		size++;
	}
	
	/**
	 * Check what is on top of the stack
	 */
	public T peek() {
		if(isEmpty()) {
			throw new IndexOutOfBoundsException("Cannot remove from empty queue");
		}
		return head.getValue();
	}
	
	public T dequeue() {
		if(isEmpty()) {
			throw new IndexOutOfBoundsException("Cannot remove element from empty queue");
		}
		Node<T> secondElement = head.getLink();
		T value = head.getValue();
		head = secondElement;
		if(head == null) {
			tail = null;
		}
		size--;
		return value;
		
	}
	
	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}
	
	public List<T> asList() {
		
		List<T> result = new ArrayList<>(size());
		
		Node<T> current = this.head;
		while(current != null) {
			result.add(current.getValue());
			current = current.getLink();
		}
		
		return result;
	}

	@Override
	public String toString() {
		return asList().toString();
	}
	
	
}
