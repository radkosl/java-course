package inclass.live.collections.queue;

public class LinkedQueueTest {

	public static void main(String[] args) {
		
		LinkedQueue<Integer> myStack = new LinkedQueue<>();
		
		System.out.println(myStack.size() == 0);
		
		System.out.println(myStack.isEmpty() == true);
		
		myStack.enqueue(10);
		
		System.out.println(myStack);
		
		myStack.enqueue(20);
		
		System.out.println(myStack);
		
		System.out.println(myStack.peek());
		
		System.out.println(myStack.dequeue());
		
		System.out.println(myStack);
		
		System.out.println(myStack.dequeue());
		
		System.out.println(myStack);
		
		myStack.enqueue(30);
		
		System.out.println(myStack.dequeue());
	}

}
