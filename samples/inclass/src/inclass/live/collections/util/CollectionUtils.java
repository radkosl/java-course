package inclass.live.collections.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CollectionUtils {

	public static void main(String[] args) {
		
		List<Integer> list = new ArrayList<Integer>();
		list.add(100);
		
		Collection<Integer> unmod = Collections.unmodifiableCollection(list);

	}

}
