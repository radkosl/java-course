package inclass.live.files;

public class TestFinally {

	public static void main(String[] args) {
		
		int result = divide(100, 0);
		System.out.println(result);
		
	}

	private static int divide(int delimo, int delitel) {
		
		try {
			int result = delimo / delitel;
			return result;
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
			return 0;
		} finally {
			System.out.println("Finally executed");
		}
		
		
		
		
	}

}
