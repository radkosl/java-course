package inclass.live.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class PrintEvenRowsInFile {

	public static void main(String[] args) throws FileNotFoundException {
		
		File file = new File("numbers.txt");
		Scanner input;
		input = new Scanner(file);
		
		int counter = 0;
		
		while(input.hasNextLine()) {
			String currentLine = input.nextLine();
			
			if(counter % 2 == 0) {
				System.out.println(currentLine);
			}
			
			counter++;
		}
		
		input.close();

	}
	
	
}
