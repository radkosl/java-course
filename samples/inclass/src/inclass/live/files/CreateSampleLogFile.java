package inclass.live.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;

public class CreateSampleLogFile {
	
	public static void main(String[] args) throws FileNotFoundException {
	
		File file = new File("not_existing1.txt");
		
		Scanner input1 = new Scanner(file);
		
//		try {
//			Scanner input = new Scanner(file);
//		} catch(FileNotFoundException fileNotFoundExceptionObject) {
//			
//			PrintStream writer =  new PrintStream("log.txt");
//			
//			StackTraceElement[] stackTraceArray = 
//					fileNotFoundExceptionObject.getStackTrace();
//			for(StackTraceElement element : stackTraceArray) {
//				String line = element.getClassName() + " " + element.getFileName();
//				writer.println(line);
//			}
//			
//			
//		}
		
		System.err.println(file.exists());
	}

}
