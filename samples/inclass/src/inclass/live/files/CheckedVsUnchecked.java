package inclass.live.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CheckedVsUnchecked {

	public static void main(String[] args){
		
//		Scanner scanner;
//		try {
//			scanner = new Scanner(new File("dsadsa"));
//		} catch (Exception e) {
//			throw new MyException("This should not happen");
//		}
//		
		try {
			myMethod();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

	private static void myMethod() throws FileNotFoundException {
		
		
	}

}
