package inclass.live.objects;

import java.util.Arrays;

public class TestClone implements Cloneable {

	
	public static void main(String[] args) throws CloneNotSupportedException {
		
		Emp emp1 = new Emp();
		emp1.setName("t");
		emp1.setId(1);
		
		Emp emp2 = new Emp();
		emp2.setName("a");
		emp2.setId(2);
		
		Emp emp3 = new Emp();
		emp3.setName("r");
		emp3.setId(3);
		
		Emp[] arr = new Emp[]{emp1, emp2, emp3};
		
		CompareByName customComparator = new CompareByName();
		Arrays.sort(arr, customComparator);
		
		System.out.println(Arrays.toString(arr));
		
		int test = Integer.MAX_VALUE;
		short a = (short)test;
		byte b = 10;
		
		short result = (short)(a * b);
		System.out.println(result);
		
		Integer var = new Integer(5);
		printInt(var);
	}
	
	
	public static void printInt(int intObject) {
		System.out.println(Integer.toBinaryString(intObject));
	}
}
