package inclass.live.objects;

import java.util.List;

public class Emp implements Comparable{
	
	private int id;
	private String name;
	private String egn;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEgn() {
		return egn;
	}

	public void setEgn(String egn) {
		this.egn = egn;
	}
	
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((egn == null) ? 0 : egn.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
    public boolean equals(Object obj) {
        
    	if(this == obj) {
    		return true;
    	}
    	
    	if(!(obj instanceof Emp)) {
    		return false;
    	}
    	
    	Emp toCheck = (Emp)obj;
    	
    	if(!this.getEgn().equals(toCheck.getEgn())) {
    		return false;
    	}
    	
    	return true;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int compareTo(Object o) {
		Emp forCompare = (Emp)o;
		
		if(this.getId() < forCompare.getId()) {
			return 1;
		}
		
		if(this.getId() == forCompare.getId()) {
			return 0;
		}
		
		return -1;
	}

	public String toString() {
		return "Emp [id=" + id + ", name=" + name + ", egn=" + egn + "]";
	}
	
	
}
