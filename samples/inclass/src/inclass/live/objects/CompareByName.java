package inclass.live.objects;

import java.util.Comparator;

public class CompareByName implements Comparator{

	@Override
	public int compare(Object o1, Object o2) {
		
		Emp e1 = (Emp)o1;
		Emp e2 = (Emp)o2;
		
		return e1.getName().compareTo(e2.getName());
		
	}

}
