package inclass.live.objects;

public class EmpTest {

	public static void main(String[] args) {
		
		Emp e1 = new Emp();
		e1.setEgn("123");
		e1.setName("Radko");
		
		Emp e2 = new Emp();
		e2.setEgn("123");
		e2.setName("Radko");
		
		System.out.println(e1.equals(e2));
		System.out.println(e1.hashCode());
		System.out.println(e2.hashCode());
		System.out.println(e1.hashCode() == e2.hashCode());
	}

}
