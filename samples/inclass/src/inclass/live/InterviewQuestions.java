package inclass.live;

public class InterviewQuestions {

	public static void main(String[] args) {
		
		int[] source = {1,2,3,2,56,7,8,4,3,5,7,6,4,3,5,67,8};
		int lookUp = 60;
		System.out.println(sumExists(source, lookUp));

	}
	
	/**
	 * Check if 2 elements in the array adds up to the lookUp
	 * e.g. source[i] + source[j] = lookUp
	 */
	public static boolean sumExists(int[] source, int lookUp) {
		
		for (int i = 0; i < source.length; i++) {
			for (int j = i +1; j < source.length; j++) {
				
				if(source[i] + source[j] == lookUp) {
					System.out.println(source[i]);
					System.out.println(source[j]);
					return true;
				}
				
			}
		}
		
		return false;
	}

}
