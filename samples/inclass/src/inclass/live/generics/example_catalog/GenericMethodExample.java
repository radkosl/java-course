package inclass.live.generics.example_catalog;

import java.util.ArrayList;
import java.util.List;

import inclass.live.generics.example_compare.Animal;
import inclass.live.generics.example_compare.Butterfly;
import inclass.live.generics.example_compare.Lion;
import inclass.live.generics.example_compare.Tortoise;

public class GenericMethodExample {

	public static void main(String[] args) {
		int firstInt = 5;
		// Integer -> 5
		int secondInt = 3;
		// Integer -> 3
		System.out.println("firstInt = " + firstInt);
		System.out.println("secondInt = " + secondInt);
		int greaterInt = greater(firstInt, secondInt);
		System.out.println("Greater: " + greaterInt);

		String firstString = "zzzz";
		String secondString = "C++";
		System.out.println("firstString = " + firstString);
		System.out.println("secondString = " + secondString);
		String greaterString = greater(firstString, secondString);
		System.out.println("Greater: " + greaterString);
		
		Animal lion = new Lion().name("king");
		Animal butter = new Butterfly().name("abby");
		Animal tort = new Tortoise().name("slowy");
		
		System.out.println(greater(lion, butter));
		System.out.println(greater(tort, lion));
		
		List<Integer> ints = new ArrayList<Integer>();
		
		GenericMethodExample.fill(ints, new Integer(5));
		
	}
	
	public static <TYPE extends Comparable<TYPE>> TYPE greater(TYPE element1, TYPE element2) {
		if (element1.compareTo(element2) > 0) {
			return element1;
		} else {
			return element2;
		}
	}
	
	static <T> void fill(List<? super T> list, T obj) {
		list.add(obj);
	}
	
}
