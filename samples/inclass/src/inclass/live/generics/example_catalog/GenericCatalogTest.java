package inclass.live.generics.example_catalog;

import inclass.live.generics.example_compare.Animal;
import inclass.live.generics.example_compare.Butterfly;
import inclass.live.generics.example_compare.Lion;

public class GenericCatalogTest {
	public static void main(String[] args) {
		
		
		GenericCatalog<Integer> catalog = new GenericCatalog<Integer>();
		catalog.add(1);
		catalog.add(2);
		catalog.add(3);
		
		Integer[] strArr = catalog.toArray(Integer.class);
		
		for ( Integer element : strArr){
			System.out.println(element);
		}
		
		GenericCatalog animals = new GenericCatalog();
		Animal lion = new Lion().age(20);
		Animal butter = new Butterfly().age(21);
		animals.add(lion);
		animals.add(butter);
		
	}
}
