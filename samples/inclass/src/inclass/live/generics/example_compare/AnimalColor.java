package inclass.live.generics.example_compare;

public enum AnimalColor {
	RED, BLUE, BLACK, YELLOW, ORANGE, GREEN;
}
