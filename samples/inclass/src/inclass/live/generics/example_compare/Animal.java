package inclass.live.generics.example_compare;

public class Animal implements Comparable<Animal>{

	private AnimalColor color;
	private String name;
	private int age;

	public AnimalColor color() {
		return color;
	}

	public Animal color(AnimalColor color) {
		this.color = color;
		return this;
	}

	public String name() {
		return name;
	}

	public Animal name(String name) {
		this.name = name;
		return this;
	}

	public int age() {
		return age;
	}

	public Animal age(int age) {
		this.age = age;
		return this;
	}
	
	@Override
	public int compareTo(Animal animal) {
		return this.name().compareTo(animal.name());
		
	}
	
	@Override
	public String toString() {
		return "Animal [color=" + color + ", name=" + name + ", age=" + age + "]";
	}

}
