package inclass.live.generics.example_compare;

import java.util.Comparator;

public class ComparatorByAge implements Comparator<Animal> {

	@Override
	public int compare(Animal o1, Animal o2) {
		return Integer.valueOf(o1.age()).compareTo(o2.age());
	}

}
