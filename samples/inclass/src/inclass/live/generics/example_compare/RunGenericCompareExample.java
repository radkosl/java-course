package inclass.live.generics.example_compare;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RunGenericCompareExample {

	public static void main(String[] args) {

		ArrayList<Animal> animals = new ArrayList<Animal>();

		Animal lion = new Lion().age(2).color(AnimalColor.YELLOW).name("Lion King");

		animals.add(new Fish().age(3).color(AnimalColor.RED).name("Freddy"));
		animals.add(lion);
		animals.add(new Tortoise().age(76).color(AnimalColor.GREEN).name("Old buddy"));
		animals.add(new Butterfly().age(1).color(AnimalColor.BLUE).name("Butter buddy"));
		
		System.out.println(animals.contains(lion));
		System.out.println(animals.size());

		System.out.println(animals);

		Collections.sort(animals);

		System.out.println(animals);

		Collections.sort(animals, new ComparatorByAge());

		System.out.println(animals);

		Collections.sort(animals, new ComparatorByColor());

		System.out.println(animals);

		String[] source = new String[] { "radko", "z", "bla", "two two" };

		System.out.println(Arrays.toString(source));

		Arrays.sort(source);

		System.out.println(Arrays.toString(source));

		Arrays.sort(source, new CustomStringComparator());

		System.out.println(Arrays.toString(source));

		List<Integer> ints = new ArrayList<Integer>();
		ints.add(1);

		System.out.println(ints.toString());
		System.out.println(ints.size());
		
		System.out.println(getPrimes(1,10));

	}

	public static ArrayList<Integer> getPrimes(int start, int end) {
		ArrayList<Integer> primesList = new ArrayList<Integer>();
		for (int num = start; num <= end; num++) {
			boolean prime = true;
			for (int div = 2; div <= Math.sqrt(num); div++) {
				if (num % div == 0) {
					prime = false;
					break;
				}
			}
			if (prime) {
				primesList.add(num);
			}
		}
		return primesList;
	}

}
