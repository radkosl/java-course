package inclass.live.generics.example_compare;

import java.util.Comparator;

public class ComparatorByColor implements Comparator<Animal> {
	
	@Override
	public int compare(Animal o1, Animal o2) {
		return o1.color().name().compareTo(o2.color().name());
	}

}
