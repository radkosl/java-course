package inclass.live;

import java.util.Scanner;

public class Methods {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Type n");
		
		int myNum = input.nextInt();
		
		for(int i = 1; i <=myNum-1 ; i++) {
			printLine(1, i);
		}
		
		for(int j = myNum ; j >= 1; j --) {
			printLine(1, j);
		}
		
		input.close();
		
		
		int anotherSum = calculateFactoriel(myNum);
		System.out.println("Factorial is: " + anotherSum);
	}
	
	public static void printLine(int start, int end) {
		for (int i = start; i <= end; i++) {
			String space = i  == end ? "" : " ";
			System.out.print(i + space);
		}
		System.out.println();
		
	}
	
	/**
	 * This will return factoriel
	 * @param number
	 * @return
	 */
	public static int calculateFactoriel(int number) {
		
		int sum = 1;
		for(int i = 1 ; i<= number ; i++ ) {
			sum = sum * i;
		}
		
		return sum;
		
	}
	
}
