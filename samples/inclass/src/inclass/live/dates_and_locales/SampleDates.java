package inclass.live.dates_and_locales;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SampleDates {

	public static void main(String[] args) throws ParseException {
		
		Date now = new Date();
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MMMM/yyyy hh:mm:ss", new Locale("bg"));
		
		System.out.println(formatter.format(now));
		
		
		
		Calendar cal = Calendar.getInstance();
		
		Date tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
		cal.setTime(tomorrow);
		
		System.out.println(cal.get(Calendar.MONTH) + 1);
		System.out.println(cal.get(Calendar.DAY_OF_YEAR));
		
		NumberFormat bgFormat = NumberFormat.
				getCurrencyInstance();
				System.out.println(bgFormat.format(28.50));
				// лв.28,
				
				NumberFormat format = NumberFormat.
						getNumberInstance();
				
						double value = (double) format.parse("8.5");
						System.out.println(value);
						// лв.28,
		
	}

}
