package inclass.live.dates_and_locales;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

public class SampleResourceBundle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String baseName = "inclass/live/dates_and_locales/MyResources";
		
		
		do {
			System.out.println("Type between fr, en, de");
			
			Scanner input = new Scanner(System.in);
			String locale = input.nextLine();
			if(locale.equals("stop")) {
				break;
			}
			Locale currentLocale = getLocale(locale);
			ResourceBundle rb = ResourceBundle.getBundle(baseName, currentLocale);
			System.out.println(rb.getString("hello"));
		
		} while(true);
	}

	private static Locale getLocale(String locale) {
		
		switch(locale) {
			case "fr" : return Locale.FRENCH;
			case "en" : return Locale.ENGLISH;
			case "de" : return Locale.GERMAN;
		}
		return null;
	}

}
