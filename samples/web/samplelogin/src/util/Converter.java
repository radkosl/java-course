package util;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.User;

public class Converter {

	public User convert(ResultSet result) {
		
		User user = new User();
		try {
			user.setFirstName(result.getString("firstName"));
			user.setId(result.getInt("id"));
			user.setLastName(result.getString("lastName"));
			user.setPassword(result.getString("password"));
			user.setPhone(result.getString("phone"));
			user.setUsername(result.getString("username"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
}
