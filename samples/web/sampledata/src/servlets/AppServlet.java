package servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AbstractDAO;
import model.Entity;
import services.ModelService;

public class AppServlet extends AbstractServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		// handle object removal
		String removeParam = request.getParameter("remove");
		if(removeParam != null) {
			String domainObject = getDomainFromURI(request.getRequestURI());
			getDAO(domainObject).remove((String)removeParam);
			String redirectPath = (String)request.getAttribute("appPath") + "app/" + domainObject + "?removed=1";
			response.sendRedirect(redirectPath);
			return;
		}
		
		setRequiredAttributes(request);
		
		ServletContext servletContext = request.getServletContext();
		servletContext.getRequestDispatcher("/WEB-INF/template.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String domainObject = getDomainFromURI(request.getRequestURI());
		
		Map<String, String[]> allParams = request.getParameterMap();
		Map<String, String> params = new HashMap<>();

		ModelService modelService = new ModelService();
		
		for(String key : allParams.keySet()) {
			params.put(key, allParams.get(key)[0]);
		}
		
		modelService.insert(domainObject, params);
		
		String redirectPath = (String)request.getAttribute("appPath") + "app/" + domainObject + "?added=1";
		response.sendRedirect(redirectPath);
	}

}
