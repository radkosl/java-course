package servlets;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import dao.AbstractDAO;
import dao.DepartmentDAO;
import dao.DirectionDAO;
import dao.EmployeeDAO;
import model.Entity;
import services.ConnectionService;

public class AbstractServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private Pattern uriPattern = Pattern.compile(".*\\/app\\/(.*)\\/?");

	/**
	 * Load db connection pool defined in tomcat context.xml
	 * Connection pooling is amazing thing having predefined pool of connections.
	 * Once connection is closed, it is directly returned back in the pool so can be
	 * used from another thread.
	 */
	public void init(ServletConfig config) throws ServletException {
		InitialContext context;
		try {
			context = new InitialContext();
			 DataSource pool = (DataSource) context
					.lookup("java:comp/env/" + config.getServletContext().getInitParameter("poolName"));
			if (pool == null)
				throw new ServletException(
						"Unknown DataSource '" + config.getServletContext().getInitParameter("poolName") + "'");
			ConnectionService.init(pool);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Based on the URI string, try to find the domain object
	 * that we want to process
	 * @param uri
	 * @return String
	 * @throws ServletException
	 */
	protected String getDomainFromURI(String uri) throws ServletException {
		Matcher matcher = this.uriPattern.matcher(uri);
		if (matcher.matches()) {
			return matcher.group(1);
		}

		throw new ServletException("Cannot find correct domain object in the uri");
	}

	protected void setRequiredAttributes(HttpServletRequest request) throws ServletException {
		String domainObject = getDomainFromURI(request.getRequestURI());
		AbstractDAO<? extends Entity> dao = getDAO(domainObject);
		
		if ("departments".equals(domainObject)) {
			DirectionDAO dirDAO = new DirectionDAO();
			request.setAttribute("directions", dirDAO.findAll());
			dirDAO.close();
		} else if ("employees".equals(domainObject)) {
			DepartmentDAO deptDAO = new DepartmentDAO();
			request.setAttribute("departments",  new DepartmentDAO().findAll());
			deptDAO.close();
		}
		
		request.setAttribute("entries", dao.findAll());
		dao.close();
		
		request.setAttribute("added", request.getParameter("added") != null ? true : false);
		request.setAttribute("removed", request.getParameter("removed") != null ? true : false);
		request.setAttribute("currentType", domainObject);
	}
	
	AbstractDAO<? extends Entity> getDAO(String type) {
		
		if(type.equals("departments")) {
			return new DepartmentDAO();
		}
		
		if(type.equals("directions")) {
			return new DirectionDAO();
		}
		
		if(type.equals("employees")) {
			return new EmployeeDAO();
		}
		
		return null;
	}
	
}
