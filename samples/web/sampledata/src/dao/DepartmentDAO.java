package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.Department;
import model.Direction;

public class DepartmentDAO extends AbstractDAO<Department> {

	public static final String TABLENAME = "departments";

	@Override
	public List<Department> findAll() {

		try {
			ResultSet result = executeQuery(
					"select dep.id as deptId, dep.name as deptName, dir.id as dirId, dir.name as dirName from departments dep inner join directions dir ON dep.directionId = dir.id");
			List<Department> records = new ArrayList<>();

			while (result.next()) {
				Department d = new Department();

				d.setId(result.getInt("deptId"));
				d.setName(result.getString("deptName"));

				Direction dir = new Direction();
				dir.setId(result.getInt("dirId"));
				dir.setName(result.getString("dirName"));

				d.setDirection(dir);
				records.add(d);
			}
			return records;
		} catch (SQLException e) {
			return Collections.emptyList();
		}
	}

	@Override
	public Department find(String id) {
		Department record = null;
		try {
			ResultSet result = executeQuery(
					"select dep.id as deptId, dep.name as deptName, dir.id as dirId, dir.name as dirName from departments dep inner join directions dir ON dep.directionId = dir.id WHERE dep.id="
							+ id);
			if (result.next()) {
				record = new Department();
				record.setId(result.getInt("deptId"));
				record.setName(result.getString("deptName"));
				Direction dir = new Direction();
				dir.setId(result.getInt("dirId"));
				dir.setName(result.getString("dirName"));
				record.setDirection(dir);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return record;
	}

	@Override
	public void remove(String id) {

		try {
			// remove department
			String sql = "DELETE FROM " + TABLENAME + " WHERE id=" + id;
			getConnection().createStatement().executeUpdate(sql);
			// remove employees in that department
			sql = "DELETE FROM " + EmployeeDAO.TABLENAME + " WHERE departmentId=" + id;
			getConnection().createStatement().executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
