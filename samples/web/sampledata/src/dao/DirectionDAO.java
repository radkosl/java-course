package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.Direction;

public class DirectionDAO extends AbstractDAO<Direction> {
	
	public static final String TABLENAME = "directions";
	
	public DirectionDAO() {
		super();
	}
	
	public DirectionDAO(Connection c) {
		super(c);
	}
	
	@Override
	public List<Direction> findAll() {
		try {
			ResultSet result = executeQuery("select * from directions");
			List<Direction> records = new ArrayList<>();

			while (result.next()) {
				Direction d = new Direction();
				d.setId(result.getInt("id"));
				d.setName(result.getString("name"));
				records.add(d);
			}
			return records;
		} catch (SQLException e) {
			return Collections.emptyList();
		}
	}

	@Override
	public Direction find(String id) {
		
		Direction record = null;
		try {
			ResultSet result = executeQuery(
					"select * from directions where id=" + id);
			if (result.next()) {
				record = new Direction();
				record.setId(result.getInt("id"));
				record.setName(result.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return record;
		
	}
	
	@Override
	public void remove(String id) {
		try {
			
			String sql = "DELETE FROM " + TABLENAME + " WHERE id=" + id;
			getConnection().createStatement().executeUpdate(sql);
			
			// get all departments
			sql = "SELECT id FROM departments WHERE directionId=" + id;
			
			ResultSet result = getConnection().createStatement().executeQuery(sql);
			StringBuilder sb = new StringBuilder("(");
			boolean anyRecords = false;
			while(result.next()) {
				anyRecords = true;
				sb.append(result.getInt("id")).append(",");
			}
			
			if(anyRecords) {
				String inQuery = sb.substring(0, sb.length() - 1) + ")";
				sql = "DELETE FROM " + DepartmentDAO.TABLENAME + " WHERE directionId=" + id;
				getConnection().createStatement().executeUpdate(sql);
				
				sql = "DELETE FROM " + EmployeeDAO.TABLENAME + " WHERE departmentId IN " + inQuery;
				getConnection().createStatement().executeUpdate(sql);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
