package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.Department;
import model.Employee;

public class EmployeeDAO extends AbstractDAO<Employee> {
	
	public static final String TABLENAME = "employees";
	
	public EmployeeDAO(Connection connection) {
		super(connection);
	}
	
	public EmployeeDAO() {
		super();
	}

	@Override
	public List<Employee> findAll() {
		try {
			ResultSet result = executeQuery(
					"select dep.id as deptId, dep.name as deptName, emp.id as empId, emp.name as empName, emp.salary as empSalary from departments dep inner join employees emp ON dep.id = emp.departmentId");
			List<Employee> records = new ArrayList<>();

			while (result.next()) {
				Department d = new Department();
				d.setId(result.getInt("deptId"));
				d.setName(result.getString("deptName"));
				Employee emp = new Employee();
				emp.setId(result.getInt("empId"));
				emp.setName(result.getString("empName"));
				emp.setSalary(result.getDouble("empSalary"));
				emp.setDepartment(d);
				records.add(emp);
			}
			return records;
		} catch (SQLException e) {
			return Collections.emptyList();
		}
	}

	@Override
	public Employee find(String id) {
		Employee record = null;
		try {
			ResultSet result = executeQuery(
					"select dep.id as deptId, dep.name as deptName, emp.id as empId, emp.name as empName, emp.salary as empSalary from departments dep inner join employees emp ON dep.id = emp.departmentId where emp.id=" + id);
			if (result.next()) {
				record = new Employee();
				record.setId(result.getInt("empId"));
				record.setName(result.getString("empName"));
				record.setSalary(result.getDouble("empSalary"));
				Department d = new Department();
				d.setId(result.getInt("deptId"));
				d.setName(result.getString("deptName"));
				record.setDepartment(d);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return record;
	}

	@Override
	public void remove(String id) {
		// remove employee
		String sql = "DELETE FROM " + TABLENAME + " WHERE id=" + id;
		
		try {
			getConnection().createStatement().executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
