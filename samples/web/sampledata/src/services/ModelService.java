package services;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

public class ModelService {

	public void insert(String type, Map<String, String> params) {

		String tableName = type;
		String query = "INSERT INTO %s (%s) VALUES (%s)";
		
		String columns = "";
		String values = "";
		
		for(String key : params.keySet()) {
			columns += key + ",";
			values += "\"" + params.get(key) +  "\"" + ",";
		}
		
		// remove the last ,
		columns = columns.substring(0, columns.length()-1);
		values = values.substring(0, values.length()-1);
		query = String.format(query, tableName, columns, values);
		
		try {
			Connection c = ConnectionService.getConnection();
			c.createStatement().executeUpdate(query);
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
}
