package services;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;
/**
 * Singleton class which provides connection object.
 */
public class ConnectionService {
	
	private static final ConnectionService INSTANCE = new ConnectionService();
	private DataSource dataSource;
	
	public static ConnectionService init(DataSource dataSource) {
		INSTANCE.dataSource = dataSource;
		return INSTANCE;
	}
	
	private ConnectionService() {
		
	}
	
	public static Connection getConnection() throws SQLException {
		return INSTANCE.dataSource.getConnection();
	}
	
}
