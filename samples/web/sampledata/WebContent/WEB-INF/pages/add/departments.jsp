<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<form role="form" action="${appPath}app/departments" method="POST" class="form-inline">
	<div class="form-group">
		<label for="email">Name:</label> 
		<input name="name" class="form-control" type="text" placeholder="Dept. name" />
	</div>
	<div class="form-group">
		<select name="directionId" class="form-control">
			<c:forEach items="${directions}" var="direction">
				<option value="${direction.id}">${direction.name}</option>
			</c:forEach>
		</select>
	</div>
	<button type="submit" class="btn btn-success">Add</button>
</form>