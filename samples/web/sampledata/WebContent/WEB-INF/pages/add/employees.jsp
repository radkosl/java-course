<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<form role="form" action="${appPath}app/employees" method="POST" class="form-inline">
	<div class="form-group">
		<label for="email">Name:</label> 
		<input name="name" class="form-control" type="text" placeholder="Emp. name" />
	</div>
	<div class="form-group">
		<label for="email">Name:</label> 
		<input name="salary" class="form-control" type="number" placeholder="Emp. salary" />
	</div>
	<div class="form-group">
		<select name="departmentId" class="form-control">
			<c:forEach items="${departments}" var="department">
				<option value="${department.id}">${department.name}</option>
			</c:forEach>
		</select>
	</div>
	<button type="submit" class="btn btn-success">Add</button>
</form>
