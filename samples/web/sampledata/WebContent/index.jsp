<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/include/header.jsp"></c:import>

<!-- Intro Section -->
<section id="intro" class="intro-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1>Начална страница</h1>
				<p>
					<strong>Тук можете до управлявате домайн обектите -</strong>
					<code> Дирекции, Отдели и Работници</code>
				</p>
			</div>
		</div>
		
		<c:import url="/WEB-INF/include/boxes.jsp"></c:import>
		
	</div>
</section>

<c:import url="/WEB-INF/include/footer.jsp"></c:import>