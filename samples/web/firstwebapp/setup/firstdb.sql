CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE categories (
  id int(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE posts (
  id int(11) NOT NULL AUTO_INCREMENT,
  title TEXT NOT NULL,
  content TEXT NOT NULL,
  user_id int(11) NOT NULL,
  category_id int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `firstdb_users_posts_fk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `firstdb_categories_posts_fk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
);

