/**
 * Remove button confirmation
 */
var confirmLinks = document.getElementsByClassName('confirm-removal');

console.log(confirmLinks);

for (var i = 0; i < confirmLinks.length; i++) {
    confirmLinks[i].addEventListener('click', function(e) {
      e.preventDefault();
      var conf = confirm("Confirm you want to delete that");
      if(conf == true) {
          location.href= e.target.getAttribute("href");
      }
      return false;
  });
}
