<%@ taglib tagdir="/WEB-INF/tags/structure" prefix="structure"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<structure:template>
	
	<h2>Register new User</h2>
	
	<c:if test="${not registerForm.valid()}">
		<div class="message-box error">Please enter data in all fields</div>
	</c:if>
	
	<div class='register-form form-container'>
		<form action='/firstwebapp/register' method='post'>
			<div class="form-row">
				<label class="required">name</label>
				<input type='text' value="${registerForm.name}" placeholder='Enter your name' name='name' />
			</div>
			<div class="form-row">
				<label class="required">email</label>
				<input type='text' placeholder='Enter your email' value="${registerForm.email}" name='email' />
			</div>
			<div class="form-row">
				<label class="required">password</label>
				<input value="${registerForm.password}" type='password' placeholder='Enter your password' name='password' />
			</div>
			<div class="form-row">
				<input type='submit' name='submit' value='Register' />
			</div>
		</form>
	</div>

</structure:template>