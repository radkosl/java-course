<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags/structure" prefix="structure"%>
<%@ taglib tagdir="/WEB-INF/tags/partials" prefix="partials"%>

<structure:template>

	<h1>Welcome to my firstwebapp</h1>
	<p>Checkout latest blog posts that we have </p>
	
	<partials:posts-table items="${posts}" />

</structure:template>