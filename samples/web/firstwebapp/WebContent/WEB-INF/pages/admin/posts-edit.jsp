<%@ taglib tagdir="/WEB-INF/tags/structure" prefix="structure"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<structure:template>

	<h3> Edit Post '${post.title}' </h3>
	<div class="form-container">
		<form action="/firstwebapp/admin/posts/edit" method="POST">
			<div class="form-row">
				<label class="required">Title</label> <br/>
				<input type="text" name="title" value="${post.title}" placeholder="Enter new post title" />
			</div>
			<div class="form-row">
				<label class="required">Content</label> <br/>
				<textarea rows="10" cols="30" name="content" placeholder="Enter post content" >${post.content}</textarea>
			</div>
			<div class="form-row">
				<input type="hidden" name="id" value="${post.id}" />
				<input type="submit" value="Edit" />
			</div>		
		</form>
	</div>
</structure:template>