<%@ taglib tagdir="/WEB-INF/tags/structure" prefix="structure"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags/partials" prefix="partials"%>

<structure:template>

<div class="my-posts">
	<h3>All My Posts:</h3>
	
	<partials:posts-table items="${posts}" editMode="${true}" />
	
	<h3> Add new post </h3>
	
	<c:if test="${!newPostForm.valid()}">
		<div class="message-box error">Please enter data in all fields</div>
	</c:if>
	
	<div class="form-container">
		<form action="/firstwebapp/admin/posts/add" method="POST">
			<div class="form-row">
				<label class="required">Title</label>
				<input type="text" name="title" value="${newPostForm.title}" placeholder="Enter new post title" />
			</div>
			<div class="form-row">
				<label class="required">Content</label>
				<textarea rows="10" cols="30" name="content" placeholder="Enter post content" >${newPostForm.title}</textarea>
			</div>
			<div class="form-row">
				<label class="required">Category</label>
				<select name="category">
					<c:forEach items="${categories}" var="category">
						<option value="${category.id}" ${newPostForm.categoryId == category.id ? 'selected' : ''}>${category.name}</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-row">
				<input type="submit" value="Create post" />
			</div>		
		</form>
	</div>
</div>

</structure:template>