<%@ taglib tagdir="/WEB-INF/tags/structure" prefix="structure"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<structure:template>
	
	<h2>Login </h2>
	
	<c:if test="${error}">
		<div class="message-box error">Invalid email/password pair. try again.</div>
	</c:if>
	
	<div class='login-form form-container'>
		<form action='/firstwebapp/login' method='post'>
			<div class="form-row">
				<label class="required">username</label>
				<input type='text' placeholder='Enter your username' name='username' />
			</div>
			<div class="form-row">
				<label class="required">password</label>
				<input type='password' placeholder='Enter your password' name='password' />
			</div>
			<div class="form-row">
				<input type='submit' name='submit' value='Login' />
			</div>
		</form>
	</div>

</structure:template>