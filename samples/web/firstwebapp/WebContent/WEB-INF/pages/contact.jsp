<%@ taglib tagdir="/WEB-INF/tags/structure" prefix="structure"%>
<structure:template>
	<div class='contact-form form-container'>
		<form action='/firstwebapp/contact' method='post'>
			<div>
				<label>Name</label><input type='text' placeholder='Enter your name'
					name='name' />
			</div>
			<div>
				<label>Email</label><input type='text'
					placeholder='Enter your email' name='email' />
			</div>
			<div>
				<label>Subject</label><input type='text'
					placeholder='Enter your subject' name='subject' />
			</div>
			<div>
				<label>Message</label>
				<textarea name='message' placeholder='Enter your message' rows='5'></textarea>
			</div>
			<div>
				<input type='submit' name='submit' value='Send' />
			</div>
		</form>
	</div>

</structure:template>