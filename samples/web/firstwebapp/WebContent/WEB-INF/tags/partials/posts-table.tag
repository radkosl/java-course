<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ attribute name="items" type="java.util.List" required="true" %> 
<%@ attribute name="editMode" type="java.lang.Boolean" required="false" %>

<c:if test="${not empty items}">
	<table>
 		<tr>
			<th>title</th>
			<th>content</th>
			<th>in</th>
			<th>written by</th>
			<c:if test="${editMode}">
				<th>#</th>
				<th>#</th>
			</c:if>
		</tr>
		<c:forEach items="${items}" var="item">
			<tr>
				<td>${item.title}</td>
				<td>${item.content}</td>
				<td>${item.category}</td>
				<td>${item.userEmail}</td>
				<c:if test="${editMode}">
					<td><a class="editable" href="/firstwebapp/admin/posts/edit?id=${item.id}">Edit</a></td>
					<td><a class="removable confirm-removal" href="/firstwebapp/admin/posts/delete?id=${item.id}">Delete</a></td>
				</c:if>
			</tr>
		</c:forEach>
	</table>
</c:if>