<%@ taglib tagdir="/WEB-INF/tags/includes" prefix="includes"%>

<!-- We are including the header here -->
<includes:header />

<!-- We are including the part between template tags -->
<jsp:doBody />

<!-- We are including the footer here -->
<includes:footer />
