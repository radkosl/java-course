<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
		<div class='footer'>Copyright &copy; <fmt:formatDate value="${today}" pattern="yyyy" /></div>
		</div>
		<!-- end .site-container -->
		<script src="/${appName}/static/javascript/main.js"></script>
	</body>
<html>