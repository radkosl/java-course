<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class='menu'>
	<a href='/firstwebapp'>home</a>
	<a href='posts'>posts</a>
	<a href='/firstwebapp/about'>about</a>
	<a href='/firstwebapp/contact'>contact</a>
	
	<c:if test="${loggedIn}">
		<a href='/firstwebapp/admin'>admin</a>
		<a class="last" href='/firstwebapp/logout'>logout</a>
	</c:if>
	
	<c:if test="${!loggedIn}">
		<a href='/firstwebapp/login'>login</a>
		<a class="last" href='/firstwebapp/register'>register</a>
	</c:if>
	
</div>