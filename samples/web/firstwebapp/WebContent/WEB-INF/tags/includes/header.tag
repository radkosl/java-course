<%@ taglib tagdir="/WEB-INF/tags/includes" prefix="includes"%>

<!DOCTYPE html>
<html lang='en'>
	<head>
		<title>${title}</title>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans&amp;subset=cyrillic-ext" rel="stylesheet"> 
		<link href="/${appName}/static/css/main.css" rel="stylesheet">	
	</head>
<body>
	
	<div class="site-container">
	
		<div class="header">
		
			<div class="logo">
				 <a href="/${appName}"><img src="/firstwebapp/static/images/logo.png" /></a>
			</div>
			
			<div class="menu">
				<!-- We are including the menu here -->
				<includes:menu />
			</div>
			
		</div>
		
	
