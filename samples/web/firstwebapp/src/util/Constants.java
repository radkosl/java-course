package util;

public class Constants {
	
	public static final String USER_PARAM = "user";
	
	public static final String LOGGED_IN_PARAM = "loggedIn";
	
	public static final String APP_NAME = "firstwebapp";
	
}
