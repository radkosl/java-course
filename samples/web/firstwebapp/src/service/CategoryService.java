package service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import database.DBConnectionFactory;
import models.Category;

public class CategoryService extends AppService {

	public CategoryService() {
		super(DBConnectionFactory.get());
	}

	public List<Category> getAllCategories() {
		Statement statement;
		try {
			statement = getConnection().createStatement();
			String query = "SELECT * from categories";
			ResultSet results = statement.executeQuery(query);
			List<Category> categories = new ArrayList<>();
			while(results.next()) {
				Category cat = new Category();
				cat.setId(results.getInt("id"));
				cat.setName(results.getString("name"));
				categories.add(cat);
			}
			return categories;
		} catch (SQLException e) {
			return Collections.emptyList();
		}
	}
	
}
