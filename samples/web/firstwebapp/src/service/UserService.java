package service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import database.DBConnectionFactory;
import forms.RegisterForm;
import models.User;

public class UserService extends AppService {
	
	public UserService() {
		super(DBConnectionFactory.get());
	}
	
	/**
	 * Search for user by username / password
	 */
	public User getUser(String email, String password) {
		String query = "SELECT * from users WHERE email='" + email + "' AND password='" + password + "' LIMIT 1";
		Statement statement;
		try {
			statement = getConnection().createStatement();
			ResultSet results = statement.executeQuery(query);
			if(results.next()) {
				User user = new User();
				user.setId(results.getInt("id"));
				user.setEmail(results.getString("email"));
				user.setName(results.getString("name"));
				user.setPassword(results.getString("password"));
				return user;
			}
		} catch (SQLException e) {}
		return null;
	}
	
	/**
	 * Create user from @RegisterForm bean
	 */
	public void createUser(RegisterForm submittedForm) {
		String queryTemplate = "INSERT INTO users (name, email, password) VALUES('#name','#email','#password')";
		String query = queryTemplate.
				replace("#name", submittedForm.getName()).
				replace("#email", submittedForm.getEmail()).
				replace("#password", submittedForm.getPassword());
		try {
			getConnection().createStatement().executeUpdate(query);
		} catch (SQLException e) {}
	}
	
}
