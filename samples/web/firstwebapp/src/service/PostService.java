package service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import database.DBConnectionFactory;
import forms.NewPostForm;
import models.Blog;
import models.User;

public class PostService extends AppService {
	
	public PostService() {
		super(DBConnectionFactory.get());
	}
	
	/**
	 * Get last 3 @Blog's
	 * @return
	 */
	public List<Blog> getLatestPosts() {
		String query = "SELECT p.id, p.title, p.content, u.email, c.name from posts p "
				+ "INNER JOIN categories c ON p.category_id=c.id "
				+ "INNER JOIN users u ON p.user_id=u.id ORDER by p.id DESC LIMIT 3";
		Statement statement;
		try {
			statement = getConnection().createStatement();
			ResultSet results = statement.executeQuery(query);
			List<Blog> posts = new ArrayList<>();
			while (results.next()) {
				fill(posts, results);
			}
			return posts;
		} catch (SQLException e) {
			return Collections.emptyList();
		}
	}

	/**
	 * Get all @Blog's
	 */
	public List<Blog> getAllPosts() {
		Statement statement;
		try {
			statement = getConnection().createStatement();
			String query = "SELECT p.id, p.title, p.content, u.email, c.name from posts p "
					+ "INNER JOIN categories c ON p.category_id=c.id " + "INNER JOIN users u ON p.user_id=u.id";
			ResultSet results = statement.executeQuery(query);
			List<Blog> posts = new ArrayList<>();
			while (results.next()) {
				fill(posts, results);
			}
			return posts;
		} catch (SQLException e) {
			return Collections.emptyList();
		}
	}
	
	/**
	 * Get all @Blog's by specific @User
	 */
	public List<Blog> getPostsForUser(User user) {
		Statement statement;
		try {
			statement = getConnection().createStatement();
			String query = "SELECT p.id, p.title, p.content, u.email, c.name from posts p "
					+ "INNER JOIN categories c ON p.category_id=c.id " + "INNER JOIN users u ON p.user_id=u.id WHERE u.id=" + user.getId() ;
			ResultSet results = statement.executeQuery(query);
			List<Blog> posts = new ArrayList<>();
			while (results.next()) {
				fill(posts, results);
			}
			return posts;
		} catch (SQLException e) {
			return Collections.emptyList();
		}
	}
	
	/**
	 * Search for @Blog by its id
	 */
	public Blog findById(String idParam) {
		Statement statement;
		try {
			statement = getConnection().createStatement();
			String query = "SELECT * from posts where id=" + idParam;
			ResultSet results = statement.executeQuery(query);
			Blog blogPost = new Blog();
			if(results.next()) {
				blogPost.setId(results.getInt("id"));
				blogPost.setTitle(results.getString("title"));
				blogPost.setContent(results.getString("content"));
				return blogPost;
			}
		} catch (SQLException e) {}
		return null;
	}

	/**
	 * Update existing post
	 */
	public void updatePost(String id, String title, String content) {
		String query = "UPDATE posts SET title='" + title + "', content='" + content + "' WHERE id=" + id;
		Statement statement;
		try {
			statement = getConnection().createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {}
	}

	/** 
	 * Create new post
	 */
	public void createPost(NewPostForm form) {
		
		String queryTemplate = "INSERT INTO posts (title, content,user_id, category_id) VALUES('#title','#content','#userId', '#categoryId')";
		String query = queryTemplate.
				replace("#title", form.getTitle()).
				replace("#content", form.getContent()).
				replace("#userId", "" + form.getUserId()).
				replace("#categoryId", "" + form.getCategoryId());
		
		Connection connection = DBConnectionFactory.get();
		Statement statement;
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {}			
	}
	
	/**
	 * Delete post by its id
	 */
	public void deletePost(String idParam) {
		String query = "DELETE from posts WHERE id=" + idParam;
		try {
			Statement statement = getConnection().createStatement();
			statement.executeUpdate(query);
		} catch (SQLException e) {}	
	}
	
	/**
	 * Util method to populate blog posts from result set into array list
	 */
	private void fill(List<Blog> posts, ResultSet results) throws SQLException {
		Blog blogPost = new Blog();
		blogPost.setId(results.getInt("id"));
		blogPost.setTitle(results.getString("title"));
		blogPost.setContent(results.getString("content"));
		blogPost.setCategory(results.getString("name"));
		blogPost.setUserEmail(results.getString("email"));
		posts.add(blogPost);
	}

}
