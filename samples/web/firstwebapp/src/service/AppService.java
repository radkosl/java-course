package service;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class AppService {
	
	private Connection connection;
	
	/**
	 * To create service, you need to pass connection
	 */
	public AppService(Connection con) {
		this.connection = con;
	}
	
	/** 
	 * Close DB Connection when finish using the service
	 */
	public void close() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				
			}
		}
	}
	
	/**
	 * Get DB Connection
	 */
	protected Connection getConnection() {
		return connection;
	}
}
