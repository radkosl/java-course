package forms;

import javax.servlet.http.HttpServletRequest;

import util.StringUtils;

public class RegisterForm {
	
	private String name;
	private String email;
	private String password;
	private boolean isDefault;

	public RegisterForm() {
		this.isDefault = true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public static RegisterForm fromRequest(HttpServletRequest request) {
		RegisterForm form = new RegisterForm();
		form.setDefault(false);
		form.setEmail(request.getParameter("email"));
		form.setName(request.getParameter("name"));
		form.setPassword(request.getParameter("password"));
		return form;
	}

	public boolean valid() {

		if (isDefault()) {
			return true;
		}

		if (StringUtils.isEmpty(getEmail())) {
			return false;
		}
		if (StringUtils.isEmpty(getName())) {
			return false;
		}
		if (StringUtils.isEmpty(getPassword())) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return "RegisterForm [name=" + name + ", email=" + email + ", password=" + password + ", isDefault=" + isDefault
				+ "]";
	}

	
}
