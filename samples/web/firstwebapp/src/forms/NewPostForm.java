package forms;

import util.StringUtils;

public class NewPostForm {

	private String title;
	private String content;
	private int categoryId;
	private int userId;

	private boolean isDefault;

	public NewPostForm() {
		this.setDefault(true);
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public boolean valid() {
		
		if (isDefault()) {
			return true;
		}
		
		if (StringUtils.isEmpty(getTitle())) {
			return false;
		}
		if (StringUtils.isEmpty(getContent())) {
			return false;
		}
		if (StringUtils.isEmpty("" + getCategoryId())) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return "NewPostForm [title=" + title + ", content=" + content + ", categoryId=" + categoryId + ", userId="
				+ userId + "]";
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	
}
