package web.handlers.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnectionFactory;
import forms.NewPostForm;
import models.Blog;
import models.Category;
import models.User;
import service.CategoryService;
import service.PostService;
import util.Constants;

public class AdminPosts extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uri = request.getRequestURI();
		PostService postService = new PostService();
		if(uri.endsWith("/edit")) {
			String idParam = request.getParameter("id");
			Blog post = postService.findById(idParam);
			request.setAttribute("post", post);
			request.setAttribute("title", "Admin | Edit Post");
			request.getRequestDispatcher("/WEB-INF/pages/admin/posts-edit.jsp").forward(request,response);
		} else if(uri.endsWith("/delete")) {
			String idParam = request.getParameter("id");
			postService.deletePost(idParam);
			response.sendRedirect("/firstwebapp/admin");
		}
		
		postService.close();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uri = request.getRequestURI();
		PostService postService = new PostService();
		User user = (User)request.getSession().getAttribute(Constants.USER_PARAM);
		
		if (uri.endsWith("/add")) {
			
			NewPostForm newPostForm = new NewPostForm();
			newPostForm.setCategoryId(Integer.parseInt(request.getParameter("category")));
			newPostForm.setContent(request.getParameter("content"));
			newPostForm.setTitle(request.getParameter("title"));
			newPostForm.setUserId(user.getId());
			newPostForm.setDefault(false);
			
			if(newPostForm.valid()) {
				postService.createPost(newPostForm);
			} else {
				List<Blog> posts = postService.getPostsForUser(user);
				postService.close();
				CategoryService categoryService = new CategoryService();
				List<Category> categories = categoryService.getAllCategories();
				categoryService.close();
				
				request.setAttribute("newPostForm", newPostForm);
				request.setAttribute("posts", posts);
				request.setAttribute("categories", categories);
				request.getRequestDispatcher("/WEB-INF/pages/admin/admin.jsp").forward(request,response);
				return;
			}
		} else if (uri.endsWith("/edit")) {
			String idParam = request.getParameter("id");
			String title = request.getParameter("title");
			String content = request.getParameter("content");
			postService.updatePost(idParam, title, content);
		}
		postService.close();
		response.sendRedirect("/firstwebapp/admin");
	}
	
}
