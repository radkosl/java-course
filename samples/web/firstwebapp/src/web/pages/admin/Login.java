package web.pages.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.DBConnectionFactory;
import models.User;
import service.UserService;
import util.Constants;

public class Login extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("title", "PA :: Login");
		request.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		//check in database
		UserService userService = new UserService();
		User foundUser = userService.getUser(username, password);
		userService.close();
		
		if(foundUser == null) {
			// show error and return to login view
			request.setAttribute("error", true);
			request.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(request, response);
		} else {
			HttpSession session = request.getSession();
			session.setAttribute(Constants.LOGGED_IN_PARAM, true);
			session.setAttribute(Constants.USER_PARAM, foundUser);
			response.sendRedirect("/firstwebapp/admin");
		}
		
	}
	
}
