package web.pages.admin;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBConnectionFactory;
import forms.RegisterForm;
import service.UserService;

/**
 * Servlet implementation class Register
 */
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("title", "PA :: Register");
		
		if(request.getAttribute("registerForm") == null) {
			RegisterForm defaultEmptyForm = new RegisterForm();
			request.setAttribute("registerForm", defaultEmptyForm);
		}
		
		request.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RegisterForm submittedForm = RegisterForm.fromRequest(request);
		
		if(submittedForm.valid()) {
			// all good, store in the database
			UserService userService = new UserService();
			userService.createUser(submittedForm);
			userService.close();
			response.sendRedirect("/firstwebapp/login");
		} else {
			// show register form again, with all details submitted from user and with error details
			request.setAttribute("registerForm", submittedForm);
			doGet(request, response);
		}
		
	}

}
