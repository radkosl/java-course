package web.pages.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import forms.NewPostForm;
import models.Blog;
import models.Category;
import models.User;
import service.CategoryService;
import service.PostService;
import util.Constants;

/**
 * Servlet implementation class AdminPage
 */
public class Admin extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("title", "PA :: Admin");
		HttpSession session = request.getSession();
		
		// db operations
		User user = (User)session.getAttribute(Constants.USER_PARAM);
		PostService postService = new PostService();
		List<Blog> posts = postService.getPostsForUser(user);
		postService.close();
		
		CategoryService categoryService = new CategoryService();
		List<Category> categories = categoryService.getAllCategories();
		categoryService.close();
		
		// go to view
		request.setAttribute("posts", posts);
		request.setAttribute("categories", categories);
		request.setAttribute("newPostForm", new NewPostForm());
		request.getRequestDispatcher("/WEB-INF/pages/admin/admin.jsp").forward(request, response);
		
	}

}
