package web.pages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/***
 * TODO
 */
public class Contact extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("title", "PA :: Contact");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/pages/contact.jsp");
		dispatcher.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("title", "PA :: Contact");
		
		// validate input parameters
		
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String subject = request.getParameter("subject");
		String message = request.getParameter("message");
		
		// display error view
		
		if(!valid(name) || !valid(email) || !valid(subject) || !valid(message)) {
			//String errorHtml = getErrorHtml();
		   // display success view
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/pages/contact.jsp");
			dispatcher.forward(request, response);
		} else {
			// send email with the data
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/pages/contact.jsp");
			dispatcher.forward(request, response);
		}
		
		
	}
	
	/**
	 * Create error message
	 */
	private String getErrorHtml() {
		return "<div><p class='error'><strong>You need to fill all fields with valid data. Try again.</strong></p></div>";
	}

	/**
	 * Check if string is empty or null
	 */
	private boolean valid(String source) {
		
		if(source == null || source.trim().isEmpty()) {
			return false;
		}
		return true;
	}
}
