package web.pages;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Blog;
import service.PostService;

public class Home extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("title", "PA :: Home");
		
		PostService postService = new PostService();
		List<Blog> posts = postService.getLatestPosts();
		postService.close();
		
		request.setAttribute("posts", posts);
		request.getRequestDispatcher("/WEB-INF/pages/home.jsp").forward(request, response);
		
	}

}
