package servlets;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class CategoryServlet
 */
public class CategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	DataSource dataSource;
	
	public void init(ServletConfig config) throws ServletException {
		InitialContext context;
		ServletContext sContext = config.getServletContext();
		try {
			context = new InitialContext();
			dataSource = (DataSource) context
					.lookup("java:comp/env/" + sContext.getInitParameter("poolName"));
			if (dataSource == null)
				throw new ServletException(
						"Unknown DataSource '" + sContext.getInitParameter("poolName") + "'");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
