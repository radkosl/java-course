<%@ taglib tagdir="/WEB-INF/tags/template" prefix="template"%>

<template:template>

	<div class="introduction">
		<h1><< Welcome to our App >></h1>
		<p>Here you can explore all the data that
			we have in our e-commerce database. Select a box to display the
			particular domain object you want to see.</p>
	</div>

	<div class="home-links">
		<a href="products">products</a>
		<a href="orders">orders</a>
		<a href="customers">customers</a>
		<a href="employees">employees</a>
		<a href="offices">offices</a>
		<a href="payments">payments</a>
	</div>

</template:template>
