<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="format" %>
<%@ taglib tagdir="/WEB-INF/tags/template" prefix="template" %>
<%@ taglib tagdir="/WEB-INF/tags/common" prefix="common" %>

<template:template>
	<div class="container">
		
		<common:singleitemheading type="Order" uid="${uid}" returnUri="orders" />
		
		<h2>Order </h2>
		<table>
			<thead>
				<tR>
					<th>Number</th>
					<th>Order Date</th>
					<th>Shipped Date</th>
					<th>Status</th>
					<th>Comments</th>
				</tR>
			</thead>

			<tbody>
				<tr>
					<td>${order.code}</td>
					<td>
						<format:formatDate value="${order.orderDate}" pattern="dd/MM/YYYY" />
					</td>
					<td>
						<format:formatDate value="${order.shippedDate}" pattern="dd/MM/YYYY" />
					</td>
					<td>${order.status}</td>
					<td>${order.comments}</td>
				</tr>		
			</tbody>
		</table>
		
		<h2>Items </h2>
		<table>
			<thead>
				<tR>
					<th>Product Name</th>
					<th>Quantity</th>
					<th>Base Price</th>
					<th>Total</th>
				</tR>
			</thead>

			<tbody>
				<c:forEach items="${results}" var="orderDetail">
					<tr>
						<td>${orderDetail.productName}</td>
						<td>${orderDetail.quantity}</td>
						<td><format:formatNumber value="${orderDetail.price}" type="currency" /></td>
						<td><format:formatNumber value="${orderDetail.total}" type="currency" /></td>
					</tr>		
				</c:forEach>
			</tbody>
		</table>
	</div>
</template:template>