<%@ tag body-content="scriptless" language="java" pageEncoding="ISO-8859-1"%>
<%@attribute name="type" type="java.lang.String" required="true" %>
<%@attribute name="returnUri" type="java.lang.String" required="true" %> 
<%@attribute name="uid" type="java.lang.String" required="true" %>

<h1> --- You are looking at ${type} with uid ${uid} --- </h1>
<p><a href="${appPath}${returnUri}">Back to listing</a></p>
<hr />