<!-- To use that tag library, we have to include it first -->
<%@ taglib tagdir="/WEB-INF/tags/includes" prefix="include"%>

<!-- Include the head - css , head attributes -->
<include:head />

<!-- Add Logo on the top -->
<include:header />

<!-- That will be the page content -->
<jsp:doBody />

<!-- Add copyright message and close the <body> , <html> tags -->
<include:footer />