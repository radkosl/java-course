<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="format" %>
<div class="header">
	<div class="left logo">
		<a href="${appPath}"><img src="${appPath}static/images/logo.png" width="100px;" /></a>
	</div>
	<div class="right">
		<p>Classic Model Apps - Show E commerce information</p>
		<p>
			<small>current date is: 
				<strong>
					<format:formatDate value="${currentDate}" pattern="dd/MM/YYYY hh:ss" />
				</strong>
			</small>
		</p>
	</div>
	<div class="clear"></div>
</div>