package main.converters;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import main.domain.Product;

public class ProductConverter implements Converter<ResultSet, Product> {

	@Override
	public Product convert(ResultSet source) {
		List<Product> all = convertAll(source);
		return all.isEmpty() ? null : all.iterator().next();
	}

	@Override
	public List<Product> convertAll(ResultSet source) {
		
		List<Product> products = new LinkedList<>();
		
		try {
			while(source.next()) {
				Product p = new Product();
				p.setQuantity(source.getInt("quantityInStock"));
				p.setBuyPrice(source.getDouble("buyPrice"));
				p.setProductCode(source.getString("productCode"));
				p.setVendor(source.getString("productVendor"));
				p.setScale(source.getString("productScale"));
				p.setProductName(source.getString("productName"));
				products.add(p);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return products;
	}

}
