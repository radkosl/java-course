package main.converters;

import java.util.List;

interface Converter<SOURCE, TARGET> {

	TARGET convert(SOURCE source);
	
	List<TARGET> convertAll(SOURCE source);
}
