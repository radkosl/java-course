package main;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.sql.DataSource;

public class AbstractServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Load db connection pool defined in tomcat context.xml Connection pooling
	 * is amazing thing having predefined pool of connections. Once connection
	 * is closed, it is directly returned back in the pool so can be used from
	 * another thread.
	 */
	public void init(ServletConfig config) throws ServletException {
		InitialContext context;
		if(!ConnectionService.hasDataSource()) {
			try {
				context = new InitialContext();
				DataSource pool = (DataSource) context
						.lookup("java:comp/env/jdbc/classicmodels");
				if (pool == null)
					throw new ServletException(
							"Unknown DataSource '" + config.getServletContext().getInitParameter("poolName") + "'");
				ConnectionService.init(pool);
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
	}
	
}
