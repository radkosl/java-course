public class ClockAlarm {
	private int hours = 0; // Inline field initialization

	private int minutes = 0; // Inline field initialization

	// Default constructor
	public ClockAlarm() {
	}

	// Constructor with parameters
	public ClockAlarm(int hours, int minutes) {
		this.hours = hours;
		this.minutes = minutes;
	}

	// Getter And Setter of the Property Hours
	public int getHours() {
		return hours;
	}

	public void setHours(int value) {
		hours = value;
	}

	// Getter And Setter of the Property Minutes
	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int value) {
		minutes = value;
	}

}
