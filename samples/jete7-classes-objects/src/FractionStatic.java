public class FractionStatic {
	private int numerator;

	private int denominator;

	public FractionStatic(int numerator, int denominator) {
		if (0 == denominator) {
			throw new IllegalArgumentException("Denominator can't be 0!");
		}
		this.numerator = numerator;
		this.denominator = denominator;
	}

	public static FractionStatic Parse(String val) {
		if (val == null || val.length() < 1) {
			throw new IllegalArgumentException("Invalid value!");
		}

		String[] parts = val.split("/");
		if (parts.length != 2) {
			throw new NumberFormatException("Invalid format!");
		}
		int numerator;
		int denominator;
		try {
			numerator = Integer.parseInt(parts[0]);
		} catch (Exception ex) {
			throw new NumberFormatException("Error!");
		}
		try {
			denominator = Integer.parseInt(parts[1]);
		} catch (Exception ex) {
			throw new NumberFormatException("Error!");
		}

		FractionStatic result = new FractionStatic(numerator, denominator);
		return result;
	}

	public int getDenominator() {
		return denominator;
	}

	public int getNumerator() {
		return numerator;
	}

	public double getDecimalValue() {
		double result = ((double) numerator) / denominator;
		return result;
	}

	public String toString() {
		return String.format("%d/%d", numerator, denominator);
	}

	public static void main(String[] args) {
		String fract1Text = "2/3";
		String fract2Text = "4/0";
		String fract3Text = "A/2";

		FractionStatic fract1 = FractionStatic.Parse(fract1Text);
		System.out.printf("%s ~ %f%n", fract1, fract1.getDecimalValue());
		System.out.println();

		try {
			FractionStatic fract2 = FractionStatic.Parse(fract2Text);
			System.out.printf("%s ~ %f%n", fract2, fract2.getDecimalValue());
		} catch (Exception ex) {
			System.out.println(ex);
		}
		System.out.println();

		try {
			FractionStatic fract3 = FractionStatic.Parse(fract3Text);
			System.out.printf("%s ~ %f%n", fract3, fract3.getDecimalValue());
		} catch (Exception ex) {
			System.out.println(ex);
		}
		System.out.println();
	}
}
