package courseManagement;

import university.*;

public class Course {
	private String name;

	private Professor professor;

	private Student[] students;

	public Course(String name, Professor prof, Student[] students) {
		this.name = name;
		this.professor = prof;
		this.students = students;
	}

	public String getName() {
		return this.name;
	}

	public Professor getProfessor() {
		return this.professor;
	}

	public Student[] getStudents() {
		return this.students;
	}
}
