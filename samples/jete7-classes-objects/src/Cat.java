public class Cat {
	private String name;

	private String owner;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Cat() {
		this.name = "Nastradin";
	}

	public Cat(String name) {
		this.name = name;
	}

	public Cat(String name, String owner) {
		this.name = name;
		this.owner = owner;
	}

	public void SayMiau() {
		System.out.printf("Cat %s said: Miauuuuuu!%n", name);
	}
}
