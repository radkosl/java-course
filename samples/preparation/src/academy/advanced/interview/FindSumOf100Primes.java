package academy.advanced.interview;

public class FindSumOf100Primes {

	public static void main(String[] args) {	
		int count = 0;
		long sum = 0L;
		
		long num = 1;
		while(count < 1000) {
			num++;
			if(prime(num)) {
				sum += num;
				count++;
			}
		}
		System.out.println(sum);
	}

	private static boolean prime(long num) {
		int root = (int)Math.sqrt(num);
		for(int i = 2; i < root; i ++) {
			if(num % i == 0) {
				return false;
			}
		}
		return true;
	}

}
