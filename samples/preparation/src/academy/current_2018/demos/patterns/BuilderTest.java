package academy.current_2018.demos.patterns;

public class BuilderTest {

	public static void main(String [] args) {
		
		Ingredient.Builder builder = Ingredient.builder();
		Ingredient ingredient = builder.
				carb(100).
				fat(200).
				protein(300).
				build();
		
		ingredient.setMagnezium(300);
	}
}
