package academy.current_2018.demos.patterns;

public class Ingredient {

	private int carb;
	private int fat;
	private int protein;
	
	private int magnezium;
	
	public static Builder builder() {
		return new Builder();
	}
	
	public static class Builder {
		
		private int carb;
		private int fat;
		private int protein;
		
		private Builder() {
			
		}
		
		public Builder carb(int carb) {
			this.carb = carb;
			return this;
		}
		
		public Builder fat(int fat) {
			this.fat = fat;
			return this;
		}
		
		public Builder protein(int protein) {
			this.protein = protein;
			return this;
		}
		
		public Ingredient build() {
			Ingredient ingredient = new Ingredient();
			ingredient.carb = this.carb;
			ingredient.protein = this.protein;
			ingredient.fat = this.fat;
			return ingredient;
		}
	}

	public int getCarb() {
		return carb;
	}

	public int getFat() {
		return fat;
	}

	public int getProtein() {
		return protein;
	}

	public int getMagnezium() {
		return magnezium;
	}

	public void setMagnezium(int magnezium) {
		this.magnezium = magnezium;
	}

	
	
}
