package academy.current_2018.demos.patterns;

public class Singleton {
	
	private static final Singleton instance = new Singleton();
	
	private Singleton() {
		
	}
	
	public static Singleton getInstance() {
		return instance;
	}
	
}
