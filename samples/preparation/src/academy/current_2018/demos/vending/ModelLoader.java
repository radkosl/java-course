package academy.current_2018.demos.vending;

import java.util.LinkedHashSet;
import java.util.Set;

import academy.current_2018.demos.vending.model.Coin;
import academy.current_2018.demos.vending.model.Product;

/**
 * Class that can be used to have all available coins and products
 * Can be SINGLETON, remember
 */
public class ModelLoader {
	
	private final static Set<Coin> coins = new LinkedHashSet<>();
	private final static Set<Product> products = new LinkedHashSet<>();
	
	static {
		loadCoins();
		loadProducts();
	}
	
	public static Set<Product> products() {
		return products;
	}
	
	public static Set<Coin> coins() {
		return coins;
	}
	
	
	/**
	 * Find product by its id
	 */
	public static Product findProductById(int id) {
		for(Product p : products) {
			if(p.getId() == id) {
				return p;
			}
		}
		return null;
	}
	
	/**
	 * Find coin by its value
	 */
	public static Coin findCoinByValue(int value) {
		for(Coin p : coins) {
			if(p.getValue() == value) {
				return p;
			}
		}
		return null;
	}
	
	/**
	 * Read that from file may be ??
	 */
	private static void loadProducts() {
		addProduct(1, "Coca Cola", 1.20d);
		addProduct(2, "Snickers", 0.80d);
		addProduct(3, "Sprite", 1.10d);
		addProduct(4, "Fanta Orange", 1.30d);
		addProduct(5, "Ruffels", 1.70d);
	}

	/**
	 * Read that from file may be ??
	 */
	private static void loadCoins() {
		addCoin("One cent", 1);
		addCoin("Two cents", 2);
		addCoin("Five cents", 5);
		addCoin("Ten cents", 10);
		addCoin("Twenty cents", 20);
		addCoin("Fifty cents", 50);
		addCoin("1 dollar", 100);
		addCoin("2 dollars", 200);
	}
	
	private static void addCoin(String name, int value) {
		Coin coin = new Coin();
		coin.setName(name);
		coin.setValue(value);
		coins.add(coin);
	}
	
	private static void addProduct(int id, String name, double value) {
		Product product = new Product();
		product.setId(id);
		product.setName(name);
		product.setPrice(value);
		products.add(product);
	}
}
