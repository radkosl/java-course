package academy.current_2018.demos.vending;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import academy.current_2018.demos.vending.model.Coin;
import academy.current_2018.demos.vending.model.Product;

public class Run {
	
	private static final String BACK="back";
	
	private VendingMachine machine;
	private Scanner input;
	
	public static void main(String[] args) {
		
		Run run = new Run();
		run.machine  = new VendingMachine();
		run.input = new Scanner(System.in);
		
		// RUN THE MACHINE 
		
		while(true) {
			
			// show products
			run.showProducts();
			
			// show menu
			run.showMenu();
			
			int option = Utils.fromString(run.input.nextLine());
			
			switch(option) {
				
				case 1: run.addProduct(); break;
				case 2: run.addCoins(); break;
				case 3: run.showOrder(); break;
				case 4: run.reset(); break;
				case 5: run.pay(); break;
			}
			
			if(option == 6) break;
			
			System.out.println("---");
			
		}
		
	}
	
	public void showMenu() {
		System.out.println("<<Menu>>");
		System.out.println("[1] Add Product [2] Add Coins [3] Show My Order [4] Reset [5] Pay [6] Exit \n");
	}
	
	public void showProducts() {
		Set<Product> products = ModelLoader.products();
		System.out.println("<<Products>>");
		for(Product prod : products) {
			System.out.println(prod);
		}
	}
	
	/**
	 * add product
	 */
	void addProduct() {
		System.out.println("Type product id and the quantity you want. Type 'back' for cancel:");
		System.out.println("Example input: '1:2' will add 2 products with id 1");
		String value = this.input.nextLine();
		if(BACK.equals(value)) {
			return;
		}
		String[] parts = value.split("[:]");
		int productId = Utils.fromString(parts[0]);
		int quantity = Utils.fromString(parts[1]);
		
		Product product = ModelLoader.findProductById(productId);
		this.machine.orderProduct(product, quantity);
	}
	
	/**
	 * add coins
	 */
	void addCoins() {
		System.out.println("Type coin value and the quantity of that coin. Type 'back' for cancel:");
		System.out.println("Example input: '100:2' will add 2 coins of 1 dollar (100 pence = 1 dollar)");
		System.out.println("Example input: '50:3' will add 3 coins of 50 pence ");
		String value = this.input.nextLine();
		if(BACK.equals(value)) {
			return;
		}
		String[] parts = value.split("[:]");
		int coinValue = Utils.fromString(parts[0]);
		int quantity = Utils.fromString(parts[1]);
		
		Coin coin = ModelLoader.findCoinByValue(coinValue);
		this.machine.insertCoins(coin, quantity);
	}
	
	void showOrder() {
		System.out.println("<<Currently Ordered>>");
		Map<Product, Integer> currentOrder = this.machine.getCurrentOrder();
		
		double total = 0d;
		for(Product product : currentOrder.keySet()) {
			System.out.println(product.toString() + " Qty: " + currentOrder.get(product));
			total += product.getPrice() * currentOrder.get(product);
		}
		
		System.out.println("Total: $" + total);
	}
	
	void reset() {
		System.out.println("<< Order Reset. Start Again >>");
		this.machine.reset(true);
	}
	
	void pay() {
		
		Map<Coin, Integer> currentCoins = this.machine.getCurrentCoins();
		Map<Product, Integer> currentOrder = this.machine.getCurrentOrder();
		
		double currentlyInserted = 0d;
		for(Coin coin : currentCoins.keySet()) {
			currentlyInserted += coin.getValue() * currentCoins.get(coin) / 100d;
		}
		System.out.println("<< Inserted >>");
		System.out.println("$" + currentlyInserted);
		
		double currentTotal = 0d;
		for(Product product : currentOrder.keySet()) {
			currentTotal += product.getPrice() * currentOrder.get(product);
		}
		System.out.println("<< Total >>");
		System.out.println("$" + currentTotal);
		
		if(currentlyInserted >= currentTotal) {
			System.out.println("Change: " + (currentlyInserted - currentTotal));
			System.out.println("Thank you for your order");
			this.machine.reset(false);
		} else {
			System.out.println("Please insert more coins");
		}
		
	}
}

