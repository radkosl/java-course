package academy.current_2018.demos.vending;

import java.util.HashMap;
import java.util.Map;

import academy.current_2018.demos.vending.model.Coin;
import academy.current_2018.demos.vending.model.Product;

/**
 * Represents the vending machine
 */
public class VendingMachine {
		
	/**
	 * HashMap that will store all the coins in the machine
	 */	
	private Map<Coin, Integer> availableCoins = new HashMap<>();
	
	/**
	 * HashMap that will store all the products
	 */	
	private Map<Product, Integer> availableProducts = new HashMap<>();
	
	/**
	 * HashMap that will store the coins added by the user
	 */	
	private Map<Coin, Integer> customerCoins = new HashMap<>();
	
	/**
	 * HashMap that will store the current order of the customer
	 */
	private Map<Product, Integer> customerOrder = new HashMap<>();
	
	public VendingMachine() {
		loadProducts();
		loadCoins();
	}
	
	/**
	 * Get current order
	 */
	public Map<Product, Integer> getCurrentOrder() {
		return customerOrder;
	}
	
	public Map<Coin, Integer> getCurrentCoins() {
		return customerCoins;
	}
	
	/**
	 * User select to add product to his order
	 */
	public void orderProduct(Product product, int quantity) {
		if(customerOrder.containsKey(product)) {
			customerOrder.put(product, customerOrder.get(product) + quantity);
		} else {
			customerOrder.put(product, quantity);
		}
		
		// decrease the available quantity
		availableProducts.put(product, availableProducts.get(product) - quantity);
	}

	/**
	 * User insert coins
	 */
	public void insertCoins(Coin coin, int quantity) {
		if(customerCoins.containsKey(coin)) {
			customerCoins.put(coin, customerCoins.get(coin) + quantity);
		} else {
			customerCoins.put(coin, quantity);
		}
		
		// increase the available coins
		availableCoins.put(coin, availableCoins.get(coin) + quantity);
	}
	
	/**
	 * Reset current order, and currently inserted coins
	 */
	public void reset(boolean resetFullMachineState) {
		
		if(resetFullMachineState) {
			// restore the products
			for(Product p : customerOrder.keySet()) {
				availableProducts.put(p, availableProducts.get(p) + customerOrder.get(p));
			}
			
			// return the coins
			for(Coin c : customerCoins.keySet()) {
				availableCoins.put(c, availableCoins.get(c) - customerCoins.get(c));
			}
		}
		
		// clear the customer input
		customerOrder.clear();
		customerCoins.clear();
	}	
	
	/**
	 * Fill machine with all coins , 100 pieces each
	 */
	private void loadCoins() {
		for(Coin coin : ModelLoader.coins()) {
			availableCoins.put(coin, 100);
		}
		
	}
	
	/**
	 * Fill machine with all products with 50 quantity for every product
	 */
	private void loadProducts() {
		for(Product p : ModelLoader.products()) {
			availableProducts.put(p, 50);
		}
	}

}
