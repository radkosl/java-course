package academy.current_2018.demos.annotations;

public class Movie implements Validatable {

	@NotNull
	private String name;

	@NotZero
	private long length;

	@NotZero
	private double rating;

	public Movie() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

}
