package academy.current_2018.demos.annotations.simple;

@Validate
public class RegisterForm {

	@NotEmpty
	private Long name;
	
	@NotEmpty
	private String email;
	
	@NotEmpty
	private String password;
	
	private String message;
	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getName() {
		return name;
	}

	public void setName(Long name) {
		this.name = name;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
