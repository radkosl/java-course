package academy.current_2018.demos.annotations.simple;

public class ValidationException extends Exception {

	public ValidationException(String message) {
		super(message);
	}
}
