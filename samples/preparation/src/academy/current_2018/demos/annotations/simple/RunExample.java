package academy.current_2018.demos.annotations.simple;

public class RunExample {

	public static void main(String[] args) {
		
		RegisterForm reg1Form = new RegisterForm();
		RegisterForm reg2Form = new RegisterForm();
		RegisterForm reg3Form = new RegisterForm();
		
		System.out.println(RegisterForm.class == reg1Form.getClass());
		System.out.println(RegisterForm.class == reg2Form.getClass());
		System.out.println(RegisterForm.class == reg3Form.getClass());
		
//		GenericTypeValidator validator = new GenericTypeValidator();
//		
//		RegisterForm regForm = new RegisterForm();
//		regForm.setName(2323L);
//		regForm.setEmail("      ");
//		
//		Class<? extends RegisterForm> sds = regForm.getClass();
//		try {
//			validator.validate(regForm);
//			
//			// go to success
//			
//		} catch (ValidationException e) {
//			System.out.println(e.getMessage());
//			
//		}
		
		
//		ContactForm contactForm = new ContactForm();
//		
//		try {
//			validator.validate(contactForm);
//			
//			// go to success
//			
//		} catch (ValidationException e) {
//			// get errors
//			
//			// got to error page
//			
//		}
		

		
	}
	

}
