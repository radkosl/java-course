package academy.current_2018.demos.annotations.simple;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GenericTypeValidator {
	
	private static final String EMPTY_STRING = "";
	
	public void validate(Object object) throws ValidationException {
		
		Class<?> objectClass = object.getClass();
		Annotation[] annotations = objectClass.getAnnotations();
		
		boolean forValidation  = false;
		
		for(Annotation an : annotations) {
			if (an.annotationType().equals(Validate.class)) {
				forValidation = true;
			}
		}
		
		if(!forValidation) {
			String message = "This object: [%s] cannot be validated";
			System.out.println(String.format(message, object.toString()));
			return;
		}
		
		Field[] fields = objectClass.getDeclaredFields();
		
		for (Field field : fields) {
			
			if(needValidation(field)) {
				
				String fieldName = field.getName();
				
				String getterMethodName = "get" + 
						fieldName.substring(0, 1).toUpperCase() + 
						fieldName.substring(1);
				
				try {
					Method method = objectClass.getMethod(getterMethodName);
					Object result = method.invoke(object);
					
					meetValidation(result);
					
				} catch (NoSuchMethodException | SecurityException e) {
					System.err.println(String.format("cannot find method %s", getterMethodName));
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
					System.out.println(String.format("cannot invoke method %s", getterMethodName));
				}
				
				System.out.println(getterMethodName);
				
			}
		}
		
	}

	private void meetValidation(Object result) throws ValidationException {
		
		if(result == null) {
			throw new ValidationException("field is null");
		}
		
		if (!(result instanceof String)) {
			throw new ValidationException("this is not string type");
		}
		String resultAsString = (String)result;
		if(resultAsString.trim().equals(EMPTY_STRING)) {
			throw new ValidationException("string is empty");
		}
	}

	private boolean needValidation(Field field) {
		Annotation[] annotations = field.getDeclaredAnnotations();
		for(Annotation an : annotations) {
			if (an.annotationType().equals(NotEmpty.class)) {
				return true;
			}
		}
		
		return false;
	}
	
}
