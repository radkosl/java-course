package academy.current_2018.demos.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class TypeValidator<T extends Validatable> {
	
	private static Set<Class<? extends Annotation>> lookUpSet  = new HashSet<>();
	
	static {
		lookUpSet.add(NotNull.class);
		lookUpSet.add(NotZero.class);
	}
	
	public void validate(T toBeValidated) throws ValidationException {
		Class<? extends Validatable> typeClass = toBeValidated.getClass();
		
		Field[] fields = toBeValidated.getClass().getDeclaredFields();
		for(Field field : fields) {
			String fieldName = field.getName();
			
			Annotation[] annotations = field.getAnnotations();
			
			Class<? extends Annotation> foundAnnotationClass = null;
			
			for(Annotation ann : annotations) {
				if(lookUpSet.contains(ann.annotationType())) {
					foundAnnotationClass = ann.annotationType();
					break;
				}
			}
			
			// we don't need to validate
			if(foundAnnotationClass == null) {
				System.out.println(String.format("No validation required for %s. Moving on ...", fieldName));
				continue;
			}
			
			String getterMethod = buildGetterMethodName(fieldName);
			
			Method method;
			try {
				method = typeClass.getMethod(getterMethod);
				Object result = method.invoke(toBeValidated);
				examine(foundAnnotationClass, fieldName, result);
				//System.out.println(String.format("Value from %s method is %s",getterMethod, result));
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				System.err.println("Something went wrong when trying to validate value under '" + getterMethod + "' method");
				e.printStackTrace();
			}
			
		}
		
	}

	private void examine(Class<? extends Annotation> foundAnnotationClass, String fieldName, Object result) throws ValidationException {
		
		if(NotNull.class.equals(foundAnnotationClass)) {
			// check if result is null
			if(result == null) {
				throw new ValidationException(fieldName, foundAnnotationClass.getSimpleName());
			}
		}
		
		if(NotZero.class.equals(foundAnnotationClass)) {
			Number value = (Number)result;
			double asDouble = value.doubleValue();
			BigDecimal asBigDecimal = BigDecimal.valueOf(asDouble);
			if(asBigDecimal.compareTo(BigDecimal.ZERO) == 0) {
				throw new ValidationException(fieldName, foundAnnotationClass.getSimpleName());
			}
		}
		
	}

	private String buildGetterMethodName(String input) {
		String capitalized = input.substring(0, 1).toUpperCase() + input.substring(1);
		return "get" + capitalized;
	}
}
