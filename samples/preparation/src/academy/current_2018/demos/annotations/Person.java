package academy.current_2018.demos.annotations;


public class Person implements Validatable {

	@NotNull
	private String name;

	@NotZero
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
