package academy.current_2018.demos.annotations;

public class ValidationException extends Exception {
	
	private static final long serialVersionUID = -4645223535849842549L;
	
	public ValidationException(String fieldName, String validationName) {
		super(String.format("%s doesn't meet the validation of %s", fieldName, "@" + validationName));
	}
	
	
}
