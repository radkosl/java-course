package academy.current_2018.demos.annotations;

public class RunExample {

	public static void main(String[] args) {
		
		TypeValidator<Movie> validator = new TypeValidator<>();
		
		Movie movie = new Movie();
		movie.setLength(1000 * 60 * 180);
		movie.setName("Armagedon");
		
		try {
			validator.validate(movie);
		} catch (ValidationException e) {
			System.err.println(e.getMessage());
		}
		
		
		TypeValidator<Person> validator1 = new TypeValidator<>();
		
		Person person = new Person();
		person.setAge(0);
		person.setName("Radko sled 21 godini");
		
		try {
			validator1.validate(person);
		} catch (ValidationException e) {
			System.err.println(e.getMessage());
		}
		
	}
	
}
