package academy.current_2018.demos.algorithms;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class SortingTest {

	public boolean hasDuplicate(char[] arr) {

//		for (int i = 0; i < arr.length; i++) {
//			for (int j = i + 1; j < arr.length; j++) {
//				if (arr[i] == arr[j]) {
//					return true;
//				}
//			}
//		}
//
//		return false;

		Set<Character> chars = new HashSet<>();
		for (int i = 0; i < arr.length; i++) {
			if (chars.contains(arr[i])) {
				return true;
			}
			chars.add(arr[i]);
		}

		return false;
	}

	public static void main(String[] args) {

		int[] arr = generateArray(100000);
		long before = System.currentTimeMillis();
		BubbleSort.sort(arr);
		System.out.println(System.currentTimeMillis() - before);

		arr = generateArray(200000);
		before = System.currentTimeMillis();
		Arrays.sort(arr);
		System.out.println(System.currentTimeMillis() - before);

	}

	private static int[] generateArray(int size) {
		int[] arr = new int[size];
		Random random = new Random();
		for (int i = 0; i < arr.length; i++) {
			arr[i] = random.nextInt(10000) * 400 + 20;
		}
		return arr;
	}

}
