package academy.current_2018.demos.algorithms;

import java.util.Arrays;

public class BubbleSort {

	public static void main(String[] args) {

		int[] source = { 4, 2, 5, 7, 4, 23, 5, 8, 5, 43, 6, 8, 6, 3, 6, 8, 6, 3 };
		
		System.out.println(Arrays.toString(source));
		
		sort(source);
		
		//System.out.println(Arrays.toString(source));
		
	}

	public static void sort(int[] source) {
		
		boolean swapExists = false;
		do {
			swapExists = false;
			for (int i = 0; i < source.length - 1; i++) {
				if(source[i] > source[i+1]) {
					swap(source, i , i + 1);
					swapExists = true;
				}
			}
		} while (swapExists);
		
		
	}

	private static void swap(int[] source, int i, int j) {
		int temp = source[i];
		source[i] = source[j];
		source[j] = temp;
	}

	@Override
	public String toString() {
		return "BubbleSort []";
	}

}
