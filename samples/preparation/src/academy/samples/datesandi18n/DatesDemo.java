package academy.samples.datesandi18n;

import java.util.*;
import java.text.*;

public class DatesDemo {

	public static void main(String[] args) {
		Locale currentLocale = Locale.getDefault();
		System.out.println("Current dafaule locale's country: " +
				currentLocale.getDisplayCountry());

		Date now = new Date();
		System.out.println("Now is: " + now);

		SimpleDateFormat shortDateFormat = new SimpleDateFormat("MM/dd/yy");
		System.out.println("The date in a short format is: " +
			shortDateFormat.format(now));
		
		SimpleDateFormat longDateFormat = 
			new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
		System.out.println("The date and time in a long format is: " +
			longDateFormat.format(now));
		
		Locale bgLocale = new Locale("bg", "BG");
		DateFormat bgDateFormat = DateFormat.getDateTimeInstance(
			DateFormat.LONG, DateFormat.LONG, bgLocale);
		String formattedDate = bgDateFormat.format(now);
		System.out.println("The date in long fomat (Bulgarian): " + formattedDate);
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(now);
		int month = calendar.get(GregorianCalendar.MONTH);
		System.out.println("The month is: " + (month+1));
		
        String dateString = "03/11/06";
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy", Locale.US);
        try {
            Date date = format.parse(dateString);
            System.out.println("Original string for parsing: " + dateString);
            System.out.println("Parsed date: " + date.toString());
        }
        catch(ParseException pe) {
            System.out.println("ERROR: Could not parse date" +
           		"in the string \"" + dateString + "\"");
        }
	}

}
