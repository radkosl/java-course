package academy.samples.workingwithobjects;

public class EqualityAndIdentity {

	private String name;
	private Integer count;
	
	public static void main(String[] args) {
		String a = new String ("fish");
		String b = new String ("fish");
		String c = b;
		System.out.println(a.equals(b)); // equality - true;
		System.out.println(b == c); // identity - true
		System.out.println(a 	== b); // identity false
		
		EqualityAndIdentity eee = new EqualityAndIdentity();
		eee.name = "test";
		
		System.out.println(eee.name.equals(null));
		System.out.println(eee.count);
		
		MyTest test1 = new MyTest();
		test1.setName("radko");
		
		MyTest test2 = new MyTest();
		test2.setName("radko");
		
		System.out.println(test1 == test2);
		
		System.out.println(test1.hashCode() == test2.hashCode());
		System.out.println();
		System.out.println(test1.equals(test2));
		
		
	}

}
