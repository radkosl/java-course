package academy.samples.workingwithobjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MyTest implements Cloneable  {
	
	private String name;
	
	private Student student;

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public static void  main(String [] atgs) {
		
		
		MyTest mt = new MyTest();
		mt.name = "radko";
		
		Student stud = new Student();
		stud.setFirstName("Radko");
		
		Address add = new Address();
		add.setStreet("Sava mutkruov");
		mt.student = stud;
		
		try {
			MyTest cloning = (MyTest)mt.clone();
			System.out.println(cloning.getName());
			System.out.println(cloning.getStudent().getFirstName());
			System.out.println(cloning);
			System.out.println(mt);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		List<Student> myList = new ArrayList<Student>();
		
		Student s2 = new Student();
		s2.setAge(22);
		s2.setFirstName("zz radko 2");
		s2.setLastName("a");
		myList.add(s2);
		
		Student s1 = new Student();
		s1.setAge(20);
		s1.setFirstName("aa radko");
		s1.setLastName("b");
		myList.add(s1);
		
		Student s3 = new Student();
		s3.setAge(25);
		s3.setFirstName("bbradko 3");
		s3.setLastName("c");
		myList.add(s3);
		
		System.out.println(myList);
		
		Collections.sort(myList);
		
		System.out.println(myList);
		
		LastNameComparator comparator = new LastNameComparator();
		
		Collections.sort(myList, comparator);
		System.out.println(myList);
		FirstNameComparator fnComparator = new FirstNameComparator();
		Collections.sort(myList, fnComparator);
		System.out.println(myList);
	}

	@Override
	public String toString() {
		return "MyTest [name=" + name + "]";
	}

}
