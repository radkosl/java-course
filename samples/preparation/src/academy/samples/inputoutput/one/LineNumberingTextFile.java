package academy.samples.inputoutput.one;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class LineNumberingTextFile {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader("files/input-output/1/some-file.txt"));
		try {
			FileWriter writer = new FileWriter("files/input-output/1/numbered-lines.txt");
			try {
				insertLineNumbers(reader, writer);
			} finally {
				writer.close();
			}
		} finally {
			reader.close();
		}
	}

	private static void insertLineNumbers(BufferedReader reader,
			FileWriter writer) throws IOException {
		int lineNumber = 0;
		while (true) {
			String line = reader.readLine();
			if (line == null) {
				break;
			}
			lineNumber++;
			System.out.println(lineNumber + ". " + line);
			writer.write(lineNumber + ". " + line + "\n");
		}
	}
}
