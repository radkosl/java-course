package academy.samples.inputoutput.two;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class ZipFileTest {

	public static void zipFileTest() throws IOException {
		ZipFile zf = new ZipFile("test.zip");
		Enumeration<? extends ZipEntry> entries = zf.entries();
		while (entries.hasMoreElements()) {
			ZipEntry entry = entries.nextElement();
			System.out.println(entry.getName());
		}
		ZipEntry readme = zf.getEntry("ZipFileTest.java");
		System.out.println(readme.getName());
	}

	public static void zipFileDump() throws IOException {
		ZipInputStream zi = null;
		try {
			zi = new ZipInputStream(new FileInputStream("test.zip"));
			ZipEntry entry = null;
			while ((entry = zi.getNextEntry()) != null) {
				if (entry.getName().matches(".*README.*")) {
					System.out.format("%s:%n", entry.getName());
					Scanner s = new Scanner(zi);
					while (s.hasNextLine()) {
						System.out.println(s.nextLine());
					}
				}
			}
		} finally {
			zi.close();
		}
	}

	public static void randomAccessZip() throws IOException {
		/*
		 * 0 local file header signature 4 bytes (0x04034b50) 2 version needed
		 * to extract 2 bytes 4 general purpose bit flag 2 bytes 6 compression
		 * method 2 bytes 8 last mod file time 2 bytes 10 last mod file date 2
		 * bytes 12 crc-32 4 bytes 16 compressed size 4 bytes 20 uncompressed
		 * size 4 bytes file name length 2 bytes extra field length 2 bytes
		 */
		// 0x504b0304
		RandomAccessFile raf = new RandomAccessFile("test.zip", "r");
		long magic = readLeUnsigned(raf);
		System.out.format("magic: %x%n", magic);
		if (magic != 0x04034b50) {
			throw new IOException("Bad zip file!");
		}
		raf.seek(16);
		long compressed = readLeUnsigned(raf);
		long uncompressed = readLeUnsigned(raf);
		System.out.format("%d -> %d%n", uncompressed, compressed);
		raf.close();

	}

	public static long readLeUnsigned(RandomAccessFile f) throws IOException {
		return (long) f.readUnsignedByte() | (long) f.readUnsignedByte() << 8
				| (long) f.readUnsignedByte() << 16
				| (long) f.readUnsignedByte() << 24;
	}

	public static void bufferedCopyExample() {
		try {
			FileInputStream in = null;
			FileOutputStream out = null;
			try {
				in = new FileInputStream("test.zip");
				out = new FileOutputStream("test_copy.zip");
				byte[] buf = new byte[65536];
				int s;
				while ((s = in.read(buf)) != -1) {
					out.write(buf, 0, s);
				}
				in.close();
				out.close();
			} finally {
				if (in != null) {
					in.close();
				}
				if (out != null) {
					out.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		zipFileTest();
		//zipFileDump();
		//randomAccessZip();
		//bufferedCopyExample();

	}

}
