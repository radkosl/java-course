package academy.samples.inputoutput.two;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class BackupFiles {
	
	public static void backupFiles(File file) throws IOException{
		if(file.isDirectory()){
			for(File f : file.listFiles()){
				backupFiles(f);
			}
		}
		else if(file.getName().endsWith("java")){
			copy(file.getPath(), file.getPath()+".bak");
		}
	}
	public static void copy(String src, String dest) throws IOException{
		InputStream in = null;
		OutputStream out = null;
		try {
			in = new BufferedInputStream(new FileInputStream(src));
			out = new BufferedOutputStream(new FileOutputStream(dest));
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
		} finally {
			if(in != null) {
				in.close();
			}
			if(out != null) {
				out.close();
			}
		}
	}
	public static void main(String[] args) throws IOException {
		backupFiles(new File("."));
	}

}
