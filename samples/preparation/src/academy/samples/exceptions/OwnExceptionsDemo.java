package academy.samples.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class OwnExceptionsDemo {
	
	private static final int MIN_YEAR = 1900;
	private static final int MAX_YEAR = 2010;
	
	private static int readYearFromConsole() throws InvalidYearException {
		Scanner in = new Scanner(System.in);
		int year;
		try {
			year = in.nextInt();
			if ((year < MIN_YEAR) || (year > MAX_YEAR)) {
				throw new InvalidYearException(
					"The number " + year + " is not valid year.");
			}
		} catch (InputMismatchException ex) {
			throw new InvalidYearException(
				"Invalid year!", ex);
		}
		return year;
	}
		
	public static void main(String[] args) {
		try {
			System.out.print("Please enter your year of birth: ");
			int year = readYearFromConsole();
			System.out.println("You entered valid year: " + year);
		} catch (InvalidYearException yearEx) {
			System.err.println("Invalid year: " + yearEx.getMessage());
			yearEx.printStackTrace();
		}
	}
}

class InvalidYearException extends Exception {

	public InvalidYearException(String message) {
		super(message);
	}

	public InvalidYearException(String message, Throwable cause) {
		super(message, cause);
	}
}