package academy.samples.generics;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class GenericCatalog<T> {
	private List<T> catalog = new ArrayList<T>();
  
	public void add(T element) {
		catalog.add(element);
	}
	
	public T get(int index) {
		return catalog.get(index);
	}
	
	public T[] toArray(Class c) {
		T[] result = (T[])Array.newInstance(c, catalog.size());
		result = catalog.toArray(result);
	
		return result;
	}
}
