package academy.samples.xmlprocessing;

import java.io.*;
import java.util.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

/**
   This is a simple class that processes an XML file using a SAX
   parser. Intended to run on a data file like this:

    <?xml version="1.0" encoding="UTF-8"?>
    
    <dots>
      this is before the first dot
      and it continues on multiple lines
      <dot x="9" y="81" />
      <dot x="11" y="121" />
      <flip>
        flip is on
        <dot x="196" y="14" />
        <dot x="169" y="13" />
      </flip>
      flip is off
      <dot x="12" y="144" />
    </dots>
*/
public class SAXExample extends DefaultHandler {
    
    public static final String INPUT_FILE_NAME = "xml/dots.xml";

    /** XML tag strings */
    public static final String DOTS = "dots";
    public static final String DOT = "dot";
    public static final String X = "x";
    public static final String Y = "y";
    public static final String FLIP = "flip";
    
    /** Data model */
    private class Dot {
        int x;
        int y;
        public Dot(int x, int y) { 
        	this.x = x; 
        	this.y = y; 
        }
        public String toString() { 
        	return "(" + x + ", " + y + ")"; 
        }
    }
    
    public List<Dot> dotList = new ArrayList<Dot>();

    /** State variables */
    private int x;
    private int y;
    private boolean flip;

    /** Constructor: initialize state */
    public SAXExample() {
        clear();
    }
    
    /** Clear state. */
    public void clear() {
        x = -1;
        y = -1;
        flip = false;
    }
    
    // SAX ContentHandler methods =====================================
    
    /* Receive notice of start of document. */
    public void startDocument() throws SAXException {
        System.out.println("startDocument");
    }

    /* Receive notice of end of document. */
    public void endDocument() throws SAXException {
        System.out.println("endDocument");
    }
    
    /* Receive notice of start of XML element. */
    public void startElement (String namespaceURI, String localName,
    		String tagName, Attributes atts)
    throws SAXException {
        System.out.println("startElement: " + tagName +
        		" (" + atts.getLength() + " attributes)");
        if (tagName.equals(DOT)) {
            x = Integer.parseInt(atts.getValue(X));
            y = Integer.parseInt(atts.getValue(Y));
            if (flip) {
                int temp = x; 
                x = y; 
                y = temp;
            }
            dotList.add(new Dot(x, y)); // add to data model
        } else if (tagName.equals(FLIP)) {
            flip = true;
        }
    }

    /* Receive notice of end of XML element. */
    public void endElement(String namespaceURI, String localName, String qName)
    throws SAXException {
        System.out.println("endElement:   " + qName);
        if (qName.equals(FLIP)) {
            flip = false;
        }
    }

    /* Receive notice of character data (text not in an XML element). */
    public void characters (char[] ch, int start, int length)
    throws SAXException {
        String s = new String(ch, start, length);
        s = s.trim();
        if (! s.equals(""))
            System.out.println("characters:   " + s);
    }

    // end SAX ContentHandler methods =================================
    
    public static void main (String[] args) throws Exception {
        SAXExample example = new SAXExample();
        FileInputStream inStream = new FileInputStream(INPUT_FILE_NAME);
        try {
	        XMLReader reader = XMLReaderFactory.createXMLReader();
	        reader.setContentHandler(example);
	        reader.parse(new InputSource(new InputStreamReader(inStream)));
	        System.out.println("\nFinished parsing input. Got the following dots:");
	        System.out.println(example.dotList);
        } finally {
        	inStream.close();
        }
    }
  
}
