package academy.samples.xmlprocessing;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XSLTransformDemo {
	public static void main(String[] args) throws TransformerException  {
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer xslTransformer = tFactory.newTransformer(
			new StreamSource("library-xml2html.xsl"));
		xslTransformer.transform(
			new StreamSource("library.xml"),
			new StreamResult("library.html"));
		
		StreamSource s = new StreamSource("library.xml");
	}
}
