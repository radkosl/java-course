package academy.samples.javacollectionframeworks;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;


public class WordFrequency {

	public static void main(String[] args) {
		SortedMap<String, Integer> m = 
			new TreeMap<String, Integer>();
		Scanner scanner = new Scanner(System.in);
		while(scanner.hasNext()){
			String word = scanner.next();
			if(word.equals("stop")) {
				break;
			}
			Integer i = m.get(word);
			m.put(word, i != null ? i + 1 : 1);
		}
		System.out.println(m);
		System.out.println(m.firstKey());
		m.remove(m.firstKey());
		System.out.println(m.headMap("f"));
		
		Queue<Integer> queue = new LinkedList<>();
	}

}
