package academy.samples.javacollectionframeworks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class ReverseLinesStack {

	public static void main(String[] args) {
		Stack<String> stack= new Stack<String>();
		Scanner scanner = null;

		try {
			scanner = new Scanner(new File("files/collections/Reversed.txt"));

			while (scanner.hasNextLine()) {
				stack.push(scanner.nextLine());
			}

			while (stack.size() > 0) {
				System.out.println(stack.pop());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
	}
}
