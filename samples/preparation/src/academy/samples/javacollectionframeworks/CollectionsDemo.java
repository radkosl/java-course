package academy.samples.javacollectionframeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;


public class CollectionsDemo {
	
	
	public static void uniqueNumbers(){
		Set<Integer> s = new HashSet<Integer>();
		Scanner scanner = new Scanner(System.in);
		while(scanner.hasNextInt()){
			s.add(scanner.nextInt());
		}
		System.out.println(s);
		
	}
	
	public static void algorithmsDemo(){
		Integer[] data = {1, 3, 4, 2, 6, 4, 11, 2}; //will not work with int[]
		List<Integer> list = new ArrayList<Integer>(Arrays.asList(data));
		System.out.println(list);

		Collections.sort(list);
		System.out.println("Sorted: " + list);
		
		int pos = Collections.binarySearch(list, 6);
		System.out.println("The position of 3 is " + pos);

		Collections.reverse(list.subList(0, 3));
		System.out.println("First 3 elements reversed: " + list);
		
		Collections.shuffle(list);
		System.out.println("Shuffled: " + list);
		
		Collections.swap(list, 0, 1);
		System.out.println("First 2 elements swapped: " + list);
		
		System.out.println("maximal: " + Collections.max(list));
		
		//something else?
		
	}
	
	public static void reverseText(){
		List<String> l = new LinkedList<String>();
		Scanner scanner = new Scanner(System.in);
		while(scanner.hasNext()){
			l.add(scanner.next());
		}
		Collections.reverse(l);
		
		/*
		for(int i = 0; i < l.size() / 2; ++i){
			String t = l.get(i);
			l.set(i, l.get(l.size() - 1 - i));
			l.set(l.size() - 1 - i, t);
		}*/
		System.out.println(l.toString());
		
	}
	
	public static void main(String[] args){
		algorithmsDemo();
		System.out.println("keep adding numbers");
		uniqueNumbers();
		reverseText();
	}
}
