package academy.samples.javacollectionframeworks;

import java.util.Arrays;
import java.util.Iterator;


public class IteratorDemo {

	public static void main(String[] args) {
		String[] names = {"Milo", "Nakov", "Martin"};
		iterate(Arrays.asList(names).iterator());

	}
	public static void iterate(Iterator i){
		while(i.hasNext()){
			System.out.println(i.next());
		}
	}

}
