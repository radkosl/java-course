package academy.test;

public class AcademyTestExecutor {

	private static String msg = "Test %s has %s. expected: %d, actual: %d";

	public static void execute(AcademyTest test, Object expected,
			Object returned) {

		String output = ">>passed<<";

		if (!expected.equals(returned)) {
			output = ">>failed<<";
		}

		output = String
				.format(msg, test.toString(), output, expected, returned);
		System.out.println(output);

	}

}
