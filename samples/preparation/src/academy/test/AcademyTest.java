package academy.test;

public class AcademyTest {

	private Object source;
	private Object expectedResult;
	
	public AcademyTest(Object source, Object expectedResult) {
		super();
		this.source = source;
		this.expectedResult = expectedResult;
	}
	public Object getSource() {
		return source;
	}
	public void setSource(Object source) {
		this.source = source;
	}
	public Object getExpectedResult() {
		return expectedResult;
	}
	public void setExpectedResult(Object expectedResult) {
		this.expectedResult = expectedResult;
	}
	@Override
	public String toString() {
		return "[input=" + source + ", expected="
				+ expectedResult + "]";
	}
	
	
	
	
}
