package academy.exam.may05_2016;

import java.util.LinkedList;

public class Solutions {

	public static void main(String[] args) {
		
		LinkedList<String> test  = new LinkedList<String>();
		
		test.add("Radko");
		test.add("R1adko");
		test.add("R2adko");
		test.add("Ra 3dko");
		
		String firstElement = (String)test.get(0);
		
		System.out.println(test.size());
		
//		System.out.println(calculateDigitsSequence("3131311111"));
//		System.out.println(calculateDigitsSequence("111311111"));
//		System.out.println(calculateDigitsSequence("1122334455"));
		
	}


	/**
	 * This method receives a parameter string and prints that string as a
	 * diagonal. Every character on new row with 1 space less.
	 * 
	 * @return void
	 */
	public static void printDiagonal(String source) {

		// how many spaces to print in front
		int offset = source.length() - 1;

		// start from the beginning of the string
		for (int i = 0; i < source.length(); i++) {

			// print out the number of spaces for the current row
			for (int j = 0; j < offset; j++) {

				// print empty.(but without new line)
				System.out.print(" ");
			}

			// print the character for that row
			System.out.println(source.charAt(i));

			// decrease the number of spaces for the next row.
			offset--;
		}
	}

	public static int calculateDigitsSequence(String source) {

		// temp sub for the additions on odd indexes
		int tempSum = 0;
		// temp product for the multiplication on the even indexes.
		int tempProduct = 1;

		// from the beginning
		for (int i = 0; i < source.length(); i++) {

			// get the current integer value from the string.
			int value = Integer.parseInt("" + source.charAt(i));

			// check if the index is even
			if (i % 2 == 0) {
				// if even, * current with tempProduct
				tempProduct *= value;
			} else {
				// if odd, + current with tempSum
				tempSum += value;
			}
		}

		// return the sum of tempSum and tempProduct as a result.
		return tempSum + tempProduct;
	}

}
