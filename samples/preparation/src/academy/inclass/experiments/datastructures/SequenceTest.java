package academy.inclass.experiments.datastructures;

import java.util.LinkedList;
import java.util.Queue;

public class SequenceTest {

	public static void main(String[] args) {
		
		int n = 3;
		int p = 16;
		
		Queue<Integer> storage = new LinkedList<Integer>();
		
		storage.offer(n);
		
		int index = 0;
		while(!storage.isEmpty()) {
			
			int current = storage.poll();
			
			if(current == p) {
				System.out.println(index);
				break;
			}
			index++;
			
			storage.offer(current + 1);
			storage.offer(current * 2);
		}
		
		
		
	}

}
