package academy.inclass.experiments.oop;

public class Test {

	public static void main(String[] args) {
		
		
		Circle c = new Circle();
		c.setRadius(5);
		
		Rectangle r = new Rectangle();
		r.setX(5);
		r.setY(10);
		
		System.out.println(c.getSurface());
		System.out.println(r.getSurface());
		
//		showSurfaceOfFigure(c);
//		showSurfaceOfFigure(r);
		
		
		
		
		Tomcat myTomcat = new Tomcat();
		Kitten kitten = new Kitten();
		System.out.println(myTomcat instanceof Cat);
		
		Cat[] myCats = new Cat[2];
		
		myCats[0] = myTomcat;
		myCats[1] = kitten;
		
		catMakeSound(myTomcat);
		catMakeSound(kitten);
		
	}
	
	
	public static void showSurfaceOfFigure(Figure f) {
		System.out.println(f.getSurface());
	}
	
	public static void catMakeSound(Cat cat) {
		cat.makeSound();
	}

}
