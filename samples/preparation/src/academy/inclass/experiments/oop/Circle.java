package academy.inclass.experiments.oop;

public class Circle extends Figure {
	
	private int radius;
	
	public Circle(){
		System.out.println("creating circle");
	}
	
	@Override
	public double getSurface() {
		System.out.println("circle surface");
		return Math.PI * this.radius * this.radius;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	public int showCircle() {
		return this.radius;
	}
}
