package academy.demos.mathandprocess;

public class MathManipulations {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// for testing ...  call the methods here 
	}
	
	/**
	 * Math.pow() функцията се използва за степенуване.
	 * Първия аргумент е числото което искате да степенувате,
	 * Втория аргумент е степента на която искате да го степенувате
	 * @return int
	 */
	public static int calculateSumWithPows(int maxNumber) {

		int sum = 0;
		for(int number = 1 ; maxNumber < 10 ; number++) {
			 
			if(number % 2 == 0) {
				sum = sum + (int)Math.pow(number, 1);
			} else {
				sum = sum + (int)Math.pow(number, 0);
			}
		}

		return sum;
	}

}
