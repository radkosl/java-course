package academy.demos.mathandprocess.paskaltriangle;

public class PaskalTriangle {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		printPaskalTriangle(1);
		printPaskalTriangle(2);
		printPaskalTriangle(3);
		printPaskalTriangle(4);
		printPaskalTriangle(5);
		printPaskalTriangle(6);
		printPaskalTriangle(7);
		printPaskalTriangle(8);
		printPaskalTriangle(9);
		printPaskalTriangle(10);
		printPaskalTriangle(11);
		printPaskalTriangle(12);

	}

	private static void printPaskalTriangle(int level) {
		
		// start from row 1, with empty array
		printRow(1, level, new int[1]);
		
		System.out.println();
		System.out.println("-------------------------------------------------------------------------------------------------------");
		System.out.println();
	}

	private static void printRow(int current, int level, int[] previousRow) {

		// stop printing
		if (current > level + 1) {
			return;
		}
		
		// for every row print the offset first (not perfect)
		String padding = getPadding(2 * (level - current) + 2*2);
		System.out.print(padding);
		
		// what will be the current row so it can be passed to the next one
		int[] newArray = new int[previousRow.length + 1];
		
		// for every element of the previous row.
		for (int i = 0; i < previousRow.length; i++) {
			
			// check for 1 at the beginning and end.
			if (i == 0 || i == previousRow.length - 1) {
				newArray[i] = 1;
				
				// print one at the beginning and end.
				System.out.print(1);
				
				// don't print empty spaces after the last number in the row....
				if (i != previousRow.length - 1) {
					System.out.print(getPadding(3));
				}
				
				continue;
			}
			
			// if not 1, calculate the new value, and add it to the new array
			newArray[i] = previousRow[i - 1] + previousRow[i];
			System.out.print(newArray[i] + getPadding(3));
		}
		
		// make sure we get reference to the new array for the next row.
		previousRow = newArray;
		
		if (current <= level) {
			// print new line before rendering the next row
			System.out.println();
			System.out.println();
		}
		// print next row using the newly created array
		printRow(current + 1, level, previousRow);
	}

	private static String getPadding(int numberOfSpaces) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < numberOfSpaces; i++) {
			sb.append(" ");
		}
		return sb.toString();
	}

}
