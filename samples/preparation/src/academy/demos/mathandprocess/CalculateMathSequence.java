package academy.demos.mathandprocess;

public class CalculateMathSequence {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println(CalculateMathSequence.calculateMathSequence(8,2));
		System.out.println(CalculateMathSequence.calculateMathSequence(5,2));

	}
	
	/**
	 * 1 + 1!/x^1 + 2!/x^2 + 3!/x^3 ....
	 */
	private static int calculateMathSequence(int count, int devider) {
		
		int N = count;
		int x = devider;

		// starting sum
		int sum = 1;
		int currentFactorialValue = 1;
		int currentPowValue = x;

		for(int i = 1 ; i <= N; i++) {
		   System.out.println("index is " + i);
		   sum  = sum + currentFactorialValue / currentPowValue;
		   System.out.println("New Temporary Sum is " + sum);
		   currentFactorialValue = currentFactorialValue * (i + 1);
		   System.out.println("Updated factorial is " + currentFactorialValue);
		   currentPowValue = currentPowValue * x;
		   System.out.println(" Updated Pow is " + currentFactorialValue);
		   System.out.println(" -------------- Next Iteration -------------- ");
		}
		
		return sum;
	}
	

}
