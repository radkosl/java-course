package academy.demos.mathandprocess;

public class NumbersTriangle {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// for testing ...  call the methods here 
		
		printNumbersTriangle(2);
	}
	
	public static void printNumbersTriangle(int level) {
		
		// start the recursion from the first row.
		printNextRow(1, level);
		
	}
	
	private static void printNextRow(int currentRow, int maxLevel) {
		
		// bottom of the recursion. If we reach our goal row number
		if(currentRow > maxLevel) {
			return;
		}
		
		// print current row
		
		// how many spaces have to printed in front
		int offset = maxLevel - currentRow;
		
		// display these spaces
		for(int offsetIndex = 0 ; offsetIndex < offset ;  offsetIndex++) {
			System.out.print("  ");
		}
		
		// Print the first part of the row, where it is incrementing from 1 to currentRow.
		for(int increment = 1 ; increment < currentRow ; increment ++) {
			System.out.print(increment + " ");
		}
		
		// Print the second part of the row, where it is decrementing from currentRow to 1.
		for(int decrement = currentRow ; decrement > 0 ; decrement --) {
			System.out.print(decrement + " ");
		}
		
		// print empty line so next row is in the line
		System.out.println();
		
		// we finish printing the current row,
		// lets go and print the next row.
		printNextRow(currentRow + 1, maxLevel);
		
	}

}
