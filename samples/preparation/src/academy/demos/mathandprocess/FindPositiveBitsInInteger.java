package academy.demos.mathandprocess;

public class FindPositiveBitsInInteger {

	public static void main(String[] args) {
		
		System.out.println(findPositiveBitsCount(5));
		System.out.println(findPositiveBitsCount(6));
		System.out.println(findPositiveBitsCount(7));
		System.out.println(findPositiveBitsCount(8));
		System.out.println(findPositiveBitsCount(9));
		System.out.println(findPositiveBitsCount(10));
		System.out.println(findPositiveBitsCount(11));
		System.out.println(findPositiveBitsCount(12));
		System.out.println(findPositiveBitsCount(13));
		
		System.out.println("Another implementation");
		System.out.println(findPositiveBitsCountWithDevision(5));
		System.out.println(findPositiveBitsCountWithDevision(6));
		System.out.println(findPositiveBitsCountWithDevision(7));
	}

	private static int findPositiveBitsCountWithDevision(int i) {
		int count = 0;
		int source = i;
		while(source > 0) {
			if(source % 2 == 1) {
				count++;
			}
			source = source / 2;
		}
		return count;
	}

	private static int findPositiveBitsCount(int i) {
		
		String binary = Integer.toBinaryString(i);
		
		System.out.println("Sequence of bits is " + binary + " ... Now extract the '1' s");
		int count = 0;
		for(char bit : binary.toCharArray()) {
			
			if(bit == '1') {
				count ++;
			}
		}
		return count;
	}

}
