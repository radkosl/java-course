package academy.demos.mathandprocess;

public class SwapVariablesValues {

	public static void main(String []args ){
		
		
		int a = -11;
		int b = -5;
		
		a = a - b;
		b = a + b;
		a = b - a;
		
		System.out.println(a);
		System.out.println(b);
	}

}
