package academy.demos.mathandprocess;

public class Factorial {

	public static void main(String[] args) {
		
		System.out.println(factorialRecursive(2));
		System.out.println(factorialIterative(2));
		
		System.out.println(factorialRecursive(3));
		System.out.println(factorialIterative(3));
		
		System.out.println(factorialRecursive(4));
		System.out.println(factorialIterative(4));
		
		System.out.println(factorialRecursive(5));
		System.out.println(factorialIterative(5));
		
		System.out.println(factorialRecursive(6));
		System.out.println(factorialIterative(6));
		
	}
		
	
	public static int factorialRecursive(int source) {
		if( source == 1) {
			return source;
		}
		return source * factorialRecursive(source - 1);
	}
	
	/**
	 * This method will calculate the factorial for input number.
	 */
	public static int factorialIterative(int source) {
		int result = 1;
		for(int currentNumber = 1 ; currentNumber <= source ; currentNumber = currentNumber + 1) {
			result = result * currentNumber;
		}
		
		return result;
	}

}
