package academy.demos.mathandprocess;

public class FindRectangleSides {

	public static void main(String[] args) {
		
		
		findSides(10);
		System.out.println("-----");
		findSides(20);
		System.out.println("-----");
		findSides(14);
		System.out.println("-----");
		findSides(13);
		
	}

	private static void findSides(int surface) {
		
		for(int i = 1 ; i <= surface; i ++) {
			
			if(surface % i == 0) {
				System.out.println(i + " and " + surface / i);
			}
		}
		
	}

}
