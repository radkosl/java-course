package academy.demos.inhouse.beforeeaster;

/**
 * All methods of this class will solve particular problem
 * That class doesn't have state. :)
 * 
 * @author radkol
 */
public class Exercises {

	/**
	 * Task 1. (Beginner)
	 * Write a method that has 1 param of type String
	 * return a String that put the first 2 characters of the source string at the end.
	 * Example source "ABCDEF", result is "CDEFAB"
	 */
	 public static String transformString(String source) {
		 String firstPart = source.substring(0, 2);
		 String secondPart = source.substring(2);
		 return  secondPart + firstPart;
	 }
	
	
	/**
	 * Task 2. (Beginner)
	 * Write a method that has 1 param an array of Strings to hold string values like this:
	 * ##Name (f|m)## where the value will be 'f' if the name is a woman's name, and 'm' if the name is mens name.
	 * Example:
	 * Beyonce (f)
	 * David Bowie (m)
	 * Madonna (f)
	 * Elton John (m)
	 * Write a method to loop and count how many are male vocalists and how many are female.
	 * Print both counted values for man and woman
	 */
	 public static void countSingers(String[] singers) {
		 
		 int countMales = 0;
		 int countWomens = 0;
		 
		 for(String singer : singers) {
			 
			 if(singer.indexOf("(m)") > 0) {
				 countMales = countMales + 1;
			 } else if (singer.indexOf("(f)") > 0) {
				 countWomens = countWomens + 1;
			 }
			 
		 }
		 
		 System.out.println("Male count is " + countMales);
		 System.out.println("Woman count is " + countWomens);
		 
	 }
	
	/**
	 * Task 3. (Intermediate)
	 * Write a method that has 1 param an array of Strings to hold string values for musical instruments:
	 * Example: 
	 * cello
	 * guitar
	 * violin
	 * double bass
	 * Loop through that array and remove the vowels of every musical instrument. Print each value with removed vowels.
	 */
	 public static void removeVowels(String[] instruments, String pattern) {
		 
		 
		 //[aueyio]
		 for(String instrument : instruments) {
			 
			 String[] parts = instrument.split(pattern);
			 
			 String currentResult = "";
			 for(String part : parts) {
				 currentResult = currentResult + part;
			 }
			 
			 System.out.println(currentResult);
			 
		 }
		 
		 
	 }
	 
	 
	/**
	 * Task 4. (Beginner)
	 * Write a method that has 1 @param of type int called 'n'
	 * Write a program to count how many numbers between 1 and n are divisible by 3 with no remainder
	 * return that number from the method.
	 */
	 public static int countNumbers(int n) {
		 
		 int resultCounter = 0;
		 
		 for(int i = 1; i < n; i = i + 1) {
			 
			 if(i % 3 == 0) {
				 resultCounter++;
			 }
		 }
		 
		 return resultCounter;
		 
		 
	 }
	
	/**
	 * Task 5. (Intermediate to Advanced)
	 * Write code to create a checker board pattern with the words "black" and "white" standing in for colours
	 * black white black white black while .....
	 * white black white black ........
	 * black white black white black while .....
	 * white black white black ........
	 * and so on ...
	 * Print out the result
	 */
	 public static void generateCheckerBoard() {
		 
		 String evenColor = "w ";
		 String oddColor = "b ";
		 
		 for(int index = 0 ; index < 8 ; index++) {
			 
			 if(index % 2 == 0) {
				 evenColor = "w ";
				 oddColor = "b ";
			 } else {
				 evenColor = "b ";
				 oddColor = "w ";
			 }
			 
			 for (int i = 0; i < 8; i++) {
				 
				 if((i % 2 == 0)) {
					 System.out.print(evenColor);
				 } else {
					 System.out.print(oddColor);
				 }
			}
			 
			 
			// end row
			 System.out.println();
		 }

		 
	 }
	 
	
	/**
	 * Task 6 (Beginner)
	 * For every number on a dartboard (1 to 20), work out the possible single, double and treble scores. 
	 * (For example, the number 20 has the possible score 20, 40 and 60.)
	 * Print out the possibilities for every number.
	 */
	 public static void printDartsValues() {
		 
		 for(int i = 1 ; i <= 20 ; i ++) {
			 
			 System.out.println(i + " "  + i * 2 + " " + i * 3);
			 
		 }
		 
		 
	 }
	 
	 
	/**
	 * Task 7 (Intermediate)
	 * Write a program to work out if a series of 5 digits are consecutive numbers.
	 * String has to be parameter of the method:
	 * Example strings that return true:
	 * "9-8-7-6-5";
	 * "1-2-3-4-5";
	 * Example strings that return false:
	 * "6-1-8-7-6"
	 * "6-9-8-7-1"
	 * return true | false if the numbers are sequential or not..
	 */
	public static boolean isSeriesConsecutive(String source) {
		
		// get all digits by splitting by -
		String[] digits = source.split("[-]");
		
		// in order to be consecutive, the sequence should be
		// increasing or decreasing properly by 1
		
		// first check if it is increasing sequence.
		boolean increateSeq = true;
		// iterate over all digits and prove that it is increasing.
		// if not, change the flag 'increaseSeq' to false;
		for(int i = 0 ; i < digits.length - 1; i ++) {
			int currentNumber = Integer.parseInt(digits[i]);
			int nextNumber = Integer.parseInt(digits[i+1]);
			if( currentNumber + 1 != nextNumber) {
				increateSeq = false;
				break;
			}
		}
		
		// if we found that the sequence is increasing, just return true
		if(increateSeq) {
			return true;
		}
		
		// check if it is decreasing sequence.
		boolean decreaseSeq = true;
		// iterate over all digits and prove that it is decreasing.
		// if not, change the flag 'decreaseSeq' to false;
		for(int i = 0 ; i < digits.length - 1; i ++) {
			int currentNumber = Integer.parseInt(digits[i]);
			int nextNumber = Integer.parseInt(digits[i+1]);
			if(currentNumber - 1 != nextNumber) {
				decreaseSeq = false;
				break;
			}
		}
		
		// if we found that the sequence is decreasing, just return true
		if(decreaseSeq) {
			return true;
		}
		
		// the sequence is nor decreasing, neither increasing so return false
		return false;
	}
	/**
	 * Task 8 (Easy to Intermediate)
	 * Print the word diagonally, diagonally from left to right, top to bottom.
	 * Eg. "academy" =>
	 * a
	 *  c
	 *   a
	 *    d
	 *     e
	 *      m
	 *       y
	 */
	 public static void printWordDiagonally(String word) {
		 
		 for(int i = 0 ; i < word.length() ; i ++) {
			 
			 for(int spaceIndex = 0 ; spaceIndex < i ; spaceIndex ++) {
				 System.out.print(" ");
			 }
			 
			 System.out.println(word.charAt(i));
			 
		 }
		 
	 }
	 
	/**
	 * Task 9 (Intermediate to Advanced)
	 * 
	 * Method that has 2 arguments of type String - 1 for team name 1 for results
	 * Write a programme to work out how many wins Manchester United had, how many games they drew, and how many Manchester United lost.
       Extend the programme to work out how many goals Manchester United scored and how many they conceded.
       
	 * Example team name
	 * Manchester United
	 * 
	 * Example source scores
	 * "Manchester United 1 Chelsea 0, Arsenal 1 Manchester United 1, Manchester United 3 Fulham 1, Liverpool 2 Manchester United 1, Swansea 2 Manchester United 4";
	 */
	 public static void showTeamStatistic(String teamName, String[] matches) {
		 
		 int winsCount = 0;
		 int drawsCount = 0;
		 int totalGoals = 0;
		 int totalConceded = 0;
		 
		 // we can use index of "teamName". If at the beginning the looked team is the first, other wise it is the second.
		 
		 for(String match : matches) {
			 
			 int teamPosition = match.indexOf(teamName);
			 // for every match look for the search team.
			 // that team is not in the current match, do nothing
			 if(teamPosition < 0) {
				 continue;
			 }
			 
			 int teamGoals = 0;
			 int otherTeamGoals = 0;
			 
			 // first team in the match is our team
			 if(teamPosition == 0) {
				 teamGoals = Integer.parseInt("" +match.charAt(teamName.length()+1));
				 otherTeamGoals = Integer.parseInt("" + match.charAt(match.length()-1));
			 } else {
				 // our team is the second position 
				 // team goals are at the end
				 teamGoals = Integer.parseInt("" +match.charAt(match.length()-1));
				 // other team goals are in 
				 otherTeamGoals = Integer.parseInt("" + match.charAt(teamPosition-2));
		
			 }
			 
			 totalGoals += teamGoals;
			 totalConceded += otherTeamGoals;
			 if(teamGoals > otherTeamGoals) {
				 winsCount++;
			 } else if(teamGoals == otherTeamGoals) {
				 drawsCount++;
			 }
			 
		 }
		 
		 System.out.printf("Total Goals Scored for %s : %d \n", teamName, totalGoals);
		 System.out.printf("Total Goals Conceded for %s : %d \n", teamName, totalConceded);
		 System.out.printf("Wins for %s : %d \n", teamName, winsCount);
		 System.out.printf("Draws for %s : %d \n", teamName, drawsCount);
		 
		 
	 }
	
	 
}
