package academy.demos.imagecrawler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleImageCrawler {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Enter the url where you want to download images from");
		String url = input.nextLine();

		try {
			System.out.println("Downloading images into downloads/image-crawler");
			URLConnection connection = getConnection(url);
			StringBuilder content = getContent(connection);
			Set<String> imageAddresses = extractImages(content);
			for (String imageLocation : imageAddresses) {
				downloadImage(imageLocation);
			}
		} catch (Exception ex) {
			System.out.println("Cannot perform download operation");
			ex.printStackTrace();
		}

		input.close();

	}

	private static void downloadImage(String imageLocation) {

		try {
			String fileName = imageLocation.substring(imageLocation.lastIndexOf('/') + 1);
			URL url = new URL(imageLocation);
			InputStream in = new BufferedInputStream(url.openStream());
			OutputStream out = new BufferedOutputStream(new FileOutputStream("downloads/image-crawler/" + fileName));

			for (int i; (i = in.read()) != -1;) {
				out.write(i);
			}
			in.close();
			out.close();
		} catch (Exception ex) {
			System.out.println("Cannot download image at: [" + imageLocation + "]");
		}
	}

	private static Set<String> extractImages(StringBuilder content) {

		int fromIndex = 0;
		String source = content.toString();
		Set<String> result = new HashSet<>();
		while (fromIndex >= 0) {
			int startImageTag = source.indexOf("<img", fromIndex);
			if (startImageTag < 0)
				break;
			int endImageTag = source.indexOf(">", startImageTag);
			if (startImageTag >= 0 && endImageTag >= 0 && endImageTag > startImageTag) {
				String currentImageTag = source.substring(startImageTag, endImageTag).trim();
				Pattern pattern = Pattern.compile("src=['|\"]([\\w:\\./]+)['|\"]");
				Matcher matcher = pattern.matcher(currentImageTag);
				if (matcher.find()) {
					String imageUrl = matcher.group(1);
					System.out.println(String.format("Finding image url [%s]", imageUrl));
					result.add(imageUrl);
				}
			}

			fromIndex = endImageTag;
		}

		return result;
	}

	private static URLConnection getConnection(String url) throws IOException {
		URL location = new URL(url);
		URLConnection connection = location.openConnection();
		connection.addRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
		return connection;
	}

	private static StringBuilder getContent(URLConnection connection) throws IOException {
		StringBuilder result = new StringBuilder();
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line = null;
		while ((line = in.readLine()) != null) {
			result.append(line.trim());
		}
		in.close();
		System.out.println(result);
		return result;
	}

}
