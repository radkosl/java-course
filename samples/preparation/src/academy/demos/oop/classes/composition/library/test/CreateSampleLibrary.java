package academy.demos.oop.classes.composition.library.test;

import academy.demos.oop.classes.composition.library.Author;
import academy.demos.oop.classes.composition.library.Book;
import academy.demos.oop.classes.composition.library.Library;

public class CreateSampleLibrary {

	public static void main(String[] args) {
		
		// create 1 library
		Library lib = new Library();
		lib.setLocation("Plovdiv");
		lib.setName("Ivan Vazov Library");
		
		// create 3 authors. 
		Author auth1 = new Author("Radko Lyutskanov", 27);
		Author auth2 = new Author("Ivan Ivanov", 55);
		Author auth3 = new Author("Todor Todorov", 43);
		
		// create books
		Book book1 = new Book();
		book1.setAuthor(auth1);
		book1.setGenre("Programming");
		book1.setNumberOfPages(1555);
		book1.setTitle("Learn How to Programming");
		
		Book book2 = new Book();
		book2.setAuthor(auth2);
		book2.setGenre("Drama");
		book2.setNumberOfPages(459);
		book2.setTitle("Into the wild");
		
		Book book3 = new Book();
		book2.setAuthor(auth3);
		book2.setGenre("Documentary");
		book2.setNumberOfPages(1200);
		book2.setTitle("World War 2");
		
		Book[] books = new Book[]{book1, book2 , book3};
		lib.setBooks(books);
		
		lib.displayCatalog();

	}

}
