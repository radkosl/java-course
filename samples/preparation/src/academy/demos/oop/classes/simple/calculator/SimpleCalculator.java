package academy.demos.oop.classes.simple.calculator;

public class SimpleCalculator {

	private int currentValue;
	
	public void add(int newValue) {
		currentValue = currentValue + newValue;
	}
	
	public void substract(int newValue) {
		currentValue = currentValue - newValue;
	}
	
	public void multiply(int newValue) {
		currentValue = currentValue * newValue;
	}
	
	public int calculate() {
		int curvalue = this.currentValue;
		this.currentValue = 0;
		return curvalue;
	}

}


