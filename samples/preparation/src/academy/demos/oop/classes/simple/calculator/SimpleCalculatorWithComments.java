package academy.demos.oop.classes.simple.calculator;

/**
 * That class represents calculator which can do the following
 * Adding
 * Subtracting
 * multiplying
 * Dividing
 *  
 * Lets say we would like to do that
 * add(3).substract(5).multiply(4).calculate();
 * Same way Windows calculator works!!!
 */
public class SimpleCalculatorWithComments {

	/**
	 * We hide the state of the calculator.
	 * No one can touch it!
	 */
	private int currentValue;
	
	/**
	 * Add value to the current value we have.
	 * Then return the entire object so we can call
	 * other operations on it !!!
	 */
	public SimpleCalculatorWithComments add(int addition) {
		this.currentValue += addition;
		return this;
	}
	
	/**
	 * Substract value from the current value we have.
	 * Then return the entire object so we can call
	 * other operations on it !!!
	 */
	public SimpleCalculatorWithComments substract(int substraction) {
		this.currentValue -= substraction;
		return this;
	}
	
	/**
	 * multiply value with the current value we have.
	 * Then return the entire object so we can call
	 * other operations on it !!!
	 */
	public SimpleCalculatorWithComments multiply(int multiplyBy) {
		this.currentValue *= multiplyBy;
		return this;
	}
	
	/**
	 * Devide current value  by passed devider.
	 * Then return the entire object so we can call
	 * other operations on it with the new result (state) !!!
	 */
	public SimpleCalculatorWithComments divide(int divideBy) {
		this.currentValue /= divideBy;
		return this;
	}
	
	/**
	 * Show what we have calculated so far.
	 * This is just returning the hidden currentValue state variable
	 * which has been changing in each operations.
	 * After we return the result, reset that value so we can use the calculator again
	 * @return
	 */
	public int calculate() {
		int result = this.currentValue;
		this.currentValue = 0;
		//System.out.println("We return the result from all previous operations, and we reset the state of the calculator, so we can use it again !!!");
		return result;
	}
	
}
