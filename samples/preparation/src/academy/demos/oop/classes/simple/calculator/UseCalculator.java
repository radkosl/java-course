package academy.demos.oop.classes.simple.calculator;

public class UseCalculator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// lets create my calculator
		SimpleCalculatorWithComments calculator = new SimpleCalculatorWithComments();
		
		int beforeDoingAnything = calculator.calculate();
		System.out.println("Beginning calculator with value " + beforeDoingAnything);
		
		// now lets see what our calculator can do
		calculator.add(10);
		calculator.add(11);
		calculator.multiply(2);
		calculator.substract(100);
		System.out.println(calculator.calculate());
		
		// same as this
		calculator = calculator.add(10);
		calculator = calculator.add(11);
		calculator = calculator.multiply(2);
		calculator = calculator.substract(100);
		System.out.println(calculator.calculate());
		
		//since we are returning the current calculator after every operation, we can chain them
		int calculate = calculator.add(10).add(11).multiply(2).substract(100).calculate();
		System.out.println(calculate);
	}

}
