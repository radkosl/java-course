package academy.demos.oop.classes.simple.employee;

public class Employee {

	// state

	private String name;
	private int age;
	private double salary;

	public Employee() {

	}

	public String getName() {
		return this.name;
	}

	public void setName(String newName) {
		this.name = newName;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int newAge) {
		this.age = newAge;
	}

	public double getSalary() {
		return this.salary;
	}

	public void setSalary(double newSalary) {
		this.salary = newSalary;
	}

	public double calculateHourRate() {

		double hourRate = (this.getSalary() / 21) / 8;
		return hourRate;

	}

//	@Override
//	public String toString() {
//		return "employee";
//	}

}