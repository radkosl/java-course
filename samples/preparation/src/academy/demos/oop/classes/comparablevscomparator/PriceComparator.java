package academy.demos.oop.classes.comparablevscomparator;

import java.util.Comparator;

public class PriceComparator implements Comparator<Movie> {

	@Override
	public int compare(Movie o1, Movie o2) {
		if(o1.getPrice() > o2.getPrice()) {
			return 1;
		} else {
			return -1;
		}
	}

}
