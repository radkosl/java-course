package academy.demos.oop.classes.comparablevscomparator;

public class Movie implements Comparable<Movie> {

	private String title;
	
	private int rating;
	
	private Double price;
	
	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public int compareTo(Movie o) {
		if(this.title.compareTo(o.title) > 0) {
			return 1;
		} else {
			return -1;
		}
	}


	@Override
	public String toString() {
		return "Movie [title=" + title + ", rating=" + rating + ", price=" + price + "]";
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	
	
	
}
