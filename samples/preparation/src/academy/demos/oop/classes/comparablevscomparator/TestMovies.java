package academy.demos.oop.classes.comparablevscomparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestMovies {

	public static void main(String[] args) {
		
		List<Movie> movies = new ArrayList<Movie>();
		
		Movie m1 = new Movie();
		m1.setRating(10);
		m1.setTitle("Armagedon");
		m1.setPrice(105d);
		movies.add(m1);
		
		m1 = new Movie();
		m1.setRating(20);
		m1.setPrice(111d);
		m1.setTitle("3 Armagedon");
		
		movies.add(m1);
		
		m1 = new Movie();
		m1.setRating(5);
		m1.setTitle("2 Armagedon");
		m1.setPrice(10115d);
		movies.add(m1);
		
		System.out.println(movies);
		
		Collections.sort(movies);
		
		System.out.println(movies);
		
		Collections.sort(movies, new RatingComparator());
		
		System.out.println(movies);
		
		Collections.sort(movies, new PriceComparator());
		
		System.err.println(movies);
	}

}
