package academy.demos.designpatterns.observer;

public class Run {

	public static void main(String[] args) {
		
		Model model = new Model();
		model.setName("Normal Name");
		
		new LowerCaseNameObserver(model);
		new UpperCaseNameObserver(model);
		
		model.notifyObservers();

	}

}
