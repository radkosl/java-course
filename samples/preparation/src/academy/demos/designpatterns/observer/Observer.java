package academy.demos.designpatterns.observer;

public abstract class Observer {
	
	private Model toObserve;
	
	public abstract void observe();
	
	public Observer(Model m) {
		this.toObserve = m;
		m.addObserver(this);
	}

	protected Model getModel() {
		return toObserve;
	}


}
