package academy.demos.designpatterns.observer;

public class UpperCaseNameObserver extends Observer{
	
	public UpperCaseNameObserver(Model m) {
		super(m);
	}

	@Override
	public void observe() {
		System.out.println(getModel().getName().toUpperCase());
	}
}
