package academy.demos.designpatterns.observer;

import java.util.ArrayList;
import java.util.List;

public class Model {
	
	private List<Observer> observers = new ArrayList<>();
	
	private String name;
	
	public void addObserver(Observer o) {
		observers.add(o);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void notifyObservers() {
		for(Observer o : observers) {
			o.observe();
		}
	}	
	
}
