package academy.demos.designpatterns.visitor;

public class Run {
	
	public static void main(String[] args) {
		
		new Zoo().accept(new DefaultAnimalVisitor());

	}

}
