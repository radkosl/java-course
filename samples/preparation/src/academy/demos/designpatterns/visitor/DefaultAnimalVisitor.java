package academy.demos.designpatterns.visitor;

public class DefaultAnimalVisitor implements AnimalVisitor {

	@Override
	public void visit(Cat cat) {
		System.out.println("Cat visited");
	}

	@Override
	public void visit(Dog dog) {
		System.out.println("Dog visited");
	}

}
