package academy.demos.database.simple;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SimpleDatabaseConnectionTest {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		// load the mysql driver
		// we have included the library in lib folder in the project
		// to the classpath of the project
		Class.forName("com.mysql.jdbc.Driver");
		
		// now that we have the driver,
		// we want to create the connection to the database
		
		// try to create connection to specific database.
		// we need to have the database server running
		// also the default user account for the database is root with no password
		// when u have installed xampp.
		Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/classicmodels", "root", "");
		
		// get all products from the products database
		// the result object is from class ResultSet which is some sort of Iterator.
		// we can iterate over the rows in the table
		
		Statement statement = conn.createStatement();
		ResultSet resultSet = statement.executeQuery("select productCode, buyPrice, productVendor from products");
		
		// move the iterator until the end.
		
		// we can use resultSet.getString(int column) OR resultSet.getString("columnLabel");
		// same for double etc.
		List<Product> products = new ArrayList<>();
		while(resultSet.next()) {
			
			Product p = new Product();
			p.setName(resultSet.getString("productCode"));
			p.setPrice(resultSet.getDouble("buyPrice"));
			p.setVendor(resultSet.getString("productVendor"));
			products.add(p);
			
			//System.out.printf("Product Code (%s), Product Price (%.3f), Vendor (%s) %n", resultSet.getString("productCode") , resultSet.getDouble("buyPrice"), resultSet.getString("productVendor"));
			
			
		}
		
		for(Product p : products) {
			System.out.println(p.toString());
		}
		
	}

}
