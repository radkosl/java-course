package academy.demos.database.complex;

public class ConnectionDetails {

	private final String host;
	private final String name;
	private final String user;
	private final String pass;

	private ConnectionDetails(String host, String name, String user, String pass) {
		this.host = host;
		this.name = name;
		this.user = user;
		this.pass = pass;
	}

	public String getHost() {
		return host;
	}

	public String getName() {
		return name;
	}

	public String getUser() {
		return user;
	}

	public String getPass() {
		return pass;
	}

	public static Host builder() {
		return new ConnectionDetailsBuilder();
	}

	public static interface Builder {
		ConnectionDetails build();
	}

	public static interface Host {
		Name host(String host);
	}

	public static interface Name {
		User name(String name);
	}

	public static interface User {
		Pass user(String user);
	}

	public static interface Pass {
		Builder pass(String pass);
	}

	private static class ConnectionDetailsBuilder implements Builder, Host, Name, User, Pass {

		private String host;
		private String name;
		private String user;
		private String pass;

		@Override
		public Builder pass(String pass) {
			this.pass = pass;
			return this;
		}

		@Override
		public Pass user(String user) {
			this.user = user;
			return this;
		}

		@Override
		public User name(String name) {
			this.name = name;
			return this;
		}

		@Override
		public Name host(String host) {
			this.host = host;
			return this;
		}

		@Override
		public ConnectionDetails build() {
			return new ConnectionDetails(host, name, user, pass);
		}

	}

}
