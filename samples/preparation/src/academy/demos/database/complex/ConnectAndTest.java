package academy.demos.database.complex;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class ConnectAndTest {

	public static void main(String[] args) throws Exception {

		loadMySQLDBDriver();
		
		Connection conn = getConnection();
		
		ResultSet resultSet = conn.createStatement().executeQuery("SELECT * FROM customers");
		
		while(resultSet.next()) {
			
			System.out.printf("ID:%s, NAME:%s, PHONE:%s, CITY:%s, POSTCODE:%s ", 
					resultSet.getString("customerNumber"),
					resultSet.getString("customerName"),
					resultSet.getString("phone"),
					resultSet.getString("city"),
					resultSet.getString("postalCode"));
			System.out.println();
			
		}
		
	}
	
	private static void loadMySQLDBDriver() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
	}
	
	private static Connection getConnection() throws Exception {
		// over engineered solution. :)
		ConnectionDetails details = 
			ConnectionDetails.builder().
			host(ConfigurationLoader.get("db.host")).
			name(ConfigurationLoader.get("db.name")).
			user(ConfigurationLoader.get("db.user")).
			pass(ConfigurationLoader.get("db.pass")).
			build();
		String connectionString = "jdbc:mysql://%s/%s";
		connectionString = String.format(connectionString, details.getHost(), details.getName());
		
		Connection con = DriverManager.getConnection(connectionString, details.getUser(), details.getPass());
		return con;
	}

}
