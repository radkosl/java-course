package academy.demos.strings.romannumbers;

public class RomanNumber {
	
	private char romanKey;
	private int intValue;
	
	public RomanNumber() {}
	
	public RomanNumber(char romanKey, int decimalValue) {
		super();
		this.romanKey = romanKey;
		this.intValue = decimalValue;
	}
	
	public char getRomanKey() {
		return romanKey;
	}
	
	public void setRomanKey(char romanKey) {
		this.romanKey = romanKey;
	}
	
	public int getIntValue() {
		return intValue;
	}
	
	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}

	@Override
	public String toString() {
		return "(" +romanKey + ","
				+ intValue + ")";
	}
	
}
