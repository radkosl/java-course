package academy.demos.streams.conversation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Run {

	public static void main(String[] args) {
		
		Conversation c1 = new Conversation();
		c1.count = 5;
		Conversation c2 = new Conversation();
		c2.count = 20;
		Conversation c3 = new Conversation();
		c3.count = 50;
		
		MessageReceiver mr1 = new MessageReceiver();
		mr1.seen = true;
		MessageReceiver mr2 = new MessageReceiver();
		MessageReceiver mr3 = new MessageReceiver();
		
		Message m1 = new Message();
		m1.receivers = Arrays.asList(mr1, mr2);
		
		Message m2 = new Message();
		m2.receivers = Arrays.asList(mr1, mr2);
		
		Message m3 = new Message();
		m3.receivers = Arrays.asList(mr1, mr2, mr3);
		
		c1.messages = Arrays.asList(m1 , m2);
		c2.messages = Arrays.asList(m1, m2 , m3);
		
		
		List<Conversation> conversations = Arrays.asList(c1, c2);
		
		Conversation cc1 = conversations.get(1);
		
		long unseen = cc1.messages.stream().map(message -> {
			return message.receivers.stream().filter(receiver -> {
				return !receiver.seen;
			}).count();
			
		}).mapToLong(Long::new).sum();
		
		System.out.println(unseen);
		
		
		List<Conversation> list = Arrays.asList(c2, c3, c1);
		
		list = list.stream().sorted(new Comparator<Conversation>(){

			@Override
			public int compare(Conversation o1, Conversation o2) {
				return o1.count - o2.count;
			}
			
		}).collect(Collectors.toList());
		
		System.out.println(list);
	}

}
