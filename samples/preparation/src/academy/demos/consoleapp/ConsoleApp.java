package academy.demos.consoleapp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import academy.demos.consoleapp.domain.Product;

public class ConsoleApp {

	public static void main(String[] args) throws IOException {
		
		Scanner input = new Scanner(System.in);
		
		List<Product> products = loadProducts();
		// start the program
		
		// show the menu
		
		displayProducts(products);
		
		menu();
		
		while(true) {
			
			// what option do you want
			String currentInput = input.nextLine();
			
			// exit point if we type EXIT
			if(currentInput.equals("4")) {
				break;
			} else 
			
			// add product for 1
			if(currentInput.equals("1")) {
				addProduct(input);
			} else 
			
			// add coins for 2
			if(currentInput.equals("2")) {
				addCoins(input);
			} else 
			
			// purchase for 3
			if(currentInput.equals("3")) {
				purchase(input);
			} else {
				System.out.println("Invalid Option. Try again...\n");
			}
			
			menu();
			
		}
		
		System.out.println("End of program");
		
	}
	
	private static void displayProducts(List<Product> products) {
		
		String template = "%d, %s, %s, %d";
		
		for(Product p : products) {
			System.out.println(String.format(template, p.getId(), p.getName(), p.getPrice(), p.getQuantity()));
		}
		
	}

	private static void purchase(Scanner input) {
		System.out.println("Purchase now");
		System.out.println("Yee your order is successful");
	}

	private static void addCoins(Scanner input) {
		System.out.println("Add Coins now...\n");
		String coins = input.nextLine();
		System.out.println("Ok do something now here with this coins...." + coins);
	}

	public static void addProduct(Scanner input) {
		System.out.println("Add the product ID now...\n");
		String productId = input.nextLine();
		
		System.out.println("Ok do something now here with this ID...." + productId);
	}
	
	public static void menu() {
		System.out.println("[1]add product , [2]add coins, [3]purchase items, [4]Exit \n");
	}
	
	public static List<Product> loadProducts() throws IOException {
		
		List<Product> products = new ArrayList<>();
		// load the products
		BufferedReader reader = new BufferedReader(new FileReader("vm/product.config"));
		
		//1;Coca Cola;1.25;100
		String line;
		while((line = reader.readLine()) != null) {
			
			String[] lineParts = line.split("[;]");
			Product p = new Product();
			p.setId(Integer.parseInt(lineParts[0]));
			p.setName(lineParts[1]);
			p.setPrice(Double.parseDouble(lineParts[2]));
			p.setQuantity(Integer.parseInt(lineParts[3]));
			products.add(p);
		}
		
		reader.close();
		
		return products;
	}
	
	
}
