package practice.crackingthecodeinterview.intro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BinarySearch {

	public static void main(String[] args) {
		
		List<int[]> tests = new ArrayList<int[]>(){{
			
			add(new int[]{1});
			add(new int[]{1, 5, 2});
			add(new int[]{1, 2, 3});
			add(new int[]{1, 4, 3, 2, 8});
		}};
		tests.forEach(array -> {
			System.out.println(binarySearch(array, 2));
		});
	
		
	}

	private static int binarySearch(int[] is, int i) {
		if(is == null || is.length == 0) {
			return -1;
		}
		if(is.length == 1) {
			return is[0] == i ? 0 : -1;
		}
		Arrays.sort(is);
		return internalSearch(is, i , 0, is.length);
	}

	private static int internalSearch(int[] arr, int search, int start, int end) {
		if(arr.length == 0) {
			return -1;
		}
		int middle = (start + end) / 2;
		if(arr[middle] == search) {
			return middle;
		}
		if(arr[middle] > search) {
			return internalSearch(arr, search, 0, middle);
		} else {
			return internalSearch(arr, search, middle, end);
		}
	}

}
