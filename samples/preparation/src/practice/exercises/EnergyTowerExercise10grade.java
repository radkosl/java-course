package practice.exercises;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EnergyTowerExercise10grade {

	public static void main(String[] args) {
		
		Object name = "name";
		Object value = 100;
		String t = String.format("Field `%s` with value: `%s` is not in the valid format", name, value.toString());
		System.out.println(t);
		t = String.format("Field `%s` with value: `%s` is too long", name, value.toString());
		System.out.println(t);
		
		Pattern p = Pattern.compile("_([0-9]{2,10})x([0-9]{2,10})(.*)");
		
		//System.out.println(p.matcher("r4223er334_43W2432432_10101x200").matches());
		
		String pat = "^.+/images/originals/.+$";
		String url = "https://static.stage.kindlink.com/kindlink-stage/images/originals/L2yn8b8Dsu_1497901073690.png";
		Matcher m = p.matcher("_200x300.png");
		
		
		System.out.println(url.matches(pat));
		if(m.matches()) {
			String id = m.group(1);
			String width = m.group(2);
			String height = m.group(3);
			System.out.println(id);
			System.out.println(width);
			System.out.println(height);

		}
	
		
		boolean test = true; 
		if(test) {
			return;
		}
		
		//int [] towers = new int[]{10,9,7,6,3,4,5,2,1};
		
		int [][] inputs = new int[][] {
			
			{10,9,7,6,3,4,5,2,1},
			{4,7,6,2,12,11,5,3,1},
			{4,7,6,2,12,11,3,5},
			{7,6,5,4,3,2,1,12,11},
			{7,6,5,4,3,2,12,11,9,1},
			{3,2,1},
			{15,12,7,9,13,12,11,10},
		};
		
		int towerHeight = 8;
		
		for (int[] input : inputs) {
			
			int max = Integer.MIN_VALUE;
			int current = 0;
			for (int i = 0; i < input.length - 1; i++) {
				
				if (input[i] < towerHeight) {
					current++;
				}
				
			}
			
		}
	}


	private static boolean last(int[] towers, int i) {
		return i == towers.length - 1;
	}
}
