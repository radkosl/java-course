package lecture6.arrays;


public class PrintArrys {

	public static void main(String[] args) {
		String[] array = { "one", "two", "three", "four" };

		
		for(int index = 0 ; index < array.length ; index++ ) {
			System.out.println(array[index]);
		}
		System.out.println("----");
		for(String currentValue : array) {
			System.out.println(currentValue);
		}
		
		
//		// Process all elements of the array
//		for (int index = 0; index < array.length; index++) {
//			// Print each element on a separate line
//			System.out.printf("element[%d] = %s%n", index, array[index]);
//		}
	}

}
