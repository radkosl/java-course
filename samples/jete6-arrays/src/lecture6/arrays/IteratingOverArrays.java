package lecture6.arrays;


public class IteratingOverArrays {

	public static void main(String[] args) {
		// Create and initialize a 10-element int array
		int[] array = new int[10];

		// Set each element to hold its index
		for (int index = 0; index < array.length; index++) {
			array[index] = index;
		}

		
		
		
		// Print the array
		System.out.println("Original array: ");
		for (int number : array) {
			System.out.print(number + " ");
		}
		System.out.println();
		
		int arrLength = array.length;
		
		for(int index = 0 ; index < arrLength ; index ++) {
			if(index % 2 != 0) {
				array[index] = -1;
			}
		}
		
		// Assign value -1 to the odd elements
//		for (int index = 1; index < array.length; index += 2) {
//			array[index] = -1;
//		}

		// Print the array
		System.out.println("Modified array: ");
		for (int number : array) {
			System.out.print(number + " ");
		}
		System.out.println();
	}

}
