package lecture6.arrays;

import java.util.Scanner;

public class CheckSymmetry {

	public static void main(String[] args) {
//		Scanner input = new Scanner(System.in);
//		System.out.print("Number of elements = ");
		int n = 1500000;
		
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = i + 1;
		}
		
		boolean symmetric = true;
		for (int i = 0; i < (arr.length + 1) / 2; i++) {
			if (arr[i] != arr[n - i - 1]) {
				symmetric = false;
				break;
			}
		}
		System.out.printf("Symmetric? %b%n", symmetric);
	}

}
