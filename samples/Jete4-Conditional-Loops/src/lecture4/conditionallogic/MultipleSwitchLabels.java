package lecture4.conditionallogic;
import java.util.Scanner;

public class MultipleSwitchLabels {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Please enter some number in the range [1..10]: ");
		int number = input.nextInt();

		switch (number) {
		case 1:
		case 4:
		case 6:
		case 8:
		case 9:
		case 10:
			System.out.println("Number is NOT prime.");
			break;
		case 2:
		case 3:
		case 5:
		case 7:
			System.out.println("Number is prime.");
			break;
		default:
			System.out.println("I don't know this number!");
			break;
		}
	}

}
