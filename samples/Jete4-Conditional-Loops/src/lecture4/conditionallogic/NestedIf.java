package lecture4.conditionallogic;
import java.util.Scanner;

public class NestedIf {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter two numbers (on separate lines).");

		int first = input.nextInt();
		int second = input.nextInt();

		if (first == second) {
			
			if(first % 10 == 0) {
				System.out.println("They are also devisable by 10");
			}
			
			System.out.println("These two numbers are equal.");
		} else {
			if (first > second) {
				System.out.println("The first number is greater.");
			} else {
				System.out.println("The second number is greater.");
			}
		}
	}

}
