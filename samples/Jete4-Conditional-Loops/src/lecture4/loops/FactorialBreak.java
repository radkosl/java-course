package lecture4.loops;

import java.util.Scanner;

public class FactorialBreak {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("n = ");
		int n = input.nextInt();

		System.out.print("n! = ");

		// "long" is the biggest integer type
		long factorial = 1;
		
		boolean test = true;
		// Perform an infinite loop
		while (test) {
			System.out.print(n);
//			if (n == 1) {
//				break;
//			}
			System.out.print(" * ");
			factorial = factorial * n;
			n = n - 1;
		}
		System.out.println(" = " + factorial);		
	}

}
