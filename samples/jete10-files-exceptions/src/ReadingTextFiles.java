import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadingTextFiles {

	public static void main(String[] args) throws FileNotFoundException {

		String fileName = System.getProperty("user.dir")
				+ "\\src\\ReadingTextFiles.java";
		File file = new File(fileName);
		Scanner input = new Scanner(file, "windows-1251");

		int lineNumber = 0;
		while (input.hasNextLine()) {
			lineNumber++;
			System.out.printf("Line %d: %s%n", lineNumber, input.nextLine());
		}
		input.close();
	}

}
