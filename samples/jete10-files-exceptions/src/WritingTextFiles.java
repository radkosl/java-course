import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class WritingTextFiles {

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		PrintStream fileOutput = new PrintStream("numbers.txt" , "windows-1251");
		for (int number = 1; number <= 20; number++) {
			fileOutput.println(number);
		}
		fileOutput.close();
	}

}
