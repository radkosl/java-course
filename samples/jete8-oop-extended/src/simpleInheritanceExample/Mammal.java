package simpleInheritanceExample;

//Base class
public class Mammal {
	private int age;

	protected static int m = 2;

	public Mammal(int age) {
		this.age = age;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void sleep() {
		System.out.println("shhh! I'm sleeping.");
	}

}
