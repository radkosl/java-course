package polymorphismExample;

public class MainClass {

	public static void main(String[] args) {
		Creature[] creatures = new Creature[] { new Dog(), new Cat() , new Creature()};

		for (Creature animal : creatures) {
			System.out.printf("%s : ", animal.getClass());
			animal.speak();
		}
	}

}
