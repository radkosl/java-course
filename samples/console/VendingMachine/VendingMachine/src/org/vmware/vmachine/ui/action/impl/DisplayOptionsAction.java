package org.vmware.vmachine.ui.action.impl;

import static org.vmware.vmachine.util.GUIUtility.displayOperations;
import static org.vmware.vmachine.util.GUIUtility.print;
import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;

/**
 * List the available operations action
 * @author radko
 *
 */
public class DisplayOptionsAction implements Action {

	@Override
	public void performAction(UIContext c) {
		displayOperations();
		print("");
	}

}
