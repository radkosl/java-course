package org.vmware.vmachine.util;

import java.io.IOException;
import java.io.Reader;
import java.util.Collection;
import java.util.Map;

import org.vmware.vmachine.entity.Product;
import org.vmware.vmachine.enums.CoinType;
/**
 * Utility for vm GUI interface.
 * Provides screen message display and 
 * some helper methods.
 * @author radko
 *
 */
public class GUIUtility {

	public static String withPadding(String value, int padding) {
		int difference = padding - value.length();
		StringBuilder sb = new StringBuilder(value);
		for (int i = 0; i < difference; i++) {
			sb.append(" ");
		}
		return sb.toString();
	}
	
	public static void displayChangeResult(Map<CoinType,Integer> result) {
		print(String.format("%s | %s", withPadding("Coin",6), withPadding("Quantity",9)));
		for(CoinType ct : result.keySet()) {
			print(String.format("%s | %s", withPadding(String.valueOf(ct.value()),6),
										   withPadding(String.valueOf(result.get(ct).intValue()),9) ));
		}
	}
	
	public static void displayProducts(Collection<Product> c) {
		print(String.format("%s | %s | %s",
				withPadding("-Id-", 5), withPadding("-Name-", 15),
				withPadding("-Price-", 10)));
		for (Product p : c) {
			print(String.format("%s | %s | %.2f",
					withPadding(String.valueOf(p.getId()), 5),
					withPadding(p.getName(), 15), p.getPrice()));
		}
	}

	public static void displayBasket(Map<Product, Integer> basket) {
		print(String.format("%s | %s | %s | %s",
				withPadding("-Id-", 5), withPadding("-Name-", 15),
				withPadding("-Qty-", 5), withPadding("-Price-", 10)));
		for (Product p : basket.keySet()) {
			System.out
					.println(String.format("%s | %s | %s | %.2f",
							withPadding(String.valueOf(p.getId()), 5),
							withPadding(p.getName(), 15),
							withPadding(String.valueOf(basket.get(p)), 5),
							p.getPrice()));

		}
	}

	public static void displayOperations() {
		int offset = 25;
		print(String.format("%s %s %s",
				withPadding("[1]Add Product", offset),
				withPadding("[2]Remove Product", offset),
				withPadding("[3]Select Payment Type", offset)));
		print(String.format("%s %s %s",
				withPadding("[4]Insert Coins", offset),
				withPadding("[5]Show Products", offset),
				withPadding("[6]Show Basket", offset)));
		print(String.format("%s %s %s",
				withPadding("[7]Show Inserted Amount", offset),
				withPadding("[8]Show Basket Amount", offset),
				withPadding("[9]Place Order", offset)));
		print(String.format("%s or type 'EXIT'",
				withPadding("[10]Cancel Order", offset)));
	}

	public static void print(String value) {
		System.out.println(value);
	}
	
	public static void showPrice(double price) {
		print(String.format("Value : %.2f", price));
	}
	
	public static void displayPaymentTypes(Collection<String> types) {
		for (String type : types) {
			print(type);
		}
	}
	
	public static int getValidInteger(Reader input) {
		int value = 0;
		char[] data = new char[32];
		try {
			input.read(data);
			value = Integer.valueOf(getValueFromInput(data));
		} catch (IOException e) {
			print("Cannot read from console");
		} catch (NumberFormatException e) {
			print("Please enter valid number");
		}
		if (value < 0) {
			print("Please enter positive number");
			value = 0;
		}
		return value;
	}
	
	public static String readLine(Reader input) {
		char[] data = new char[64];
		try {
			input.read(data);
			return getValueFromInput(data);
		} catch (IOException e) {
			print("Cannot read from console");
		}
		return null;
	}
	
	private static String getValueFromInput(char[] input) {
		return String.valueOf(input).trim();
	}
}
