package org.vmware.vmachine.entity.payment;

import java.util.Map;

import org.vmware.vmachine.enums.CoinType;
import org.vmware.vmachine.enums.ResultStatus;

/**
 * Order level Result Message
 * 
 * @author radko
 */
public class OrderResult {

	private ResultStatus status;
	private String message;
	private String messageForChange;
	private Map<CoinType, Integer> change;
	private boolean changeAvailable;

	public Map<CoinType, Integer> getChange() {
		return change;
	}

	public void setChange(Map<CoinType, Integer> change) {
		this.change = change;
	}

	public boolean isChangeAvailable() {
		return changeAvailable;
	}

	public void setChangeAvailable(boolean changeAvailable) {
		this.changeAvailable = changeAvailable;
	}

	public ResultStatus getStatus() {
		return status;
	}

	public void setStatus(ResultStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageForChange() {
		return messageForChange;
	}

	public void setMessageForChange(String messageForChange) {
		this.messageForChange = messageForChange;
	}
}
