package org.vmware.vmachine.ui.action.impl;

import static org.vmware.vmachine.util.GUIUtility.displayProducts;
import static org.vmware.vmachine.util.GUIUtility.print;
import static org.vmware.vmachine.util.GUIUtility.getValidInteger;

import java.io.Reader;

import org.vmware.vmachine.entity.Product;
import org.vmware.vmachine.exception.InvalidQuantityException;
import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;

/**
 * Represents add product user action
 */
public class AddProductAction implements Action {

	@Override
	public void performAction(UIContext c) {
		int value = 0;
		Reader input = c.getInput();
		print("Please type the 'id' of the product you want to buy: ");
		print("");
		//product selection
		Product p = null;
		while(value == 0) {
			value = getValidInteger(input);
			if(value == 0) continue;
			if((p = c.getVmMachine().getProductById(value)) == null) {
				System.out.println("There is no product with 'id' " + value + ". Please enter again.");
				displayProducts(c.getVmMachine().showAllProducts());
				value = 0;
			}
		}
		//quantity for that product
		print(String.format("Please enter quantity for product : %s",p.getName()));
		value = -1;
		while(value == -1) {
			value = getValidInteger(input);
			try {
				c.getVmMachine().addProduct(p, value);
			} catch (InvalidQuantityException e) {
				System.out.println(e.getMessage());
				value = -1;
			}
		}
	}	

}
