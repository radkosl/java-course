package org.vmware.vmachine.paymentProviders.impl;

import org.vmware.vmachine.entity.OrderState;
import org.vmware.vmachine.entity.payment.PaymentResult;
import org.vmware.vmachine.enums.ResultStatus;
import org.vmware.vmachine.exception.PaymentProcessException;
import org.vmware.vmachine.paymentProviders.PaymentProvider;

/**
 * Card payment dummy provider
 * @author Radec
 *
 */
public class CARDPaymentProvider implements PaymentProvider{

	@Override
	public PaymentResult processPayment(OrderState orderState) throws PaymentProcessException {
		
		//DUMMY IMPLEMENTATION
		PaymentResult status = new PaymentResult();
		status.setMessage("Cannot pay via CARD. Please choose another payment method.");
		status.setStatus(ResultStatus.NOK);
		status.setChangeReturnNeeded(false);
		return status;
	}


}
