package org.vmware.vmachine.ui.action.impl;

import static org.vmware.vmachine.util.GUIUtility.displayBasket;
import static org.vmware.vmachine.util.GUIUtility.print;

import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;

/**
 * Represents basket status request action
 *
 */
public class DisplayBasketAction implements Action {

	@Override
	public void performAction(UIContext c) {
		
		displayBasket(c.getVmMachine().showBasket());
		print("");
	}

}
