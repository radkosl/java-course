package org.vmware.vmachine.entity;

/**
 * Represents products in the @see Storage <br>
 * Available for purchase in @see VendingMachine
 * @author Radec
 *
 */
public class Product implements Comparable<Product>{
	
	/**
	 * Unique identifier
	 */
	int id;
	/**
	 * Product name
	 */
	String name;
	/**
	 * Product price
	 */
	double price;

	public Product(int id, String name, double price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public int compareTo(Product o) {
		if(o == null) return 0;
		if(this.getId() > o.getId()) return 1;
		if(this.getId() < o.getId()) return -1;
		
		return 0;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	
	@Override
	public boolean equals(Object o) {
		
		if(!(o instanceof Product)) return false;
		
		return this.getId() == ((Product)o).getId();
	}

}
