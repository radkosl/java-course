package org.vmware.vmachine.enums;

/**
 * Represents operation outcome status
 * @author Radec
 *
 */
public enum ResultStatus {
	OK("Success"), NOK("Error");

	private final String strRepresentation;

	private ResultStatus(String status) {
		strRepresentation = status;
	}

	public String getStatus() {
		return strRepresentation;
	}
	
	@Override
	public String toString() {
		return strRepresentation;
	}
}
