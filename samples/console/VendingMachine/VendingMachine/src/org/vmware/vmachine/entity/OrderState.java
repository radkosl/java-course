package org.vmware.vmachine.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.vmware.vmachine.entity.payment.PaymentDetails;
import org.vmware.vmachine.enums.CoinType;
import org.vmware.vmachine.util.GeneralUtility;

/**
 * Represents the state of the order of the current user. 
 * - selected payment type.
 * - selected products / quantities 
 * - basket
 * - coins added
 * - payment details
 */
public class OrderState {

	/**
	 * current products in the customer's basket
	 */
	Map<Product, Integer> basket;

	/**
	 * current coins inserted by the customer. If any
	 */
	private Map<CoinType, Integer> coins;

	/**
	 * selected payment type.
	 */
	String selectedPaymentType;
	
	/**
	 * Payment details for the customer
	 */
	private PaymentDetails paymentDetails;
	
	private Map<CoinType,Integer> change;
	
	public OrderState() {
		init();
	}
	
	private void init() {
		basket = new HashMap<Product, Integer>();
		coins = new HashMap<CoinType,Integer>();
		selectedPaymentType = GeneralUtility.getDefaultPaymentType();
		paymentDetails = new PaymentDetails();
	}
	
	public Map<Product, Integer> getBasket() {
		return basket;
	}

	public void setBasket(Map<Product, Integer> basket) {
		this.basket = basket;
	}

	public String getSelectedPaymentType() {
		return selectedPaymentType;
	}

	public void setSelectedPaymentType(String selectedPaymentType) {
		this.selectedPaymentType = selectedPaymentType;
	}

	public Map<CoinType, Integer> getCoins() {
		return coins;
	}

	public void setCoins(Map<CoinType, Integer> coins) {
		this.coins = coins;
	}
	
	public void reset() {
		init();
	}

	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}
	
	public double getTotalBasketValue(){
		double sum = 0d;
		for(Product p : basket.keySet()) {
			sum += p.getPrice() * basket.get(p);
		}
		return sum;
	}
	
	public double getTotalInsertedCash() {
		double sum = 0d;
		for(CoinType ct : coins.keySet()) {
			sum += ct.value() * coins.get(ct);
		}
		return sum;
	}

	public Map<CoinType,Integer> getChange() {
		return change;
	}

	public void setChange(Map<CoinType,Integer> change) {
		this.change = change;
	}
}
