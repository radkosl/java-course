package org.vmware.vmachine.exception;

@SuppressWarnings("serial")
public class InvalidCoinValueException extends Exception {
	private static final String TEMPLATE_MESSAGE = "There is no coin with value %s";

	public InvalidCoinValueException(String value) {
		super(String.format(TEMPLATE_MESSAGE, value));
	}
}
