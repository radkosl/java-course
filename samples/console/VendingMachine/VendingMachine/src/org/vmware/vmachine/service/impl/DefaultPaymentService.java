package org.vmware.vmachine.service.impl;

import org.vmware.vmachine.entity.OrderState;
import org.vmware.vmachine.entity.payment.PaymentResult;
import org.vmware.vmachine.exception.OrderProcessException;
import org.vmware.vmachine.exception.PaymentProcessException;
import org.vmware.vmachine.factory.PaymentProviderFactory;
import org.vmware.vmachine.paymentProviders.PaymentProvider;
import org.vmware.vmachine.service.PaymentService;

/**
 * Implementation of payment service.
 * @author radko
 */
public class DefaultPaymentService implements PaymentService{

	@Override
	public PaymentResult pay(OrderState orderState) throws OrderProcessException {
		//get the provider based on the selected payment type.
		PaymentProvider pProvider = PaymentProviderFactory.getPaymentProvider(orderState.getSelectedPaymentType());
		if(pProvider == null) {
			throw new OrderProcessException("Cannot find Payment provider. Try different payment method.");
		}
		try {
			return pProvider.processPayment(orderState);
		} catch (PaymentProcessException e) {
			throw new OrderProcessException(e.getReason());
		}
	}

}
