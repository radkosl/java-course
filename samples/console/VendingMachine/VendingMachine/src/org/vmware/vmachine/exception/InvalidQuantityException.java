package org.vmware.vmachine.exception;

@SuppressWarnings("serial")
public class InvalidQuantityException extends Exception {

	private static final String TEMPLATE_MESSAGE = "Requested invalid quantity %d for Product %s. Max allowed : %d";
	
	public InvalidQuantityException(String pName,int quantity,int available) {
		super(String.format(TEMPLATE_MESSAGE, quantity,pName,available));
	}
	
}
