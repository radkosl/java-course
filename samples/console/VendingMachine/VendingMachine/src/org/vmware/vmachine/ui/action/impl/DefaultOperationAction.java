package org.vmware.vmachine.ui.action.impl;

import static org.vmware.vmachine.util.GUIUtility.print;

import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;

/**
 * Represents invalid operation selection action
 */
public class DefaultOperationAction implements Action {

	@Override
	public void performAction(UIContext c) {
		
		print("Invalid selected options. Please select again");
		print("");
	}

}
