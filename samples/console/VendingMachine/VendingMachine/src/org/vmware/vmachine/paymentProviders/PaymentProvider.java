package org.vmware.vmachine.paymentProviders;

import org.vmware.vmachine.entity.OrderState;
import org.vmware.vmachine.entity.payment.PaymentResult;
import org.vmware.vmachine.exception.PaymentProcessException;

/**
 * Payment Provider abstraction
 * @author radko
 *
 */
public interface PaymentProvider {

	/**
	 * Process the payment based on the @see OrderState
	 */
	PaymentResult processPayment(OrderState orderState) throws PaymentProcessException; 
	
}
