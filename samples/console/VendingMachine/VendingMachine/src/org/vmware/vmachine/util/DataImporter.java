package org.vmware.vmachine.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.vmware.vmachine.entity.Product;
import org.vmware.vmachine.enums.CoinType;
import org.vmware.vmachine.main.Storage;

/**
 * Class for loading products and coins in the @see Storage.
 * Created to simulate machine maintenance process.
 * @author radko
 */
public class DataImporter {
	private static final String PRODUCT_IMPORT = "products.properties";
	private static final String COINS_IMPORT = "coins.properties";
	
	public static void importProducts(Storage s) {
		try {
			BufferedReader reader = new BufferedReader(
									new InputStreamReader(
									new FileInputStream(GeneralUtility.getDataImportPath(PRODUCT_IMPORT))));
			String line;
			while((line = reader.readLine()) != null) {
				String [] data = line.split("[;]");
				s.loadProduct(new Product(Integer.valueOf(data[0]), data[1], Double.valueOf(data[2])), Integer.valueOf(data[3]));
			}
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void importCoins(Storage s) {
		try {
			BufferedReader reader = new BufferedReader(
									new InputStreamReader(
									new FileInputStream(GeneralUtility.getDataImportPath(COINS_IMPORT))));
			String line;
			while((line = reader.readLine()) != null) {
				String [] data = line.split("[;]");
				s.loadCoins(CoinType.valueOf(data[0]), Integer.valueOf(data[1]));
			}
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
