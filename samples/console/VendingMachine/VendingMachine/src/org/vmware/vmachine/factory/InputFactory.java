package org.vmware.vmachine.factory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
/**
 * Factory for returning @see Reader implementation <br>
 * based on @see InputStream
 * @author Radec
 *
 */
public class InputFactory {

	public static Reader createReader(InputStream is) {
		return new InputStreamReader(is);
	}
}
