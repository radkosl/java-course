package org.vmware.vmachine.ui.action.impl;

import static org.vmware.vmachine.util.GUIUtility.getValidInteger;
import static org.vmware.vmachine.util.GUIUtility.print;
import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;

/**
 * Remove product from basket action
 * @author radko
 */
public class RemoveProductAction implements Action{

	@Override
	public void performAction(UIContext c) {
		print("Please type the 'id' of the product you want to remove: ");
		//product selection
		int value = 0;
		while(value == 0) {
			value = getValidInteger(c.getInput());
			if(value == 0) continue;
		}
		c.getVmMachine().removeProduct(value);
	}

}
