package org.vmware.vmachine.ui.action.impl;

import static org.vmware.vmachine.util.GUIUtility.displayPaymentTypes;
import static org.vmware.vmachine.util.GUIUtility.print;
import static org.vmware.vmachine.util.GUIUtility.readLine;

import org.vmware.vmachine.exception.InvalidPaymentType;
import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;

/**
 * Select payment type action
 * @author radko
 *
 */
public class SetPaymentTypeAction implements Action{

	@Override
	public void performAction(UIContext c) {
		print("");
		print("Select payment type: ");
		print("");
		displayPaymentTypes(c.getVmMachine().getPaymentTypes());
		
		while(true) {
			try {
				String type = readLine(c.getInput());
				c.getVmMachine().setPaymentType(type);
				break;
			} catch (InvalidPaymentType e) {
				print(e.getMessage());
			}
		}
	}

}
