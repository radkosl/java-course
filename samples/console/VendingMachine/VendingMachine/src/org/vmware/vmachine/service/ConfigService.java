package org.vmware.vmachine.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.vmware.vmachine.util.GeneralUtility;

/**
 * Service for reading configuration data
 * @author Radec
 */
public final class ConfigService {
	
	private static final ConfigService INSTANCE =  new ConfigService();
	private final Map<String,String> configs = new HashMap<String,String>();
	
	private ConfigService() {
		if(INSTANCE != null) {
			throw new IllegalStateException("Cannot re-instantiate singleton class");
		}
		load();
	}
	
	private void load() {
		try {
			String configFilePath = GeneralUtility.getAppConfigFilePath();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(configFilePath)));
			String line = null;
			while((line = reader.readLine()) != null) {
				String[] currentVal = line.split("[=]");
				configs.put(currentVal[0], currentVal[1]);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println("Cannot load configuration file");
		} catch (IOException e) {
			System.out.println("Cannot read configuration file");
		}	
	}

	public static final ConfigService getInstance() {
		return INSTANCE;
	}
	
	public String getProperty(String key) {
		return configs.get(key);
	}

}
