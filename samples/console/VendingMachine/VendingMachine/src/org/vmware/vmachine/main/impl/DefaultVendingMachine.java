package org.vmware.vmachine.main.impl;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.vmware.vmachine.converter.CoinConverter;
import org.vmware.vmachine.converter.OrderResultConverter;
import org.vmware.vmachine.entity.OrderState;
import org.vmware.vmachine.entity.Product;
import org.vmware.vmachine.entity.payment.ChangeReturnResult;
import org.vmware.vmachine.entity.payment.OrderResult;
import org.vmware.vmachine.entity.payment.PaymentResult;
import org.vmware.vmachine.enums.CoinType;
import org.vmware.vmachine.enums.ResultStatus;
import org.vmware.vmachine.exception.CannotCalculateChangeException;
import org.vmware.vmachine.exception.InvalidCoinValueException;
import org.vmware.vmachine.exception.InvalidPaymentType;
import org.vmware.vmachine.exception.InvalidQuantityException;
import org.vmware.vmachine.exception.OrderProcessException;
import org.vmware.vmachine.factory.PaymentServiceFactory;
import org.vmware.vmachine.factory.StorageFactory;
import org.vmware.vmachine.handler.ChangeReturnHandler;
import org.vmware.vmachine.main.Storage;
import org.vmware.vmachine.main.VendingMachine;
import org.vmware.vmachine.service.PaymentService;
import org.vmware.vmachine.util.GeneralUtility;

/**
 * Implementation for @see VendingMachine
 * @author Radec
 *
 */
public class DefaultVendingMachine implements VendingMachine {

	private final Storage vmStorage;
	private final OrderState customerOrderState;
	
	public DefaultVendingMachine() {
		vmStorage = StorageFactory.getStorage();
		customerOrderState = new OrderState();
	}
	
	@Override
	public Set<Product> showAllProducts() {
		return vmStorage.getAvailableProducts();
	}

	@Override
	public void addProduct(Product product, int qty) throws InvalidQuantityException {
		
		Map<Product, Integer> currentBasket = customerOrderState.getBasket();

		// check if customer is trying to update the quantity for existing
		// product in the basket.
		if (currentBasket.containsKey(product)) {
			qty += currentBasket.get(product);
		}
		if (vmStorage.isProductAvailable(product, qty)) {
			currentBasket.put(product, qty);
		} else {
			throw new InvalidQuantityException(product.getName(), qty, vmStorage.getAvailableQuantity(product));
		}

	}

	@Override
	public void addCoins(String type, int qty) throws InvalidCoinValueException {
		CoinType ct = CoinConverter.convert(type);
		Map<CoinType, Integer> addedCoins = customerOrderState.getCoins();
		if (addedCoins.containsKey(type)) {
			addedCoins.put(ct, addedCoins.get(type) + qty);
		} else {
			addedCoins.put(ct, qty);
		}
	}

	@Override
	public OrderResult cancelOrder() {
		OrderResult orderResult = new OrderResult();
		orderResult.setStatus(ResultStatus.OK);
		orderResult.setMessage("Successful cancelation of the order. Please start again.");
		orderResult.setMessageForChange(String.format("Change returned: %.2f",customerOrderState.getTotalInsertedCash()));
		updateMachineState();
		
		return orderResult;
	}

	private void updateMachineState() {
		customerOrderState.reset();
	}
	
	private void updateStoreState(boolean productDecrease,boolean coinDecrease) {
		transferProducts(customerOrderState.getBasket(), productDecrease);
		transferCoins(customerOrderState.getCoins(), coinDecrease);
		
	}
	
	private void transferCoins(Map<CoinType, Integer> coins, boolean decrease) {
		for (CoinType type : coins.keySet()) {
			vmStorage.loadCoins(type,
					decrease ? -coins.get(type) : coins.get(type));
		}
	}

	private void transferProducts(Map<Product, Integer> basket, boolean decrease) {
		for (Product p : basket.keySet()) {
			vmStorage.loadProduct(p, decrease ? -basket.get(p) : basket.get(p));
		}
	}

	@Override
	public OrderResult processOrder() throws OrderProcessException {
		PaymentService paymentService = PaymentServiceFactory.getPaymentService();
		PaymentResult pResult  =  paymentService.pay(customerOrderState);
		OrderResult orderResult = OrderResultConverter.convert(pResult);
		
		if(orderResult.isChangeAvailable()) {
			ChangeReturnHandler changeHandler = new ChangeReturnHandler(
													customerOrderState.getTotalBasketValue(),
													customerOrderState.getTotalInsertedCash(),
													vmStorage.getAvailableCoins());
			try {
				ChangeReturnResult crResult = changeHandler.calculateChange();
				if(ResultStatus.NOK.equals(crResult.getResultStatus())) {
					throw new OrderProcessException(crResult.getMessage());
				}
				orderResult.setChange(crResult.getReturnedCoins());
				orderResult.setMessageForChange(crResult.getMessage());
				
				//remove the change coins from the machine.
				transferCoins(crResult.getReturnedCoins(),true);
			} catch (CannotCalculateChangeException ccce) {
				throw new OrderProcessException(ccce.getMessage());
			}
		}
		
		if(orderResult.getStatus().equals(ResultStatus.OK)) { 
			updateStoreState(true,false);
			updateMachineState();
		}
		return orderResult;
	}

	@Override
	public Collection<String> getPaymentTypes() {
		return GeneralUtility.getPaymentTypes();
	}

	@Override
	public void setPaymentType(String paymentType) throws InvalidPaymentType {
		boolean updated = false;
		for(String type : getPaymentTypes()) {
			if(type.equalsIgnoreCase(paymentType)) {
				updated = true;
				customerOrderState.setSelectedPaymentType(paymentType);
				break;
			}
		}
		if(!updated) {
			throw new InvalidPaymentType(paymentType);
		}
	}

	@Override
	public Product getProductById(int id) {
		return vmStorage.getProductById(id);
	}

	@Override
	public Map<Product,Integer> showBasket() {
		return customerOrderState.getBasket();
	}

	@Override
	public double showBasketTotal() {
		return customerOrderState.getTotalBasketValue();
	}

	@Override
	public double showCashTotal() {
		return customerOrderState.getTotalInsertedCash();
	}

	@Override
	public void removeProduct(int id) {
		Product forRemoval = getProductById(id);
		if(forRemoval != null) {
			customerOrderState.getBasket().remove(forRemoval);
		}
	}


}
