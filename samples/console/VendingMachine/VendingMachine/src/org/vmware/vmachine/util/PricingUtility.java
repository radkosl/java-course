package org.vmware.vmachine.util;

public class PricingUtility {

	public static int intCoinValue(double value) {
		return Double.valueOf(Math.round(value * 100)).intValue();
	}
}
