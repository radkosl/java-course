package org.vmware.vmachine.ui.context;

import java.io.Reader;

import org.vmware.vmachine.main.VendingMachine;

/**
 * Represents Application context.
 * Requires @see VendingMachine instance <br>
 * Requires @see Reader instance
 */
public class UIContext {
	
    //hide the constructor
	UIContext(){}
	
	private VendingMachine vmMachine;
	private Reader input;
	
	public void setVmMachine(VendingMachine vmMachine) {
		this.vmMachine = vmMachine;
	}

	public void setInput(Reader input) {
		this.input = input;
	}

	public VendingMachine getVmMachine() {
		return vmMachine;
	}

	public Reader getInput() {
		return input;
	}

}
