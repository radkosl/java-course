package org.vmware.vmachine.converter;

import java.util.ArrayList;
import java.util.LinkedList;

import org.vmware.vmachine.enums.CoinType;
import org.vmware.vmachine.exception.InvalidCoinValueException;

/**
 * Product @see CoinType from String
 * 
 * @author Radec
 *
 */
public class CoinConverter {

	public static CoinType convert(String type) throws InvalidCoinValueException{
		try {
			int value = Integer.parseInt(type);
			for(CoinType cType : CoinType.values()) {
				if(cType.valueInCoins() == value) {
					return cType;
				}
			}
		} catch (Exception e) {}
		
		throw new InvalidCoinValueException(type);
	}

}
