package org.vmware.vmachine.ui.action.impl;

import static org.vmware.vmachine.util.GUIUtility.print;
import static org.vmware.vmachine.util.GUIUtility.showPrice;
import org.vmware.vmachine.ui.action.Action;
import org.vmware.vmachine.ui.context.UIContext;

/**
 * Show total basket amount action
 *
 */
public class DisplayBasketTotalAction implements Action {

	@Override
	public void performAction(UIContext c) {
		
		showPrice(c.getVmMachine().showBasketTotal());
		print("");
	}

}
