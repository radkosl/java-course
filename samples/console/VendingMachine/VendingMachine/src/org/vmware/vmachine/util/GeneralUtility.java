package org.vmware.vmachine.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.vmware.vmachine.service.ConfigService;

/**
 * Helper class with static methods.
 * Provides support for reading config data.
 * @author radko
 */
public class GeneralUtility {

	public static final String APP_CONFIG_FILE = "vmachine.properties";
	public static final String APP_CONFIG_FOLDER = "config";
	public static final String IMPORT_FOLDER = "import";

	public static Collection<String> getPaymentTypes() {
		String value = ConfigService.getInstance().getProperty("payment_types");
		List<String> result = new ArrayList<String>();
		for (String type : value.split("[;]")) {
			result.add(type);
		}
		return result;
	}
	
	public static String getDefaultPaymentType() {
		return ConfigService.getInstance().getProperty("default_payment_type");
		
	}
	
	public static String getPaymentProviderSuffix() {
		return ConfigService.getInstance().getProperty("payment_strategy_suffix");
		
	}
	
	public static String getPaymentStrategyClasspath() {
		return ConfigService.getInstance().getProperty("payment_strategy_classpath");
	}
	
	public static String getDataImportPath(String fileToImport) {
		return GeneralUtility.class.getClassLoader().
				getResource(".").getPath() + APP_CONFIG_FOLDER + File.separator + IMPORT_FOLDER + File.separator + fileToImport;
	}
	
	public static String getAppConfigFilePath() {
		return GeneralUtility.class.getClassLoader().
				getResource(".").getPath() + APP_CONFIG_FOLDER + "/" + APP_CONFIG_FILE;
	}

}
