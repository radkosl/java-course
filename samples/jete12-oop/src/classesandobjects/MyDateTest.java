package classesandobjects;

public class MyDateTest {
	public static void main(String[] args) {
		MyDate myBirth;
		myBirth = new MyDate();
		myBirth.day= 14;
		myBirth.month = 6;
		myBirth.year = 1980;
	
		System.out.printf("%d.%02d.%d\n", 
			myBirth.day, myBirth.month, myBirth.year);

		MyDate yourBirth = new MyDate();
		yourBirth.day = 11;
		yourBirth.month = 3;
		yourBirth.year = 1975;
		System.out.printf("%d.%02d.%d\n", 
			yourBirth.day, yourBirth.month, yourBirth.year);
	}
}
