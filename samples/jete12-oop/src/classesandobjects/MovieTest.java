package classesandobjects;

public class MovieTest {
	
	public static void main(String[] args) {
		Movie matrix = new Movie("The Matrix", "8/10");
		matrix.displayDetails();

		Movie southPark = new Movie("South Park");
		southPark.displayDetails();
		southPark.setRating("7/10");
		southPark.displayDetails();
	}
}
