package inheritanceandpolimorphism;

public class Movie extends InventoryItem implements Sortable {

	private String title;
	
	public Movie(String title, float price) {
		this.title = title;
		this.price = price;
	}
	
	public void displayInfo() {
		System.out.printf("Title: %s, Price: %f\n", 
			this.title, this.price);
	}
	
	public int compare(Object movie2) {
		String title = this.title;
		String title1= ((Movie)movie2).title;
		
		if(title.compareTo(title1) > 0){
			return 1;
		}
		else if(title.compareTo(title1) < 0){
			return -1;
		}
		else{
			return 0 ;
		}
	}
	
}
