package inheritanceandpolimorphism;

public interface Sortable {
	/**
	 * Compare 'this' object to another object
	 * @return
	 *   0 if this object is equal to obj2
	 *   a value < 0 if this object < obj2
	 *   a value > 0 if this object > obj2
	 */
	int compare(Object obj2);
} 
