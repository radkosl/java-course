package inheritanceandpolimorphism;

public class Sort {  

	public static void sortObjects(Sortable[] items) {
		// Perform "Bubble sort" algorithm
		for (int i = 1; i < items.length; i++) {
			for (int j = 0; j < items.length-1; j++) {
				if (items[j].compare(items[j+1]) > 0) {
					Sortable tempItem = items[j+1];
					items[j+1] = items[j];
					items[j] = tempItem;
				}
			}
		}
	}
	
}
