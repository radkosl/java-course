public class SplittingStrings {

	public static void main(String[] args) {
		String listOfBeers = "Amstel, Zagorka, Tuborg, Becks.";
		String[] beers = listOfBeers.split("[ ,.]");
		System.out.println("Available beers are:");
		for (String beer : beers) {
			// Two sequential separators in the input cause
			// presence of empty element in the result
			if (!"".equalsIgnoreCase(beer)) {
				System.out.println(beer);
			}
		}
	}

}
