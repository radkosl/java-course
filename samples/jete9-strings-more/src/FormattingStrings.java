import java.util.Date;
import java.util.GregorianCalendar;

public class FormattingStrings {
	
	public static void main(String[] args) {		
		String template = "If I were %s, I would %s.";
		String sentence1 = String.format(
		    template, "developer", "know Java");
		System.out.println(sentence1);
		// If I were developer, I would know Java.

		String sentence2 = String.format(
		    template, "elephant", "weigh 4500 kg");
		System.out.println(sentence2);
		// If I were elephant, I would weigh 4500 kg.

	
		Date d = (new GregorianCalendar()).getTime();
		System.out.printf("Now is %1$td.%1$tm.%1$tY %1$tH:%1$tM:%1$tS", d);
		// Now is 23.05.2006 21:09:32
	}

}
