public class StringConcatenation {

	public static void main(String[] args) {
		String firstName = "Ivan";
		String lastName = "Dimitrov";

		String fullName = firstName + " " + lastName;
		System.out.println(fullName);

		int age = 25;

		String nameAndAge = 
			"Name: " + fullName + 
			"\nAge: " + age;
		System.out.println(nameAndAge);
	}

}
