package jete.solution.derived.cats;

import jete.solution.GenderEnumeration;
import jete.solution.derived.Cat;

/**
 * This can be only male right ???
 * hence override get gender method and always get GenderEnumeration.FEMALE
 * 
 * @author radkol
 *
 */
public class Kitten extends Cat{

	
	@Override
	public GenderEnumeration getGender() {
		return GenderEnumeration.FEMALE;
	}
}
