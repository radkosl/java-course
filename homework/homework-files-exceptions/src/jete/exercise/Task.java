package jete.exercise;


public class Task {
	
	/**
	 * Task1
	 * Write a method 'readNumbers' which has no arguments and which
	 * reads the entire content of the file "numbers.txt", line by line and try to sum
	 * all numbers in every line. Note that numbers are separated by empty space and not all of them are valid numbers!.
	 * The method should return the total sum of all valid numbers in all lines.
	 * (The method should return int value which is the calculated sum.)
	 * Handle the exceptions by printing error to the console.
	 * Hint: You can use the Utility.java in helpers package to check if string can be converted to int
	 * if it is not valid int, you should not add it to the final sum.
	 */
	
	/**
	 * Task 2
	 * Write a method 'createStudents' which has no arguments.
	 * The method should return Student[] -> array of Students objects. Student class is in helpers.Student package.
	 * The method should read the file "students.txt" line by line and:
	 * First line of the file will contain the number of lines(Student records) that exists.
	 * It has to ignore the second line of the file which has no student content and it is just a header of the file(like a table header).
	 * Then for every other line, it should process the line and build Student object with the information in the line.
	 * Make sure you trim the content properly!!! -> use trim() function
	 * What will be the character(s) that you have to split the line on?
	 * Create an array of students and add all Student objects that are created in that array.
	 * Return the array as a result from that method.
	 * 
	 */
	
}
