package jete.exercise;


public class Task {
	
	/**
	 * 13. Write a method 'reverseSentence' with one parameter(String sourceSentence) which reverses the words in given sentence (source parameter). Example: 
	 * "C# is not C++ and PHP is not Delphi" BECOMES "Delphi not is PHP and C++ not is C#"
	 * Return the reversed string sentence from the method
	 */
	
	/**
	 *  14. Write a method 'extractDates' with one parameter(String sourceText) which check for dd.mm.yyyy date pattern for valid date text and return the number of occurrences of these valid dates. Example: 
	 *  "Radko is born in 19.08.1988. He graduated from King's College in 10.09.2012. He started Java Course in 22.03.2016. Invalid Dates - 50.03.20111 2011.50.33" returns '3'
	 *  Return the number of occurrences of all valid dates
	 *  Conditions:
	 *  Valid year part is considered between '1900' to '2199';
	 *  Valid day part is considered between '01' to '31';
	 *  Valid month part is considered between '01' to '12';
	 * 
	 *  USE CharactersHelper.java helper class. write additional helper methods if needed :))
	 */
	
}
