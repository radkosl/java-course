package jete.solution;

import jete.helpers.CharactersHelper;

public class TaskSolution {
	
	/**
	 * 13. Write a method 'reverseSentence' with one parameter(String sourceSentence) which reverses the words in given sentence (source parameter). Example: 
	 * "C# is not C++ and PHP is not Delphi" BECOMES "Delphi not is PHP and C++ not is C#"
	 * Return the reversed string sentence from the method
	 */
	public String reverseSentence(String source) {
		
		if(source == null) {
			throw new NullPointerException("Cannot reverse null string. Program stops here...");
		}
		
		// empty string don't do anything
		if(source.length() == 0) {
			return source;
		}
		
		// easy, just split the string with empty and iterate over it from end to beginning.
		String[] parts = source.split("[\\s]"); // or you can do that "[ ]" but we can use regex expression also, \s mean whitespace!
		
		// we know exactly how long will be so we can initialise it with correct size right ? If not sure about the site, we can use the empty constructor StringBuilder()!
		StringBuilder result = new StringBuilder(source.length());
		for(int i = parts.length- 1; i >=0 ; i--) {
			result.append(parts[i]).append(" ");
		}
		
		// what is missing is that we need to get rid of the last empty space that we put at the end while looping. You can do that with check in the loop :).
		return result.toString().trim();
		
	}
	
	/**
	 *  14. Write a method 'extractDates' with one parameter(String sourceText) which check for dd.mm.yyyy date pattern for valid date text and return the number of occurrences of these valid dates. Example: 
	 *  "Radko is born in 19.08.1988. He graduated from King's College in 10.09.2012. He started Java Course in 22.03.2016. Invalid Dates - 50.03.20111 2011.50.33" returns '3'
	 *  Return the number of occurrences of all valid dates
	 *  Conditions:
	 *  Valid year part is considered between '1900' to '2199';
	 *  Valid day part is considered between '01' to '31';
	 *  Valid month part is considered between '01' to '12';
	 * 
	 *  USE CharactersHelper.java helper class.
	 */
	public int extractDates(String sourceText) {
		
		if(sourceText == null) {
			throw new NullPointerException("Cannot extract from null string. Program stops here...");
		}
		
		// empty string don't do anything
		if(sourceText.length() == 0) {
			return 0;
		}
		
		String[] parts = sourceText.split("[ ]");
		
		int dateOccurancesCount = 0;
		
		// we can use foreach loop right ?
		for(String part : parts) {
			// for every part we found, check if it is valid date dd.mm.yyyy
			if(validDate(part)) {
				dateOccurancesCount++;
			}
		}
		
		return dateOccurancesCount;
		
	}

	/**
	 * Check if string is valid dd.mm.yyyy
	 */
	private boolean validDate(String part) {
		
		if(part == null || part.isEmpty()) {
			return false;
		}
		
		// split by '.'
		String[] dateParts = part.split("[.]");
		
		// we need to have 3 parts - dd , mm and yyyy
		if(dateParts == null || dateParts.length != 3) {
			return false;
		}
		
		/**
		 * We can easily try to convert the string parts into integers. and check if they are between the range.
		 * But we need to catch Exceptions in order to do that. we don't know how to do that just yet, so we 
		 * will do it old school. Use the CharactersHelper !!!
		 */
		
		if(!validDay(dateParts[0])) {
			return false;
		}
		
		if(!validMonth(dateParts[1])) {
			return false;
		}
		
		if(!validYear(dateParts[2])) {
			return false;
		}
		
		
		return true;
	}
	
	/**
	 * Examine if particular string meets the condition for valid YEAR !
	 */
	private boolean validYear(String year) {
		
		// year is exactly 4 digits. return if not valid.
		if(!validSizeAndDigits(year, 4)) {
			return false;
		}
		
		char firstChar = year.charAt(0);
		char secondChar = year.charAt(1);
		
		// valid years can be from '1900' to '2199';
		if(CharactersHelper.isCharDigitEqualTo(firstChar, 1)) {
			
			// first digit is 1, so now second should be 9.  -> 19XX
			// we don't care for the last two digits right ?
			if(CharactersHelper.isCharDigitEqualTo(secondChar, 9)) {
				return true;
			}
			
			return false;
			
		} else if (CharactersHelper.isCharDigitEqualTo(firstChar, 2)) {
		
			// first digit is 2, so now second should be 1. -> 21XX
			// we don't care for the last two digits right ?
			if(CharactersHelper.isCharDigitLessThan(secondChar, 2)) {
				return true;
			}
			
			return false;
		}
		
		return false;
	}
	
	/**
	 * Examine if particular string meets the condition for valid MONTH !
	 */
	private boolean validMonth(String month) {
		// month is exactly 2 digits. return if not valid.
		if(!validSizeAndDigits(month, 2)) {
			return false;
		}
		
		char firstChar = month.charAt(0);
		char secondChar = month.charAt(1);
		
		// we know that first char is 0, we should check if second is not zero!
		if(CharactersHelper.isZero(firstChar)) {
			if(!CharactersHelper.isZero(secondChar)) {
				return true;
			} else {
				return false;
			}
		}
		
		// first char is 1, so second should be 1 or 2 -> 11 and 12 is max valid month
		if(CharactersHelper.isOne(firstChar)) {
			
			if(CharactersHelper.isCharDigitLessThan(secondChar, 3)) {
				return true;
			}
		}
		
		return false;

	}
	
	/**
	 * Examine if particular string meets the condition for valid DAY !
	 */
	private boolean validDay(String day) {
		
		// month is exactly 2 digits. return if not valid.
		if(!validSizeAndDigits(day, 2)) {
			return false;
		}
		
		char firstChar = day.charAt(0);
		char secondChar = day.charAt(1);
		
		// we know that first char is 0, we dont care for the following digit '0X' is valid!
		if(CharactersHelper.isZero(firstChar)) {
			if(!CharactersHelper.isZero(secondChar)) {
				return true;
			} else {
				return false;
			}
		}
		
		// first char has to be less than 4 -> 1, 2, 3
		if(CharactersHelper.isCharDigitLessThan(firstChar, 4)) {
			
			
			// if first is 3 only option for second is to be 0 or 1 -> 30 or 31 is valid
			if(CharactersHelper.isCharDigitEqualTo(firstChar, 3)) {
				if(CharactersHelper.isCharDigitLessThan(secondChar, 2)) {
					return true;
				} else {
					return false;
				}
			}
			
			// for chars 1 and 2 we don't care for the second digit 1X and 2X are valid for days...
			return true;
		}
		
		return false;
		
	}
	
	/**
	 * Examine if given string is with specific lenght,
	 * and all characters inside that string are actually digits !.
	 */
	private boolean validSizeAndDigits(String source, int charsCount) {
		
		if(source == null || source.length() != charsCount) {
			return false;
		}
		
		// check if all chars are actually digits
		// return if not true
		for(char currentChar : source.toCharArray()) {
			if(!CharactersHelper.validDigit(currentChar)) {
				return false;
			}
		}
		
		return true;
	}
	
}
