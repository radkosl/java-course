package jete.solution;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import jete.AbstractTest;

public class TaskSolutionTest extends AbstractTest {
	
	private String packageName;
	private String examineClass;
	
	@Before
	public void setup() {
		packageName = "jete.solution";
		examineClass = "TaskSolution";
	}
	
	@Test
	public void reverseSentence() {
		
		TaskSolution examineObject = (TaskSolution)createInstance(getClass(this.packageName, this.examineClass));
		
		String sourceMessage = "C# is not C++ and PHP is not Delphi";
		String expectedMessage = "Delphi not is PHP and C++ not is C#";
		executeTest(examineObject, "reverseSentence", expectedMessage, new String[]{sourceMessage}, new Class[]{String.class});
		
		expectedMessage = "C# is not C++ and PHP is not Delphi";
		sourceMessage = "Delphi not is PHP and C++ not is C#";
		executeTest(examineObject, "reverseSentence", expectedMessage, new String[]{sourceMessage}, new Class[]{String.class});
		
		expectedMessage = "27 is Radko";
		sourceMessage = "Radko is 27";
		executeTest(examineObject, "reverseSentence", expectedMessage, new String[]{sourceMessage}, new Class[]{String.class});
		
		expectedMessage = "Plovdiv Academy";
		sourceMessage = "Academy Plovdiv";
		executeTest(examineObject, "reverseSentence", expectedMessage, new String[]{sourceMessage}, new Class[]{String.class});
		
		expectedMessage = "";
		sourceMessage = "";
		executeTest(examineObject, "reverseSentence", expectedMessage, new String[]{sourceMessage}, new Class[]{String.class});
		
		expectedMessage = "x + y = 10";
		sourceMessage = "10 = y + x";
		executeTest(examineObject, "reverseSentence", expectedMessage, new String[]{sourceMessage}, new Class[]{String.class});
	}
	
	@Test
	public void extractDates() {
		
		TaskSolution examineObject = (TaskSolution)createInstance(getClass(this.packageName, this.examineClass));
		
		List<String> valid = new ArrayList<String>(){
			{
				add("20.09.2010");
				add("31.12.2199");
				add("01.01.1900");
				add("31.01.1901");
				add("31.04.2016");
				add("19.08.1988");
				add("12.05.2007");
				add("12.05.2198");
				add("12.08.2017");
			}
		};
		
		List<String> invalid = new ArrayList<String>(){
			{
				add("00.00.0000");
				add("010.00.0000");
				add("00.00.00100");
				add("31.00.2016");
				add("31.011.2016");
				add("31.110.2016");
				add("00.11.2016");
				add("01.01.2200");
				add("32.01.2199");
				add("31.13.2100");
				add("32.101.2007");
				add("12.05.1899");
			}
		};
		
		for(String source : valid) {
			executeTest(examineObject, "extractDates", 1, new String[]{source}, new Class[]{String.class});
		}
		
		for(String source : invalid) {
			executeTest(examineObject, "extractDates", 0, new String[]{source}, new Class[]{String.class});
		}
		
		String sentence1 = String.format("Radko is born in %s. He graduated in %s. He finish high school in %s. He turn 20 in %s", valid.get(0), valid.get(1), valid.get(2), invalid.get(0) );
		executeTest(examineObject, "extractDates", 3, new String[]{sentence1}, new Class[]{String.class});
		
		sentence1 = String.format("Radko is born in %s. He graduated in %s. He finish high school in %s. He turn 20 in %s", valid.get(5), valid.get(6), valid.get(4), invalid.get(3) );
		executeTest(examineObject, "extractDates", 3, new String[]{sentence1}, new Class[]{String.class});
		
		sentence1 = String.format("Radko is born in %s. He graduated in %s. He finish high school in %s. He turn 20 in %s", invalid.get(0), invalid.get(1), invalid.get(2), invalid.get(0) );
		executeTest(examineObject, "extractDates", 0, new String[]{sentence1}, new Class[]{String.class});
		
		sentence1 = String.format("Radko is born in no date %s", invalid.get(7));
		executeTest(examineObject, "extractDates", 0, new String[]{sentence1}, new Class[]{String.class});
		
		sentence1 = String.format("Radko is born in no date %s %s %s %s", invalid.get(7), invalid.get(8), invalid.get(8),  valid.get(8));
		executeTest(examineObject, "extractDates", 1, new String[]{sentence1}, new Class[]{String.class});
		
		
	}

}
