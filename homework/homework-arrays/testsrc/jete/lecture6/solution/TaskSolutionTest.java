package jete.lecture6.solution;

import org.junit.Before;
import org.junit.Test;

import jete.lecture6.AbstractTest;
import jete.lecture6.exercise.Task;

public class TaskSolutionTest extends AbstractTest {
	
	TaskSolution task;
	
	@Before
	public void setup() {
		task = new TaskSolution();
	}
	
	@Test
	public void task1() {
		
		String methodName = "task1";
		Object[] args = new Object[1];
		args[0] = 5;
		
		Class[] paramTypes = new Class[1];
		paramTypes[0] = Integer.TYPE;
		
		executeTest(task, methodName, 15, args, paramTypes);
		
		args[0] = 6;
		executeTest(task, methodName, 21, args, paramTypes);
		
		args[0] = 7;
		executeTest(task, methodName, 28, args, paramTypes);
	}
	
	@Test
	public void task2() {
		
		String methodName = "task2";
		Object[] args = new Object[1];
		args[0] = 2;
		
		Class[] paramTypes = new Class[1];
		paramTypes[0] = Integer.TYPE;
		
		executeTest(task, methodName, 4, args, paramTypes);
		
		args[0] = 3;
		executeTest(task, methodName, 9, args, paramTypes);
		
		args[0] = 4;
		executeTest(task, methodName, 16, args, paramTypes);
		
		args[0] = 5;
		executeTest(task, methodName, 25, args, paramTypes);
	}
	
	@Test
	public void task3() {
		
		String methodName = "task3";
		Object[] args = new Object[2];
		Class[] paramTypes = new Class[2];
		paramTypes[0] = Integer.TYPE;
		paramTypes[1] = Integer.TYPE;
		
		args[0] = 2;
		args[1] = 1;
		executeTest(task, methodName, 6, args, paramTypes);
		
		args[0] = 2;
		args[1] = 2;
		executeTest(task, methodName, 9, args, paramTypes);
		
		args[0] = 2;
		args[1] = 3;
		executeTest(task, methodName, 12, args, paramTypes);
		
		args[0] = 3;
		args[1] = 2;
		executeTest(task, methodName, 18, args, paramTypes);
		
		args[0] = 4;
		args[1] = 4;
		executeTest(task, methodName, 50, args, paramTypes);
		
		args[0] = 5;
		args[1] = 2;
		executeTest(task, methodName, 45, args, paramTypes);
	}
	
	@Test
	public void task4() {
		
		String methodName = "task4";
		Object[] args = new Object[2];
		Class[] paramTypes = new Class[2];
		paramTypes[0] = Integer.TYPE;
		paramTypes[1] = Integer.TYPE;
		
		args[0] = 2;
		args[1] = 1;
		executeTest(task, methodName, 4, args, paramTypes);
		
		args[0] = 2;
		args[1] = 2;
		executeTest(task, methodName, 6, args, paramTypes);
		
		args[0] = 4;
		args[1] = 3;
		executeTest(task, methodName, 24, args, paramTypes);
		
	}
		
}
