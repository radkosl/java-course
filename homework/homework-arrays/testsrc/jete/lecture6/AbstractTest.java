package jete.lecture6;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class AbstractTest {
	
	protected static final String messageFormat = "%s:Expected result from method '%s' with parameters '%s' is '%s', actual result is '%s' ...";
	protected static final String messageException = ">> There was an error while running the test for %s method. Please skip this one for now. Exception message: %s <<";
	protected static final String messageNoMethod = "There is no public static method with name '%s'. Please create it!";
	
	@SuppressWarnings("unchecked")
	protected Method getMethod(@SuppressWarnings("rawtypes") Class cls, String methodName, Class[] paramTypes) {
		
		try {
			Method method = cls.getMethod(methodName, paramTypes);
			return method;
		} catch (NoSuchMethodException | SecurityException e) {
			return null;
		}
		
	}
	
	public void executeTest(Object source, String methodName, Object expectedResult, Object[] args, Class[] paramTypes) {
		
		Method m = getMethod(source.getClass(), methodName, paramTypes);
		
		if(m == null) {
			assertTrue(String.format(messageNoMethod, methodName), false);
		} else {
			Object result = invokeMethod(m, source, args);
			
			String msg = "|";
			for(Object o : args) {
				msg += o.toString() + "|";
			}
			
			assertTrue(String.format(messageFormat, methodName, methodName, msg, expectedResult, result), result.equals(expectedResult));
		}
	}
	
	public Object invokeMethod(Method m, Object source, Object[] args) {
		try {
			Object result = m.invoke(source, args);
			return result;
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			assertTrue(String.format(messageException, m.getName(), e.getMessage()), false);
		}
		
		return null;
	}
	
}
