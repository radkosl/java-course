package jete.lecture6.solution;

import java.util.Arrays;

public class TaskSolution {
	
	/**
	 * Task 1
	 * Implement a method called 'task1' that takes int parameter 'n' as argument.
	 * create an array with size 'n' and fill the array
	 * with values from 1 to n, starting from the beginning
	 * return the sum of all elements in the array as a result
	 */
	
	public static int task1(int n) {
		
		int[] result = new int[n];
		
		for(int i = 0 ; i < n ; i++ ) {
			result[i] = i + 1;
		}
		
		int sum = 0;
		
		for(int i : result) {
			sum += i;
		}
		
		return sum;
	}
	
	
	/**
	 * Task 2
	 * Implement a method called 'task2' that takes int parameter 'n' as argument.
	 * create an array with size 'n' and fill the array with the first 'n' odd values, 
	 * with values from 1 to n, starting from the beginning
	 * e.g. 
	 * arr[0] = 1, 
	 * arr[1] = 3, 
	 * arr[2] = 5 
	 * etc.
	 * return the sum of all elements in the array as a result
	 */
	public static int task2(int n) {
		
		int[] result = new int[n];
		
		int incrementStep = 0;
		for(int i = 0 ; i < n ; i++ ) {
			result[i] = 1 + incrementStep;
			incrementStep = incrementStep + 2;
		}
		
		int sum = 0;
		
		for(int i : result) {
			sum += i;
		}
		
		
		return sum;
	}
	
	/**
	 * Task 3
	 * Implement a method 'task3' that takes 2 int parameters called 'arrayLength' and 'addition'.
	 * create an array with size 'arrayLength' and fill the array as follows: 
	 * arr[0] = 1 + addition * 1, 
	 * arr[1] = 2 + addition * 2, 
	 * arr[2] = 3 + addition * 3 
	 * ... etc.
	 * return the sum of all elements in the array as a result
	 */
	public static int task3(int arrayLength, int addition) {
		
		int[] result = new int[arrayLength];
		
		for(int i = 1 ; i <= arrayLength ; i++ ) {
			result[i-1] = i + addition * i;
		}
		
		int sum = 0;
		
		for(int i : result) {
			sum += i;
		}
		
		
		return sum;
	}
	
	/**
	 * Task 4
	 * Implement a method 'task4' that takes 2 int parameters called 'arrayLength' and 'addition'.
	 * create an array with size 'arrayLength' and fill the array as follows: 
	 * arr[0] = 1 + addition * 1, 
	 * arr[1] = 2 + addition * 2 - arr[0], 
	 * arr[2] = 3 + addition * 3 - arr[1] 
	 * ... etc.
	 * return the sum of all elements in the array as a result
	 */
	public static int task4(int arrayLength, int addition) {
		
		int[] result = new int[arrayLength];
		
		for(int i = 1 ; i <= arrayLength ; i++ ) {
			
			// first element won't be subtracted
			int substraction = 0;
			if(i > 1) {
				substraction = result[i-2];
			}
			result[i-1] = i + addition * i - substraction;
		}
		
		int sum = 0;
		
		for(int i : result) {
			sum += i;
		}
		
		
		return sum;
	}
}
