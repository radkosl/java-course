package jete.lecture3.solution;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TaskSolutionTest {
	TaskSolution task;
	
	@Before
	public void setup() {
		task = new TaskSolution();
	}
	
	@Test
	public void task1() {
		String res = task.task1();
		assertTrue("Result should be '23'", res.equals("23"));
	}
	
	@Test
	public void task2() {
		String res = task.task2();
		assertTrue("Result should be 'HelloWorld'", res.equals("HelloWorld"));
		
	}
	
	@Test
	public void task3() {
		boolean res = task.task3();
		assertTrue("Result should be 'true'", res);
	}
	
	@Test
	public void task4() {
		int res = task.task4();
		assertTrue("Result should be '9', " + res + " found.", res == 9);
	}
	
	@Test
	public void task5() {
		String res = task.task5();
		assertTrue("Result should be 'string', '" + res + "' found.", res.equals("string"));
	}
	
	@Test
	public void task6() {
		String res = task.task6();
		assertTrue("Result should be 'Thisisstring', '" + res + "' found.", res.equals("Thisisstring"));
	}
	
	@Test
	public void task7() {
		String res = task.task7();
		assertTrue("Result should be 'Tsveta', '" + res + "' found.", res.equals("Tsveta"));
	}
	
	@Test
	public void task8() {
		String res = task.task8();
		assertTrue("Result should be 'The price of this thing is 24,00', '" + res + "' found.", res.equals("The price of this thing is 24,00"));
	}
	
}
