package jete.lecture3.solution;

import java.util.Locale;
import java.util.regex.Pattern;

public class TaskSolution {

	// Assign values to these three string variables with values "1" ,"2" and
	// "3".
	// change value of v1 so it will combine the string defined in v2 and v3.
	public String task1() {

		String v1 = "1";
		String v2 = "2";
		String v3 = "3";

		v1 = v2 + v3;

		return v1;

	}

	// Define string with value "Hello World"
	// Remove the whitespacing of that string (The 'space' character) and assign
	// the result to 'result' String variable
	public String task2() {

		String source = "Hello World";
		String result = source.replaceAll("\\s+", "");

		return result;

	}

	// Define pattern that will match the following: "111-222-333-"
	public boolean task3() {

		String pattern = "(\\d+-)+";

		return Pattern.matches(pattern, "111-222-333-");

	}

	// Set 'source' string var with value "This is my string".
	// Find the index of letter "y"
	public int task4() {
		String source = "This is my string";
		int index = source.indexOf('y');

		return index;
	}

	// Set 'source' string var with value "This is my string".
	// Create another variable and retrieve part of 'source' that start from
	// index 11 until the end.
	public String task5() {
		String source = "This is my string";
		String result = source.substring(11);

		return result;
	}

	// Set 'source' string var with value "This is my string".
	// Remove 'my' part and any whitespace in that string and assign it to
	// another variable
	public String task6() {

		String source = "This is my string";
		
		// solution 1
		String result = source.replace("my", "").replaceAll("\\s", "");
		
		// solution 2
		// String result = source.replaceAll("\\s|my", "");
		
		return result;
	}

	// Use the following 3 String variables. Find which one is 'last'. By last it means alphabetically
	public String task7() {
		
		String var1 = "Todor";
		String var2 = "Teodora";
		String var3 = "Tsveta";
		String result = ""; // assign the finding to that variable
		
		if(var1.compareTo(var2) > 0) {
			if(var1.compareTo(var3) > 0) {
				result = var1;
			} else {
				result = var3;
			}
		} else if (var2.compareTo(var3) > 0) {
			result = var2;
		} else {
			result = var3;
		}
		
		return result;
	}
	
	// Set System Locale to French.
	// Define double variable val with value 24.005
	// Define string variable formattedInput with value 'The price of this thing is #double_value#' , where
	// #double_value# will be the double value defined above. Make sure you are displaying only one digit after
	// the floating point
	// 
	public String task8() {
		
		// set locale here
		Locale.setDefault(Locale.FRENCH);
		
		double val = 24.004d;
		
		String formattedInput = "The price of this thing is %.2f";
		
		String result = String.format(formattedInput, val);
		
		return result;
	}
	
}
