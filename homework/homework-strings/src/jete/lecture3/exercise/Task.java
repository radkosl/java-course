package jete.lecture3.exercise;

import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

public class Task {
	

	// Assign values to these three string variables with values "1" ,"2" and "3".
	// change value of v1 so it will combine the string defined in v2 and v3.
	public String task1() {
		
		String v1=""; // TODO
		String v2=""; // TODO
		String v3=""; // TODO
		
		return v1;
		
	}
	
	// Set 'source' string variable with value "Hello World" 
	// Remove the whitespacing of that string (The 'space' character) and assign the result to 'result' String variable
	public String task2() {
		
		String source = ""; // TODO
		String result = ""; // TODO
		
		return result;
	}
	
	// Define pattern that will match the following: "111-222-333-"
	public boolean task3() {
		String pattern = ""; // TODO
		return Pattern.matches(pattern, "111-222-333-");
	}
	
	// Set 'source' string var with value "This is my string".
	// Find the index of letter "y"
	public int task4() {
		String source = ""; // TODO
		int index = -1; // TODO
		
		return index;
	}
	
	// Set 'source' string var with value "This is my string".
	// Create another variable and retrieve part of 'source' that start from index 11 until the end.
	public String task5() {
		String source = ""; // TODO
		String result = ""; // TODO
		
		return result;
	}
	
	// Set 'source' string var with value "This is my string".
	// Remove 'my' part and any whitespace in that string and assign it to another variable
	public String task6() {
		String source = ""; // TODO
		String result = ""; // TODO
		
		return result;
	}
	
	// Use the following 3 String variables. Find which one is 'first'. By first it means alphabetically.
	// Try to do it with if - else conditional structure (or any other approach you might think is good here)
	public String task7() {
		
		String var1 = "Radko";
		String var2 = "Teodora";
		String var3 = "Reneta";
		
		String result = ""; // assign the finding to that variable
		
		return result;
	}
	
	// Set System Locale to French.
	// Define double variable val with value 24.00
	// Define string variable formattedInput with value 'The price of this thing is #double_value#' , where
	// #double_value# will be the double value defined above.
	// 
	public String task8() {
		
		// set locale here
		// TODO
		
		double val = -100d; // TODO
		String formattedInput = ""; // TODO
		
		
		String result = String.format(formattedInput, val);
		
		return result;
	}
	
	
}
