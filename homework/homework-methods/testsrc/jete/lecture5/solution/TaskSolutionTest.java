package jete.lecture5.solution;

import org.junit.Before;
import org.junit.Test;

import jete.lecture5.AbstractTest;
import jete.lecture5.solution.TaskSolution;

public class TaskSolutionTest extends AbstractTest {
	
	TaskSolution task;
	
	@Before
	public void setup() {
		task = new TaskSolution();
	}
	
	@Test
	public void task1() {
		executeTest(task, "calculateTwentyOdds", 100);
	}
	
	@Test
	public void task2() {
		executeTest(task, "sumEvenAndMultiplyOdds", 4325);
	}
	
	@Test
	public void task3() {
		executeTest(task, "findRCharacterInString", 15);
	}
		
}
