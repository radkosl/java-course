package jete.lecture5.solution;

public class TaskSolution {

	/**
	 * All method signatures should look like this 
	 * public static {return} | {void} {methodname} ({arguments}) {}
	 */
	
	/**
	 * Task 1
	 * write a method called "calculateTwentyOdds" that has no arguments and will return
	 * the sum of all odd numbers between 1 and 20.
	 * e.g. 1,3,5,7,9 ....
	 */
	 public static int calculateTwentyOdds() {
		 
		 int sum = 0;
		 for(int index = 1 ; index < 20 ; index = index + 2) {
			 sum = sum + index;
		 }
		 
		 return sum;
		 
	 }
	 
	/**
	 * Task 2
	 * Write a method called "sumEvenAndMultiplyOdds" that has no arguments and will do the following
	 * Define int variable 'n' with value 100;
	 * Sum all even numbers between 1 and 'n' and store the result in int variable 'evenSum'
	 * Find the product(*) of all numbers between 1 and 'n' (which can be divided by 25 with no reminder and are also not even) and store the result in int variable 'nineProduct'
	 * Then sum both variables 'evenSum' and 'nineProduct' and that will be returned from your method.
	 */
	 public static int sumEvenAndMultiplyOdds() {
		 
		 int n = 100;
		 int evenSum = 0;
		 int nineProduct = 1;
		 
		 for(int number = 1; number < n; number ++) {
			 
			 // even number
			 if(number % 2 == 0) {
				 evenSum  = evenSum + number;
			 } else {
				 // number is not even, check if it can be divided by 25
				 if(number % 25 == 0) {
					 nineProduct = nineProduct * number;
				 }
			 }
		 }
		 
		 int finalSum = evenSum + nineProduct;
		 
		 return finalSum;
		 
	 }
	 
	 /**
	  * Task 3
	  * 
	  * Write a method 'findRCharacterInString' with no arguments that returns how many times the characters 'r','R' appears in the following text.
	  * "Radko is croSsing the Red River at random times. He tries to step slowLy so he can reach the other Shore without huRting.[From RRR book]"
	  * 
	  */
	 public static int findRCharacterInString() {
		 String source = "Radko is croSsing the Red River at random times. He tries to step slowLy so he can reach the other Shore without huRting.[From RRR book]";
		 
		 int index = 0;
		 int sourceLength = source.length();
		 int rCount = 0;
		 
		 while(index < sourceLength) {
			 
			 char currentLetter = source.charAt(index);
			 if(currentLetter == 'r' || currentLetter == 'R') {
				 rCount++;
			 }
			 
			 index++;
		 }
		 
		 return rCount;
	 }
}
