package jete.lecture5.exercise;

public class Task {

	/**
	 * All method signatures should look like this 
	 * public static {return} {methodname} ({arguments}) {}
	 */
	
	/**
	 * Task 1
	 * write a method called "calculateTwentyOdds" that has no arguments and will return
	 * the sum of all odd numbers between 1 and 20.
	 * e.g. 1,3,5,7,9 ....
	 */
	 // TODO
	
	/**
	 * Task 2
	 * Write a method called "sumEvenAndMultiplyOdds" that has no arguments and will do the following
	 * Define int variable 'n' with value 100;
	 * Sum all even numbers between 1 and 'n' and store the result in int variable 'evenSum'
	 * Find the product(*) of all numbers between 1 and 'n' (which can be divided by 25 with no reminder and are also not even) and store the result in int variable 'nineProduct'
	 * Then sum both variables 'evenSum' and 'nineProduct' and that will be returned from your method.
	 */
	 // TODO
	
	 /**
	  * Task 3
	  * 
	  * Write a method 'findRCharacterInString' with no arguments that returns how many times the characters 'r','R' appears in the following text.
	  * "Radko is croSsing the Red River at random times. He tries to step slowLy so he can reach the other Shore without huRting.[From RRR book]"
	  * 
	  */
	  // TODO
}
