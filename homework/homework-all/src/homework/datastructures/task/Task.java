package homework.datastructures.task;

public class Task {
	
	
	/**
	 * Write a method called "arrayListSum" which accepts 1 argument of type ArrayList<Integer>
	 * and returns as a result the sum of all elements in that list
	 */
	
	/**
	 * ---------------------------------------
	 */

	/**
	 * Write a method called "arrayListCompexSum" which accepts 1 argument of type ArrayList<Integer>
	 * and returns a map of type Map<Integer,Integer> with one entry inside. That entry's key will hold the sum of all elements in the odd indexes in the list, 
	 * and the value will hold the product of all even indexes.
	 * Example result:
	 * input array list => 1, 2, 3, 4
	 * result map => key => 6 (2 + 4), value => (1 + 3)
	 */
	
	/**
	 * ---------------------------------------
	 */
	
	/**
	 * Write a method called "transformList" which accepts 1 argument of type ArrayList<Integer>
	 * and return a map of type Map<Integer,Integer> where the keys in the map will be the indexes in the array list, and the respective values will be the values in that index
     * Example result:
	 * input array list => 100, 200 , 300
	 * result map => key#value  (0#100, 1#200, 2#300)
	 */

	/**
	 * ---------------------------------------
	 */
	
	/**
	 * Method called "assertListItemsLength" which accepts 1 argument of type ArrayList<String>
	 * and returns boolean if the total length of all strings in the odd index positions is equal to the total length of all strings in the even index positions.
     * Example result:
	 * input array list => "rad","tst","1","2"
	 * result true
	 * 
	 * input array list => "radko","test","111","22"
	 * result false
	 */
	
	/**
	 * ---------------------------------------
	 */
	
	/**
	 * Method called "sumGroupedKeys" which accepts 1 argument of type Map<String, Integer>
	 * where the keys of the map will be "strings" and the values will be "integers"
	 * The method should return another Map<Integer,Integer> which keys will be the distinct lengths of the string, and the value will be the total count of all strings with that length.
	 * Example
	 * 
	 * Input map:
	 * 
	 * "Radko" , 5
	 * "Todor" , 8
	 * "Mimi", 12
	 * "Radi", 3,
	 * "Awesome" , 22
	 * 
	 * Result map :
	 * 
	 * 5, 13    ( Radko and Todor has 5 letters (the key) so we can sum their values and that will be the result)
	 * 4, 15    ( Mimi and Radi has 4 letters (the key) so we can sum their values and that will be the result)
	 * 7, 22	( Awesome has 7 letters (the key) so we can get its value(only one) and that will be the result)
	 *  
	 */
}
