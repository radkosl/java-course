package homework.exceptions.task;

public class Task {
		
	
	/**
	 * All classes that you need to create should be placed in this package
	 * homework.exceptions.task
	 */
	
	/**
	 * Write a class called School.
	 * School will have the following fields:
	 * name - String type
	 * schoolType - Enum type - What was Enum ? (To do that field you need to create new Enum class
	 * with only couple of predefined values in it. PRIMARY and SECONDARY, HIGHSCHOOL)
	 * check that https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html if not sure how to do this.
	 * 
	 * Create another class called 'StudentClass' which represents a group (class) of students.
	 * it has the following fields
	 * name -> String  - e.g. 12b, 8d etc.
	 * speciality -> String - e.g Biology, Informatics etc.
	 * capacity -> int - total number of students in that class
	 * 
	 * Add a Collection of StudentClass objects as attribute to School class. 
	 * There should be link that school has many classes right ?
	 * Choose The Collection to be of type java.util.List ...
	 * 
	 * Create method in School class with declaration like this addClass(StudentClass newClass)
	 * and make sure u add this new student class to all classes in that School.
	 * 
	 * Write a main method class, create all types of objects required.
	 * Setup sample school and prints its data in that main method.
	 * If you have time make sure you validate all data in the classes
	 * for example 'capacity' can't be more than 35 right ?
	 * or the length of the school name can't be more than 100
	 * Also all fields should have value in them(they are not optional, they are required)
	 * Or speciality can't be null
	 * U can use Exceptions here ... 
	 * Make sure you have overrided toString method Everywhere so you can print the objects easily!
	 */
	
	
	/**
	 * Task 2.
	 * Create a Calculator.java class which will be working as the windows calculator you all have. Think about what do you need for the "state of the calculator".
	 * But you should be able to do all 4 operations on numbers, so you need four methods
	 * 
	 * add(int value) -> add the passed value to the existing value
	 * substract(int value) -> substract the passed value from the existing value
	 * divide(int value) -> same logic but devide them
	 * multiply(int value) -> same logic but multiply them
	 * 
	 * method calculate() which when called, return the result "the current state of the calculator" and then reset that state so we can start using the calculator from the beginning.
	 * so i should be able to do this.
	 * Calculator c = new Calculator();
	 * c.add(5);
	 * c.substract(10);
	 * c.multiply(2);
	 * System.out.print(c.calculate()); => displaying -10
	 * -------------------------------------------------
	 * If that is easy for you, try to do that like this
	 * 
	 * Calculator c = new Calculator();
	 * c.add(5).substract(10).divide(2).multiply(4).calculate()
	 * 
	 * Do you see the difference .. It is huge difference :)
	 * 
	 */
	
	
}
