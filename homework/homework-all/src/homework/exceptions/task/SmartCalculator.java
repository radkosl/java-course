package homework.exceptions.task;

public class SmartCalculator {

	private int currentValue;
	
	public SmartCalculator add(int newValue) {
		currentValue = currentValue + newValue;
		return this;
	}
	
	public SmartCalculator substract(int newValue) {
		currentValue = currentValue - newValue;
		return this;
	}
	
	public SmartCalculator multiply(int newValue) {
		currentValue = currentValue * newValue;
		return this;
	}
	
	public int calculate() {
		int curvalue = this.currentValue;
		this.currentValue = 0;
		return curvalue;
	}

}


