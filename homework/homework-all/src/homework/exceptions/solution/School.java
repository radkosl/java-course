package homework.exceptions.solution;

import java.util.ArrayList;
import java.util.List;

public class School {

	private String name;
	private SchoolType schoolType;
	private List<StudentClass> studentClasses;
	
	public School() {
		
	}
	
	public School(String name, SchoolType schoolType, List<StudentClass> studentClasses) {
		super();
		this.name = name;
		this.schoolType = schoolType;
		this.studentClasses = studentClasses;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public SchoolType getSchoolType() {
		return schoolType;
	}
	public void setSchoolType(SchoolType schoolType) {
		this.schoolType = schoolType;
	}
	public List<StudentClass> getStudentClasses() {
		return studentClasses;
	}
	public void setStudentClasses(List<StudentClass> studentClasses) {
		this.studentClasses = studentClasses;
	}
	
	public void addNewClass(StudentClass newClass) {
		if(getStudentClasses() == null) {
			setStudentClasses(new ArrayList<StudentClass>());
		}
		
		getStudentClasses().add(newClass);
	}
	
	@Override
	public String toString() {
		return "School [name=" + name + ", schoolType=" + schoolType + ", studentClasses=" + studentClasses + "]";
	}
	
}
