package homework.exceptions.solution;

import java.util.ArrayList;
import java.util.List;

public class RunSolution {

	public static void main(String[] args) {
		
		
		School school = new School();
		List<StudentClass> studentClasses = new ArrayList<>();
		
		school.setName("High Math School Plovdiv");
		school.setSchoolType(SchoolType.HIGHSCHOOL);
		school.setStudentClasses(studentClasses);
		
		StudentClass sc = new StudentClass();
		sc.setCapacity(25);
		sc.setName("12j");
		sc.setSpeciality("Informatics with English");
		
		studentClasses.add(sc);
		
		sc = new StudentClass();
		sc.setCapacity(28);
		sc.setName("12b");
		sc.setSpeciality("Informatics with French");
		
		studentClasses.add(sc);
		
		System.out.println(school.toString());
		
		// add new class and print again
		
		sc = new StudentClass();
		sc.setCapacity(32);
		sc.setName("11e");
		sc.setSpeciality("Biology with English");
		
		school.addNewClass(sc);
		
		System.out.println(school.toString());

	}

}
