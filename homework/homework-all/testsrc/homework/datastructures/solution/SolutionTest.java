package homework.datastructures.solution;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import homework.AbstractTest;

public class SolutionTest extends AbstractTest {
	
	private String packageName;
	
	@Before
	public void setup() {
		packageName = "homework.datastructures.solution";
	}
	
	@Test
	public void task1() {
		Class sClass = getClass(packageName, "Solution");
		Solution obj = (Solution)createInstance(sClass);
		
		List<Integer> source = new ArrayList<>();
		source.add(1);
		source.add(1);
		source.add(1);
		source.add(1);
		executeTest(obj, "arrayListSum", 4, new Object[]{source}, new Class[]{List.class});
		source.add(10);
		executeTest(obj, "arrayListSum", 14, new Object[]{source}, new Class[]{List.class});
		source.removeAll(source);
		executeTest(obj, "arrayListSum", 0, new Object[]{source}, new Class[]{List.class});
		source.add(-100);
		source.add(200);
		executeTest(obj, "arrayListSum", 100, new Object[]{source}, new Class[]{List.class});
		source.remove(0);
		executeTest(obj, "arrayListSum", 200, new Object[]{source}, new Class[]{List.class});

	}
	
	@Test
	public void task2() {
		Class sClass = getClass(packageName, "Solution");
		Solution obj = (Solution)createInstance(sClass);
		
		Method arrayListCompexSum = getMethod(sClass, "arrayListComplexSum", new Class[]{List.class});
		
		List<Integer> source = new ArrayList<>();
		source.add(1);
		source.add(2);
		source.add(3);
		source.add(2);
		
		Map<Integer,Integer> result = (Map<Integer,Integer>)invokeMethod(arrayListCompexSum, obj, source);
		
		assertTrue(result.get(4) == 4);
		
		source.add(6);
		result = (Map<Integer,Integer>)invokeMethod(arrayListCompexSum, obj, source);
		
		assertTrue(result.get(10) == 4);
		
		source.add(11);
		result = (Map<Integer,Integer>)invokeMethod(arrayListCompexSum, obj, source);
		
		assertTrue(result.get(10) == 15);
		
	}
	
	@Test
	public void task3() {
		Class sClass = getClass(packageName, "Solution");
		Solution obj = (Solution)createInstance(sClass);
		
		Method transformList = getMethod(sClass, "transformList", new Class[]{List.class});
		
		List<Integer> source = new ArrayList<>();
		source.add(1);
		source.add(2);
		source.add(3);
		source.add(100);
		
		Map<Integer,Integer> result = (Map<Integer,Integer>)invokeMethod(transformList, obj, source);
		
		assertTrue(result.get(0) == 1);
		assertTrue(result.get(1) == 2);
		assertTrue(result.get(2) == 3);
		assertTrue(result.get(3) == 100);
		
	}
	
	@Test
	public void task4() {
		Class sClass = getClass(packageName, "Solution");
		Solution obj = (Solution)createInstance(sClass);
		
		Method assertListItemsLength = getMethod(sClass, "assertListItemsLength", new Class[]{List.class});
		
		List<String> source = new ArrayList<>();
		source.add("1");
		source.add("2");
		source.add("3");
		source.add("4");
		
		executeTest(obj, "assertListItemsLength", true , new Object[]{source}, new Class[]{List.class});
		
		source.add("214");
		
		executeTest(obj, "assertListItemsLength", false , new Object[]{source}, new Class[]{List.class});
		
		source.add("rad");
		
		executeTest(obj, "assertListItemsLength", true , new Object[]{source}, new Class[]{List.class});
		
		source.add("radko");
		
		source.add("radkoo");
		
		source.add("r");
		
		executeTest(obj, "assertListItemsLength", true , new Object[]{source}, new Class[]{List.class});
		
	}
	
	@Test
	public void task5() {
		Class sClass = getClass(packageName, "Solution");
		Solution obj = (Solution)createInstance(sClass);
		
		Method sumGroupedKeys = getMethod(sClass, "sumGroupedKeys", new Class[]{Map.class});
		
		Map<String, Integer> source = new HashMap<>();
		source.put("to", 5); // +
		source.put("of", 5); // +
		source.put("an", 10); 
		
		source.put("a", 1); // +
		source.put("b", 2); // +
		source.put("c", 3); 
		
		source.put("radko", 50); // +
		source.put("todor", 20);
		
		Map<Integer, Integer> result = (Map<Integer,Integer>)invokeMethod(sumGroupedKeys, obj, source);
		
		assertTrue(result.get(2) == 20);
		assertTrue(result.get(1) == 6);
		assertTrue(result.get(5) == 70);
		assertTrue(result.get(10) == null);
		
	}
	
	
}
