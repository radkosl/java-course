package homework;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AbstractTest {
	
	protected static final String messageFormat = " >>%s<< : Expected result from method '%s' with parameters '%s' is '%s', actual result is '%s' ...";
	protected static final String messageException = ">> There was an error while running the test for %s method. Please skip this one for now. Exception message: %s <<";
	protected static final String instanceException = ">> Object from class %s cannot be created. Full class name: %s. Exception message: %s << ";
	protected static final String messageNoMethod = ">>%s<< : There is no (static | non- static) method with name '%s'. Please create it!";
	protected static final String messageNoClass = "There is no class for name '%s' in package '%s'. Please create it !";
	
	protected Class getClass(String packagePath, String className) {
		
		try {
			return Class.forName(packagePath + "." + className);
		} catch (ClassNotFoundException e) {
			assertTrue(String.format(messageNoClass, className, packagePath), false);
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	protected Method getMethod(@SuppressWarnings("rawtypes") Class cls, String methodName, Class... paramTypes) {
		
		try {
			Method method = cls.getMethod(methodName, paramTypes);
			return method;
		} catch (NoSuchMethodException | SecurityException e) {
			assertTrue(String.format(messageNoMethod, cls.getCanonicalName(), methodName), false);
		}
		return null;
		
	}
	
	protected void executeTest(Object source, String methodName, Object expectedResult, Object[] args, Class[] paramTypes) {
		
		Method m = getMethod(source.getClass(), methodName, paramTypes);
		Object result = invokeMethod(m, source, args);
		String argsMessage = getArgsMessage(args);
		assertTrue(String.format(messageFormat, methodName, methodName, argsMessage, expectedResult, result), result.equals(expectedResult));
	}
	
	private String getArgsMessage(Object[] args) {
		if(args == null || args.length == 0) {
			return "[]";
		}
		
		String msg = "[";
		for(Object o : args) {
			msg += o.toString() + ", ";
		}
		return msg + "]";
	}

	protected Object invokeMethod(Method m, Object source, Object... args) {
		try {
			Object result = m.invoke(source, args);
			return result;
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			assertTrue(String.format(messageException, m.getName(), e.getMessage()), false);
		}
		
		return null;
	}
	
	protected Object createInstance(Class classObj) {
		try {
			return classObj.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			assertTrue(String.format(instanceException,classObj.getSimpleName(), classObj.getCanonicalName(), e.getMessage()), false);
		}
		return null;
	}
	
}
