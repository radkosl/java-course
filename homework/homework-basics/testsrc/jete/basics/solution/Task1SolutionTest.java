package jete.basics.solution;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import jete.basics.solution.TasksSolution;

public class Task1SolutionTest {
	
	private TasksSolution task1;
	
	@Before
	public void setup() {
		task1 = new TasksSolution();
	}
	
	@Test
	public void declareVariables() {
		boolean res = task1.declareVariables();
		assertTrue("@declareVariables Expects value to be true, " + res + " instead.", res);
	}
	
	@Test
	public void initializeIntVariable() {
		int val = task1.initializeIntVariable();
		assertTrue("@initializeIntVariable Expects value to be 257, " + val + " instead.", val == 257);
	}
	
	@Test
	public void representCharAsNumber() {
		int num = task1.representCharAsNumber();
		assertTrue("@representCharAsNumber Expects value to be 67, " + num + " instead.", num == 67);
	}
	
	@Test
	public void representNumberAsChar() {
		char symbol = task1.representNumberAsChar();
		assertTrue("@representNumberAsChar Expects value to be 'E', '" + symbol + "' instead.", symbol == 'E');
	}
	
	@Test
	public void calculateIntResult() {
		int res = task1.calculateIntResult();
		assertTrue("@calculateIntResult Expects value to be '900', '" + res + "' instead.", res == 900);
	}
	
	@Test
	public void findТheReminder() {
		int res = task1.findТheReminder();
		assertTrue("@findТheReminder Expects value to be '108', '" + res + "' instead.", res == 108);
	}

}
