package jete.lecture4.exercise;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import jete.lecture4.exercise.Task;

public class TaskTest {
	Task task;
	
	@Before
	public void setup() {
		task = new Task();
	}
	
	@Test
	public void task1() {
		int result = task.findGreatestNumber();
		assertTrue("Result should be '500', " + result + " found.", result == 500);
	}
	
	@Test
	public void task2() {
		char result = task.findTheProductNumberSign();
		assertTrue("Result should be '+', " + result + " found.", result == '+');
	}
	
	@Test
	public void task3() {
		boolean result = task.calculationAndLogicalOperations();
		assertTrue("Result should be 'false', " + result + " found.", !result);
	}
	
	@Test
	public void task4() {
		int result = task.findSum1();
		assertTrue("Result should be '24500', " + result + " found.", result == 24500);
	}
	
	@Test
	public void task5() {
		String result = task.easyOne();
		assertTrue("Result should be 'FooBarFooFooBarFooBothFooBarFooFooBarFooBothFooBarFooFooBarFooBothFooBarFooFooBarFooBothFooBarFooFooBarFooBothFooBarFooFooBarFooBothFooBarFooFoo', " + result + " found.", "FooBarFooFooBarFooBothFooBarFooFooBarFooBothFooBarFooFooBarFooBothFooBarFooFooBarFooBothFooBarFooFooBarFooBothFooBarFooFooBarFooBothFooBarFooFoo".equals(result));
	}
	
}
