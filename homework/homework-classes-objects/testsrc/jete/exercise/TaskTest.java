package jete.exercise;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import jete.AbstractTest;

public class TaskTest extends AbstractTest {
	
	private String packageName;
	
	@Before
	public void setup() {
		packageName = "jete.exercise";
	}
	
	@Test
	public void task1() {
		
		Class fruitClass = getClass(packageName, "Fruit");
		
		Fruit fruit = (Fruit)createInstance(fruitClass);
		
		Method mGetName = getMethod(fruitClass, "getName");
		Method mGetColor = getMethod(fruitClass, "getColor");
		Method mGetShape = getMethod(fruitClass, "getShape");
		
		Method mSetName = getMethod(fruitClass, "setName", String.class);
		Method mSetColor = getMethod(fruitClass, "setColor", String.class);
		Method mSetShape = getMethod(fruitClass, "setShape", String.class);
		
		invokeMethod(mSetName, fruit, "Apple");
		invokeMethod(mSetColor, fruit, "Green");
		invokeMethod(mSetShape, fruit, "Oval");
		
		String name = (String)invokeMethod(mGetName, fruit);
		String color = (String)invokeMethod(mGetColor, fruit);
		String shape = (String)invokeMethod(mGetShape, fruit);
		
		
		String expectedMessage = String.format("This Fruit is %s, %s %s", color, shape, name);
		
		executeTest(fruit, "describeThisFruit", expectedMessage, new Object[0], new Class[0]);
	}
	
	@Test
	public void task2() {
		
		Class shapeClass = getClass(packageName, "Shape");
		
		Shape shape = (Shape)createInstance(shapeClass);
		
		Method getCreatedObjectsCount = getMethod(shapeClass, "getCreatedObjectsCount");
		Method removeObjects = getMethod(shapeClass, "removeObjects" , int.class);
		Method getShapeStatus = getMethod(shapeClass, "getShapeStatus");
		
		shape = (Shape)createInstance(shapeClass);
		shape = (Shape)createInstance(shapeClass);
		shape = (Shape)createInstance(shapeClass);
		shape = (Shape)createInstance(shapeClass);
		
		invokeMethod(removeObjects, shape, 1);
		String result = (String) invokeMethod(getShapeStatus, shape);
		int count = (int) invokeMethod(getCreatedObjectsCount, shape);
		
		executeTest(shape, "getShapeStatus", "Current Shape Count is 4", new Object[0], new Class[0]);
		invokeMethod(removeObjects, shape, 1);
		executeTest(shape, "getShapeStatus", "Current Shape Count is 3", new Object[0], new Class[0]);
		executeTest(shape, "getCreatedObjectsCount", 3, new Object[0], new Class[0]);
	}
		
}
