package jete.exercise;
/**
 *  Create a class called 'Shape' in jete.exercise package.
	Create 1 field for this class
	Field 1. static int field called 'objectsCount';
	
	1 Please make sure you add constructor with no parameters (default constructor);
	In that constructor, increment the 'objectsCount' field with '1'.
	
	2 Create static method called "getCreatedObjectsCount" with no parameters which return "objectsCount" field;
	
	3 Create static method called "removeObjects" with one parameter 'n' which doesn't return value. That method should
	decrement "objectsCount" field with "n"
	
	4 Create method called "getShapeStatus" with no parameters which returns "Current Shape Count is {objectsCount}" as String, 
	where {objectsCount} is replaced with the value in objectsCount field
	
 * @author radkol
 *
 */
public class Shape {

	public static void main(String[] args) {
		System.out.println("Implement me");
	}
}
