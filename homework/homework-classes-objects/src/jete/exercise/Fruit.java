package jete.exercise;

/**
 * 
 *  Create a class called 'Fruit' in jete.exercise package.
	Create 3 fields for this class
	Field 1. String field called 'name';
	Field 2. String field called 'color';
	Field 3. String field called 'shape';
	Create appropriate constructor(s). 
	Please make sure you add constructor with no parameters (default constructor);
	Create getter / setter methods for all fields.
	Create method called "describeThisFruit" which has no arguments and will return String, containing the following:
	"This Fruit is {field.color}, {field.shape} {field.name}" (without the quotes)
	where {###} is actually the values in the fields color, shape, name 

 * @author radkol
 *
 */
public class Fruit {
	/** TODO **/
}
