package jete.solution;

/**
 *  Create a class called 'Shape' in jete.exercise package.
	Create 1 field for this class
	Field 1. static int field called 'objectsCount';
	
	1 Please make sure you add constructor with no parameters (default constructor);
	In that constructor, increment the 'objectsCount' field with '1'.
	
	2 Create static method called "getCreatedObjectsCount" with no parameters which return "objectsCount" field;
	
	3 Create static method called "removeObjects" with one parameter 'n' which doesn't return value. That method should
	decrement "objectsCount" field with "n"
	
	4 Create method called "getShapeStatus" with no parameters which returns "Current Shape Count is {objectsCount}" as String, 
	where {objectsCount} is replaced with the value in objectsCount field
	
 * @author radkol
 *
 */
public class Shape {

	/**
	 * Why static? What is the difference between static and non-static.
	 * The point here is to keep track of the created objects
	 * like how many times we call the constructor :)
	 */
	private static int objectsCount;
	
	public Shape() {
		// even eclipse give warning for that. You can access static properties from objects,
		// but it is not encouraging since the result will always be the same right ?
		// ----> this.objectsCount++; <----
		objectsCount++;
	}
	
	public static int getCreatedObjectsCount() {
		return objectsCount;
	}
	
	public static void removeObjects(int n) {
		objectsCount = objectsCount - n;
	}
	
	public String getShapeStatus() {
		return String.format("Current Shape Count is %d", objectsCount);
	}
	
}
