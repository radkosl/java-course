#######################################
Task 1.
#######################################
Create a class called 'Fruit' in jete.exercise package.
Create 3 fields for this class
Field 1. String field called 'name';
Field 2. String field called 'color';
Field 3. String field called 'shape';
Create appropriate constructor(s). 
Please make sure you add constructor with no parameters (default constructor);
Create getter / setter methods for all fields.
Create method called "describeThisFruit" which has no arguments and will return String, containing the following:
This Fruit is {field.color}, {field.shape} {field.name}", where {###} is actually the values in the fields color, shape, name 
########################################
Run the TaskTest to check your implementation. You can also create separate project and test things there!.
########################################
Task 2.
########################################
Create a class called 'Shape' in jete.exercise package.
Create 1 field for this class
Field 1. static int field called 'objectsCount';
Please make sure you add constructor with no parameters (default constructor);

In that constructor, increment the 'objectsCount' field with '1'.

Create method called "getCreatedObjectsCount" with no parameters which return "objectsCount" field;

Create static method called "removeObjects" with one parameter 'n' which doesn't return value. That method should
decrement "objectsCount" field with "n"

Create method called "getShapeStatus" with no parameters which returns "Current Shape Count is {objectsCount}" as String, 
where {objectsCount} is replaced with the value in objectsCount field

########################################
Run the TaskTest to check your implementation. You can also create separate project and test things there!.
########################################